'use strict';

define('@ember/test-helpers/-utils', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.nextTickPromise = nextTickPromise;
  exports.runDestroyablesFor = runDestroyablesFor;
  exports.isNumeric = isNumeric;
  const nextTick = exports.nextTick = setTimeout;
  const futureTick = exports.futureTick = setTimeout;

  /**
   @private
   @returns {Promise<void>} promise which resolves on the next turn of the event loop
  */
  function nextTickPromise() {
    return new Ember.RSVP.Promise(resolve => {
      nextTick(resolve);
    });
  }

  /**
   Retrieves an array of destroyables from the specified property on the object
   provided, iterates that array invoking each function, then deleting the
   property (clearing the array).
  
   @private
   @param {Object} object an object to search for the destroyable array within
   @param {string} property the property on the object that contains the destroyable array
  */
  function runDestroyablesFor(object, property) {
    let destroyables = object[property];

    if (!destroyables) {
      return;
    }

    for (let i = 0; i < destroyables.length; i++) {
      destroyables[i]();
    }

    delete object[property];
  }

  /**
   Returns whether the passed in string consists only of numeric characters.
  
   @private
   @param {string} n input string
   @returns {boolean} whether the input string consists only of numeric characters
   */
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
});
define('@ember/test-helpers/application', ['exports', '@ember/test-helpers/resolver'], function (exports, _resolver) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.setApplication = setApplication;
  exports.getApplication = getApplication;


  var __application__;

  /**
    Stores the provided application instance so that tests being ran will be aware of the application under test.
  
    - Required by `setupApplicationContext` method.
    - Used by `setupContext` and `setupRenderingContext` when present.
  
    @public
    @param {Ember.Application} application the application that will be tested
  */
  function setApplication(application) {
    __application__ = application;

    if (!(0, _resolver.getResolver)()) {
      let Resolver = application.Resolver;
      let resolver = Resolver.create({ namespace: application });

      (0, _resolver.setResolver)(resolver);
    }
  }

  /**
    Retrieve the application instance stored by `setApplication`.
  
    @public
    @returns {Ember.Application} the previously stored application instance under test
  */
  function getApplication() {
    return __application__;
  }
});
define('@ember/test-helpers/build-owner', ['exports', 'ember-test-helpers/legacy-0-6-x/build-registry'], function (exports, _buildRegistry) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = buildOwner;


  /**
    Creates an "owner" (an object that either _is_ or duck-types like an
    `Ember.ApplicationInstance`) from the provided options.
  
    If `options.application` is present (e.g. setup by an earlier call to
    `setApplication`) an `Ember.ApplicationInstance` is built via
    `application.buildInstance()`.
  
    If `options.application` is not present, we fall back to using
    `options.resolver` instead (setup via `setResolver`). This creates a mock
    "owner" by using a custom created combination of `Ember.Registry`,
    `Ember.Container`, `Ember._ContainerProxyMixin`, and
    `Ember._RegistryProxyMixin`.
  
    @private
    @param {Ember.Application} [application] the Ember.Application to build an instance from
    @param {Ember.Resolver} [resolver] the resolver to use to back a "mock owner"
    @returns {Promise<Ember.ApplicationInstance>} a promise resolving to the generated "owner"
  */
  function buildOwner(application, resolver) {
    if (application) {
      return application.boot().then(app => app.buildInstance().boot());
    }

    if (!resolver) {
      throw new Error('You must set up the ember-test-helpers environment with either `setResolver` or `setApplication` before running any tests.');
    }

    let { owner } = (0, _buildRegistry.default)(resolver);
    return Ember.RSVP.Promise.resolve(owner);
  }
});
define('@ember/test-helpers/dom/-get-element', ['exports', '@ember/test-helpers/dom/get-root-element'], function (exports, _getRootElement) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = getElement;


  /**
    Used internally by the DOM interaction helpers to find one element.
  
    @private
    @param {string|Element} target the element or selector to retrieve
    @returns {Element} the target or selector
  */
  function getElement(target) {
    if (target.nodeType === Node.ELEMENT_NODE || target.nodeType === Node.DOCUMENT_NODE || target instanceof Window) {
      return target;
    } else if (typeof target === 'string') {
      let rootElement = (0, _getRootElement.default)();

      return rootElement.querySelector(target);
    } else {
      throw new Error('Must use an element or a selector string');
    }
  }
});
define('@ember/test-helpers/dom/-get-elements', ['exports', '@ember/test-helpers/dom/get-root-element'], function (exports, _getRootElement) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = getElements;


  /**
    Used internally by the DOM interaction helpers to find multiple elements.
  
    @private
    @param {string} target the selector to retrieve
    @returns {NodeList} the matched elements
  */
  function getElements(target) {
    if (typeof target === 'string') {
      let rootElement = (0, _getRootElement.default)();

      return rootElement.querySelectorAll(target);
    } else {
      throw new Error('Must use a selector string');
    }
  }
});
define('@ember/test-helpers/dom/-is-focusable', ['exports', '@ember/test-helpers/dom/-is-form-control'], function (exports, _isFormControl) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = isFocusable;


  const FOCUSABLE_TAGS = ['A'];

  /**
    @private
    @param {Element} element the element to check
    @returns {boolean} `true` when the element is focusable, `false` otherwise
  */
  function isFocusable(element) {
    if ((0, _isFormControl.default)(element) || element.isContentEditable || FOCUSABLE_TAGS.indexOf(element.tagName) > -1) {
      return true;
    }

    return element.hasAttribute('tabindex');
  }
});
define('@ember/test-helpers/dom/-is-form-control', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = isFormControl;
  const FORM_CONTROL_TAGS = ['INPUT', 'BUTTON', 'SELECT', 'TEXTAREA'];

  /**
    @private
    @param {Element} element the element to check
    @returns {boolean} `true` when the element is a form control, `false` otherwise
  */
  function isFormControl(element) {
    let { tagName, type } = element;

    if (type === 'hidden') {
      return false;
    }

    return FORM_CONTROL_TAGS.indexOf(tagName) > -1;
  }
});
define("@ember/test-helpers/dom/-to-array", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = toArray;
  /**
    @private
    @param {NodeList} nodelist the nodelist to convert to an array
    @returns {Array} an array
  */
  function toArray(nodelist) {
    let array = new Array(nodelist.length);
    for (let i = 0; i < nodelist.length; i++) {
      array[i] = nodelist[i];
    }

    return array;
  }
});
define('@ember/test-helpers/dom/blur', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/settled', '@ember/test-helpers/dom/-is-focusable', '@ember/test-helpers/-utils'], function (exports, _getElement, _fireEvent, _settled, _isFocusable, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.__blur__ = __blur__;
  exports.default = blur;


  /**
    @private
    @param {Element} element the element to trigger events on
  */
  function __blur__(element) {
    let browserIsNotFocused = document.hasFocus && !document.hasFocus();

    // makes `document.activeElement` be `body`.
    // If the browser is focused, it also fires a blur event
    element.blur();

    // Chrome/Firefox does not trigger the `blur` event if the window
    // does not have focus. If the document does not have focus then
    // fire `blur` event via native event.
    if (browserIsNotFocused) {
      (0, _fireEvent.default)(element, 'blur', { bubbles: false });
      (0, _fireEvent.default)(element, 'focusout');
    }
  }

  /**
    Unfocus the specified target.
  
    Sends a number of events intending to simulate a "real" user unfocusing an
    element.
  
    The following events are triggered (in order):
  
    - `blur`
    - `focusout`
  
    The exact listing of events that are triggered may change over time as needed
    to continue to emulate how actual browsers handle unfocusing a given element.
  
    @public
    @param {string|Element} [target=document.activeElement] the element or selector to unfocus
    @return {Promise<void>} resolves when settled
  */
  function blur(target = document.activeElement) {
    return (0, _utils.nextTickPromise)().then(() => {
      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`blur('${target}')\`.`);
      }

      if (!(0, _isFocusable.default)(element)) {
        throw new Error(`${target} is not focusable`);
      }

      __blur__(element);

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/click', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/dom/focus', '@ember/test-helpers/settled', '@ember/test-helpers/dom/-is-focusable', '@ember/test-helpers/-utils', '@ember/test-helpers/dom/-is-form-control'], function (exports, _getElement, _fireEvent, _focus, _settled, _isFocusable, _utils, _isFormControl) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.__click__ = __click__;
  exports.default = click;


  /**
    @private
    @param {Element} element the element to click on
    @param {Object} options the options to be merged into the mouse events
  */
  function __click__(element, options) {
    (0, _fireEvent.default)(element, 'mousedown', options);

    if ((0, _isFocusable.default)(element)) {
      (0, _focus.__focus__)(element);
    }

    (0, _fireEvent.default)(element, 'mouseup', options);
    (0, _fireEvent.default)(element, 'click', options);
  }

  /**
    Clicks on the specified target.
  
    Sends a number of events intending to simulate a "real" user clicking on an
    element.
  
    For non-focusable elements the following events are triggered (in order):
  
    - `mousedown`
    - `mouseup`
    - `click`
  
    For focusable (e.g. form control) elements the following events are triggered
    (in order):
  
    - `mousedown`
    - `focus`
    - `focusin`
    - `mouseup`
    - `click`
  
    The exact listing of events that are triggered may change over time as needed
    to continue to emulate how actual browsers handle clicking a given element.
  
    Use the `options` hash to change the parameters of the MouseEvents. 
  
    @public
    @param {string|Element} target the element or selector to click on
    @param {Object} options the options to be merged into the mouse events
    @return {Promise<void>} resolves when settled
  */
  function click(target, options = {}) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `click`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`click('${target}')\`.`);
      }

      let isDisabledFormControl = (0, _isFormControl.default)(element) && element.disabled === true;

      if (!isDisabledFormControl) {
        __click__(element, options);
      }

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/double-click', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/dom/focus', '@ember/test-helpers/settled', '@ember/test-helpers/dom/-is-focusable', '@ember/test-helpers/-utils'], function (exports, _getElement, _fireEvent, _focus, _settled, _isFocusable, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.__doubleClick__ = __doubleClick__;
  exports.default = doubleClick;


  /**
    @private
    @param {Element} element the element to double-click on
    @param {Object} options the options to be merged into the mouse events
  */
  function __doubleClick__(element, options) {
    (0, _fireEvent.default)(element, 'mousedown', options);

    if ((0, _isFocusable.default)(element)) {
      (0, _focus.__focus__)(element);
    }

    (0, _fireEvent.default)(element, 'mouseup', options);
    (0, _fireEvent.default)(element, 'click', options);
    (0, _fireEvent.default)(element, 'mousedown', options);
    (0, _fireEvent.default)(element, 'mouseup', options);
    (0, _fireEvent.default)(element, 'click', options);
    (0, _fireEvent.default)(element, 'dblclick', options);
  }

  /**
    Double-clicks on the specified target.
  
    Sends a number of events intending to simulate a "real" user clicking on an
    element.
  
    For non-focusable elements the following events are triggered (in order):
  
    - `mousedown`
    - `mouseup`
    - `click`
    - `mousedown`
    - `mouseup`
    - `click`
    - `dblclick`
  
    For focusable (e.g. form control) elements the following events are triggered
    (in order):
  
    - `mousedown`
    - `focus`
    - `focusin`
    - `mouseup`
    - `click`
    - `mousedown`
    - `mouseup`
    - `click`
    - `dblclick`
  
    The exact listing of events that are triggered may change over time as needed
    to continue to emulate how actual browsers handle clicking a given element.
  
    Use the `options` hash to change the parameters of the MouseEvents. 
  
    @public
    @param {string|Element} target the element or selector to double-click on
    @param {Object} options the options to be merged into the mouse events
    @return {Promise<void>} resolves when settled
  */
  function doubleClick(target, options = {}) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `doubleClick`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`doubleClick('${target}')\`.`);
      }

      __doubleClick__(element, options);
      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/fill-in', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/-is-form-control', '@ember/test-helpers/dom/focus', '@ember/test-helpers/settled', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/-utils'], function (exports, _getElement, _isFormControl, _focus, _settled, _fireEvent, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = fillIn;


  /**
    Fill the provided text into the `value` property (or set `.innerHTML` when
    the target is a content editable element) then trigger `change` and `input`
    events on the specified target.
  
    @public
    @param {string|Element} target the element or selector to enter text into
    @param {string} text the text to fill into the target element
    @return {Promise<void>} resolves when the application is settled
  */
  function fillIn(target, text) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `fillIn`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`fillIn('${target}')\`.`);
      }
      let isControl = (0, _isFormControl.default)(element);
      if (!isControl && !element.isContentEditable) {
        throw new Error('`fillIn` is only usable on form controls or contenteditable elements.');
      }

      if (typeof text === 'undefined' || text === null) {
        throw new Error('Must provide `text` when calling `fillIn`.');
      }

      (0, _focus.__focus__)(element);

      if (isControl) {
        element.value = text;
      } else {
        element.innerHTML = text;
      }

      (0, _fireEvent.default)(element, 'input');
      (0, _fireEvent.default)(element, 'change');

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/find-all', ['exports', '@ember/test-helpers/dom/-get-elements', '@ember/test-helpers/dom/-to-array'], function (exports, _getElements, _toArray) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = find;


  /**
    Find all elements matched by the given selector. Equivalent to calling
    `querySelectorAll()` on the test root element.
  
    @public
    @param {string} selector the selector to search for
    @return {Array} array of matched elements
  */
  function find(selector) {
    if (!selector) {
      throw new Error('Must pass a selector to `findAll`.');
    }

    if (arguments.length > 1) {
      throw new Error('The `findAll` test helper only takes a single argument.');
    }

    return (0, _toArray.default)((0, _getElements.default)(selector));
  }
});
define('@ember/test-helpers/dom/find', ['exports', '@ember/test-helpers/dom/-get-element'], function (exports, _getElement) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = find;


  /**
    Find the first element matched by the given selector. Equivalent to calling
    `querySelector()` on the test root element.
  
    @public
    @param {string} selector the selector to search for
    @return {Element} matched element or null
  */
  function find(selector) {
    if (!selector) {
      throw new Error('Must pass a selector to `find`.');
    }

    if (arguments.length > 1) {
      throw new Error('The `find` test helper only takes a single argument.');
    }

    return (0, _getElement.default)(selector);
  }
});
define('@ember/test-helpers/dom/fire-event', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = fireEvent;


  // eslint-disable-next-line require-jsdoc
  const MOUSE_EVENT_CONSTRUCTOR = (() => {
    try {
      new MouseEvent('test');
      return true;
    } catch (e) {
      return false;
    }
  })();
  const DEFAULT_EVENT_OPTIONS = { bubbles: true, cancelable: true };
  const KEYBOARD_EVENT_TYPES = exports.KEYBOARD_EVENT_TYPES = Object.freeze(['keydown', 'keypress', 'keyup']);
  const MOUSE_EVENT_TYPES = ['click', 'mousedown', 'mouseup', 'dblclick', 'mouseenter', 'mouseleave', 'mousemove', 'mouseout', 'mouseover'];
  const FILE_SELECTION_EVENT_TYPES = ['change'];

  /**
    Internal helper used to build and dispatch events throughout the other DOM helpers.
  
    @private
    @param {Element} element the element to dispatch the event to
    @param {string} eventType the type of event
    @param {Object} [options] additional properties to be set on the event
    @returns {Event} the event that was dispatched
  */
  function fireEvent(element, eventType, options = {}) {
    if (!element) {
      throw new Error('Must pass an element to `fireEvent`');
    }

    let event;
    if (KEYBOARD_EVENT_TYPES.indexOf(eventType) > -1) {
      event = buildKeyboardEvent(eventType, options);
    } else if (MOUSE_EVENT_TYPES.indexOf(eventType) > -1) {
      let rect;
      if (element instanceof Window) {
        rect = element.document.documentElement.getBoundingClientRect();
      } else if (element.nodeType === Node.DOCUMENT_NODE) {
        rect = element.documentElement.getBoundingClientRect();
      } else if (element.nodeType === Node.ELEMENT_NODE) {
        rect = element.getBoundingClientRect();
      } else {
        return;
      }

      let x = rect.left + 1;
      let y = rect.top + 1;
      let simulatedCoordinates = {
        screenX: x + 5, // Those numbers don't really mean anything.
        screenY: y + 95, // They're just to make the screenX/Y be different of clientX/Y..
        clientX: x,
        clientY: y
      };

      event = buildMouseEvent(eventType, Ember.assign(simulatedCoordinates, options));
    } else if (FILE_SELECTION_EVENT_TYPES.indexOf(eventType) > -1 && element.files) {
      event = buildFileEvent(eventType, element, options);
    } else {
      event = buildBasicEvent(eventType, options);
    }

    element.dispatchEvent(event);
    return event;
  }

  // eslint-disable-next-line require-jsdoc
  function buildBasicEvent(type, options = {}) {
    let event = document.createEvent('Events');

    let bubbles = options.bubbles !== undefined ? options.bubbles : true;
    let cancelable = options.cancelable !== undefined ? options.cancelable : true;

    delete options.bubbles;
    delete options.cancelable;

    // bubbles and cancelable are readonly, so they can be
    // set when initializing event
    event.initEvent(type, bubbles, cancelable);
    Ember.assign(event, options);
    return event;
  }

  // eslint-disable-next-line require-jsdoc
  function buildMouseEvent(type, options = {}) {
    let event;
    let eventOpts = Ember.assign({ view: window }, DEFAULT_EVENT_OPTIONS, options);
    if (MOUSE_EVENT_CONSTRUCTOR) {
      event = new MouseEvent(type, eventOpts);
    } else {
      try {
        event = document.createEvent('MouseEvents');
        event.initMouseEvent(type, eventOpts.bubbles, eventOpts.cancelable, window, eventOpts.detail, eventOpts.screenX, eventOpts.screenY, eventOpts.clientX, eventOpts.clientY, eventOpts.ctrlKey, eventOpts.altKey, eventOpts.shiftKey, eventOpts.metaKey, eventOpts.button, eventOpts.relatedTarget);
      } catch (e) {
        event = buildBasicEvent(type, options);
      }
    }

    return event;
  }

  // eslint-disable-next-line require-jsdoc
  function buildKeyboardEvent(type, options = {}) {
    let eventOpts = Ember.assign({}, DEFAULT_EVENT_OPTIONS, options);
    let event, eventMethodName;

    try {
      event = new KeyboardEvent(type, eventOpts);

      // Property definitions are required for B/C for keyboard event usage
      // If this properties are not defined, when listening for key events
      // keyCode/which will be 0. Also, keyCode and which now are string
      // and if app compare it with === with integer key definitions,
      // there will be a fail.
      //
      // https://w3c.github.io/uievents/#interface-keyboardevent
      // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent
      Object.defineProperty(event, 'keyCode', {
        get() {
          return parseInt(eventOpts.keyCode);
        }
      });

      Object.defineProperty(event, 'which', {
        get() {
          return parseInt(eventOpts.which);
        }
      });

      return event;
    } catch (e) {
      // left intentionally blank
    }

    try {
      event = document.createEvent('KeyboardEvents');
      eventMethodName = 'initKeyboardEvent';
    } catch (e) {
      // left intentionally blank
    }

    if (!event) {
      try {
        event = document.createEvent('KeyEvents');
        eventMethodName = 'initKeyEvent';
      } catch (e) {
        // left intentionally blank
      }
    }

    if (event) {
      event[eventMethodName](type, eventOpts.bubbles, eventOpts.cancelable, window, eventOpts.ctrlKey, eventOpts.altKey, eventOpts.shiftKey, eventOpts.metaKey, eventOpts.keyCode, eventOpts.charCode);
    } else {
      event = buildBasicEvent(type, options);
    }

    return event;
  }

  // eslint-disable-next-line require-jsdoc
  function buildFileEvent(type, element, files = []) {
    let event = buildBasicEvent(type);

    if (files.length > 0) {
      Object.defineProperty(files, 'item', {
        value(index) {
          return typeof index === 'number' ? this[index] : null;
        }
      });
      Object.defineProperty(element, 'files', {
        value: files,
        configurable: true
      });
    }

    Object.defineProperty(event, 'target', {
      value: element
    });

    return event;
  }
});
define('@ember/test-helpers/dom/focus', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/settled', '@ember/test-helpers/dom/-is-focusable', '@ember/test-helpers/-utils'], function (exports, _getElement, _fireEvent, _settled, _isFocusable, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.__focus__ = __focus__;
  exports.default = focus;


  /**
    @private
    @param {Element} element the element to trigger events on
  */
  function __focus__(element) {
    let browserIsNotFocused = document.hasFocus && !document.hasFocus();

    // makes `document.activeElement` be `element`. If the browser is focused, it also fires a focus event
    element.focus();

    // Firefox does not trigger the `focusin` event if the window
    // does not have focus. If the document does not have focus then
    // fire `focusin` event as well.
    if (browserIsNotFocused) {
      // if the browser is not focused the previous `el.focus()` didn't fire an event, so we simulate it
      (0, _fireEvent.default)(element, 'focus', {
        bubbles: false
      });

      (0, _fireEvent.default)(element, 'focusin');
    }
  }

  /**
    Focus the specified target.
  
    Sends a number of events intending to simulate a "real" user focusing an
    element.
  
    The following events are triggered (in order):
  
    - `focus`
    - `focusin`
  
    The exact listing of events that are triggered may change over time as needed
    to continue to emulate how actual browsers handle focusing a given element.
  
    @public
    @param {string|Element} target the element or selector to focus
    @return {Promise<void>} resolves when the application is settled
  */
  function focus(target) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `focus`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`focus('${target}')\`.`);
      }

      if (!(0, _isFocusable.default)(element)) {
        throw new Error(`${target} is not focusable`);
      }

      __focus__(element);

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/get-root-element', ['exports', '@ember/test-helpers/setup-context'], function (exports, _setupContext) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = getRootElement;


  /**
    Get the root element of the application under test (usually `#ember-testing`)
  
    @public
    @returns {Element} the root element
  */
  function getRootElement() {
    let context = (0, _setupContext.getContext)();
    let owner = context && context.owner;

    if (!owner) {
      throw new Error('Must setup rendering context before attempting to interact with elements.');
    }

    let rootElement;
    // When the host app uses `setApplication` (instead of `setResolver`) the owner has
    // a `rootElement` set on it with the element or id to be used
    if (owner && owner._emberTestHelpersMockOwner === undefined) {
      rootElement = owner.rootElement;
    } else {
      rootElement = '#ember-testing';
    }

    if (rootElement.nodeType === Node.ELEMENT_NODE || rootElement.nodeType === Node.DOCUMENT_NODE || rootElement instanceof Window) {
      return rootElement;
    } else if (typeof rootElement === 'string') {
      return document.querySelector(rootElement);
    } else {
      throw new Error('Application.rootElement must be an element or a selector string');
    }
  }
});
define('@ember/test-helpers/dom/tap', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/dom/click', '@ember/test-helpers/settled', '@ember/test-helpers/-utils'], function (exports, _getElement, _fireEvent, _click, _settled, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = tap;


  /**
    Taps on the specified target.
  
    Sends a number of events intending to simulate a "real" user tapping on an
    element.
  
    For non-focusable elements the following events are triggered (in order):
  
    - `touchstart`
    - `touchend`
    - `mousedown`
    - `mouseup`
    - `click`
  
    For focusable (e.g. form control) elements the following events are triggered
    (in order):
  
    - `touchstart`
    - `touchend`
    - `mousedown`
    - `focus`
    - `focusin`
    - `mouseup`
    - `click`
  
    The exact listing of events that are triggered may change over time as needed
    to continue to emulate how actual browsers handle tapping on a given element.
  
    Use the `options` hash to change the parameters of the tap events. 
  
    @public
    @param {string|Element} target the element or selector to tap on
    @param {Object} options the options to be merged into the touch events
    @return {Promise<void>} resolves when settled
  */
  function tap(target, options = {}) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `tap`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`tap('${target}')\`.`);
      }

      let touchstartEv = (0, _fireEvent.default)(element, 'touchstart', options);
      let touchendEv = (0, _fireEvent.default)(element, 'touchend', options);

      if (!touchstartEv.defaultPrevented && !touchendEv.defaultPrevented) {
        (0, _click.__click__)(element, options);
      }

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/trigger-event', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/settled', '@ember/test-helpers/-utils'], function (exports, _getElement, _fireEvent, _settled, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = triggerEvent;


  /**
   * Triggers an event on the specified target.
   *
   * @public
   * @param {string|Element} target the element or selector to trigger the event on
   * @param {string} eventType the type of event to trigger
   * @param {Object} options additional properties to be set on the event
   * @return {Promise<void>} resolves when the application is settled
   *
   * @example
   * <caption>Using triggerEvent to Upload a file
   * When using triggerEvent to upload a file the `eventType` must be `change` and  you must pass an
   * array of [Blob](https://developer.mozilla.org/en-US/docs/Web/API/Blob) as `options`.</caption>
   *
   * triggerEvent(
   *   'input.fileUpload',
   *   'change',
   *   [new Blob(['Ember Rules!'])]
   * );
   */
  function triggerEvent(target, eventType, options) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `triggerEvent`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`triggerEvent('${target}', ...)\`.`);
      }

      if (!eventType) {
        throw new Error(`Must provide an \`eventType\` to \`triggerEvent\``);
      }

      (0, _fireEvent.default)(element, eventType, options);

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/trigger-key-event', ['exports', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/fire-event', '@ember/test-helpers/settled', '@ember/test-helpers/-utils'], function (exports, _getElement, _fireEvent, _settled, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = triggerKeyEvent;


  const DEFAULT_MODIFIERS = Object.freeze({
    ctrlKey: false,
    altKey: false,
    shiftKey: false,
    metaKey: false
  });

  // This is not a comprehensive list, but it is better than nothing.
  const keyFromKeyCode = {
    8: 'Backspace',
    9: 'Tab',
    13: 'Enter',
    16: 'Shift',
    17: 'Control',
    18: 'Alt',
    20: 'CapsLock',
    27: 'Escape',
    32: ' ',
    37: 'ArrowLeft',
    38: 'ArrowUp',
    39: 'ArrowRight',
    40: 'ArrowDown',
    48: '0',
    49: '1',
    50: '2',
    51: '3',
    52: '4',
    53: '5',
    54: '6',
    55: '7',
    56: '8',
    57: '9',
    65: 'a',
    66: 'b',
    67: 'c',
    68: 'd',
    69: 'e',
    70: 'f',
    71: 'g',
    72: 'h',
    73: 'i',
    74: 'j',
    75: 'k',
    76: 'l',
    77: 'm',
    78: 'n',
    79: 'o',
    80: 'p',
    81: 'q',
    82: 'r',
    83: 's',
    84: 't',
    85: 'u',
    86: 'v',
    87: 'v',
    88: 'x',
    89: 'y',
    90: 'z',
    91: 'Meta',
    93: 'Meta', // There is two keys that map to meta,
    187: '=',
    189: '-'
  };

  /**
    Calculates the value of KeyboardEvent#key given a keycode and the modifiers.
    Note that this works if the key is pressed in combination with the shift key, but it cannot
    detect if caps lock is enabled.
    @param {number} keycode The keycode of the event.
    @param {object} modifiers The modifiers of the event.
    @returns {string} The key string for the event.
   */
  function keyFromKeyCodeAndModifiers(keycode, modifiers) {
    if (keycode > 64 && keycode < 91) {
      if (modifiers.shiftKey) {
        return String.fromCharCode(keycode);
      } else {
        return String.fromCharCode(keycode).toLocaleLowerCase();
      }
    }
    let key = keyFromKeyCode[keycode];
    if (key) {
      return key;
    }
  }

  /**
   * Infers the keycode from the given key
   * @param {string} key The KeyboardEvent#key string
   * @returns {number} The keycode for the given key
   */
  function keyCodeFromKey(key) {
    let keys = Object.keys(keyFromKeyCode);
    let keyCode = keys.find(keyCode => keyFromKeyCode[keyCode] === key);
    if (!keyCode) {
      keyCode = keys.find(keyCode => keyFromKeyCode[keyCode] === key.toLowerCase());
    }
    return parseInt(keyCode);
  }

  /**
    Triggers a keyboard event of given type in the target element.
    It also requires the developer to provide either a string with the [`key`](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values)
    or the numeric [`keyCode`](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode) of the pressed key.
    Optionally the user can also provide a POJO with extra modifiers for the event.
  
    @public
    @param {string|Element} target the element or selector to trigger the event on
    @param {'keydown' | 'keyup' | 'keypress'} eventType the type of event to trigger
    @param {number|string} key the `keyCode`(number) or `key`(string) of the event being triggered
    @param {Object} [modifiers] the state of various modifier keys
    @param {boolean} [modifiers.ctrlKey=false] if true the generated event will indicate the control key was pressed during the key event
    @param {boolean} [modifiers.altKey=false] if true the generated event will indicate the alt key was pressed during the key event
    @param {boolean} [modifiers.shiftKey=false] if true the generated event will indicate the shift key was pressed during the key event
    @param {boolean} [modifiers.metaKey=false] if true the generated event will indicate the meta key was pressed during the key event
    @return {Promise<void>} resolves when the application is settled
  */
  function triggerKeyEvent(target, eventType, key, modifiers = DEFAULT_MODIFIERS) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `triggerKeyEvent`.');
      }

      let element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`triggerKeyEvent('${target}', ...)\`.`);
      }

      if (!eventType) {
        throw new Error(`Must provide an \`eventType\` to \`triggerKeyEvent\``);
      }

      if (_fireEvent.KEYBOARD_EVENT_TYPES.indexOf(eventType) === -1) {
        let validEventTypes = _fireEvent.KEYBOARD_EVENT_TYPES.join(', ');
        throw new Error(`Must provide an \`eventType\` of ${validEventTypes} to \`triggerKeyEvent\` but you passed \`${eventType}\`.`);
      }

      let props;
      if (typeof key === 'number') {
        props = { keyCode: key, which: key, key: keyFromKeyCodeAndModifiers(key, modifiers) };
      } else if (typeof key === 'string' && key.length !== 0) {
        let firstCharacter = key[0];
        if (firstCharacter !== firstCharacter.toUpperCase()) {
          throw new Error(`Must provide a \`key\` to \`triggerKeyEvent\` that starts with an uppercase character but you passed \`${key}\`.`);
        }

        if ((0, _utils.isNumeric)(key) && key.length > 1) {
          throw new Error(`Must provide a numeric \`keyCode\` to \`triggerKeyEvent\` but you passed \`${key}\` as a string.`);
        }

        let keyCode = keyCodeFromKey(key);
        props = { keyCode, which: keyCode, key };
      } else {
        throw new Error(`Must provide a \`key\` or \`keyCode\` to \`triggerKeyEvent\``);
      }

      let options = Ember.assign(props, modifiers);

      (0, _fireEvent.default)(element, eventType, options);

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/dom/type-in', ['exports', '@ember/test-helpers/-utils', '@ember/test-helpers/settled', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/-is-form-control', '@ember/test-helpers/dom/focus', '@ember/test-helpers/dom/fire-event'], function (exports, _utils, _settled, _getElement, _isFormControl, _focus, _fireEvent) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = typeIn;


  /**
   * Mimics character by character entry into the target `input` or `textarea` element.
   *
   * Allows for simulation of slow entry by passing an optional millisecond delay
   * between key events.
  
   * The major difference between `typeIn` and `fillIn` is that `typeIn` triggers
   * keyboard events as well as `input` and `change`.
   * Typically this looks like `focus` -> `focusin` -> `keydown` -> `keypress` -> `keyup` -> `input` -> `change`
   * per character of the passed text (this may vary on some browsers).
   *
   * @public
   * @param {string|Element} target the element or selector to enter text into
   * @param {string} text the test to fill the element with
   * @param {Object} options {delay: x} (default 50) number of milliseconds to wait per keypress
   * @return {Promise<void>} resolves when the application is settled
   */
  function typeIn(target, text, options = { delay: 50 }) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!target) {
        throw new Error('Must pass an element or selector to `typeIn`.');
      }

      const element = (0, _getElement.default)(target);
      if (!element) {
        throw new Error(`Element not found when calling \`typeIn('${target}')\``);
      }
      let isControl = (0, _isFormControl.default)(element);
      if (!isControl) {
        throw new Error('`typeIn` is only usable on form controls.');
      }

      if (typeof text === 'undefined' || text === null) {
        throw new Error('Must provide `text` when calling `typeIn`.');
      }

      (0, _focus.__focus__)(element);

      return fillOut(element, text, options.delay).then(() => (0, _fireEvent.default)(element, 'change')).then(_settled.default);
    });
  }

  // eslint-disable-next-line require-jsdoc
  function fillOut(element, text, delay) {
    const inputFunctions = text.split('').map(character => keyEntry(element, character, delay));
    return inputFunctions.reduce((currentPromise, func) => {
      return currentPromise.then(() => delayedExecute(func, delay));
    }, Ember.RSVP.Promise.resolve());
  }

  // eslint-disable-next-line require-jsdoc
  function keyEntry(element, character) {
    const charCode = character.charCodeAt();

    const eventOptions = {
      bubbles: true,
      cancellable: true,
      charCode
    };

    const keyEvents = {
      down: new KeyboardEvent('keydown', eventOptions),
      press: new KeyboardEvent('keypress', eventOptions),
      up: new KeyboardEvent('keyup', eventOptions)
    };

    return function () {
      element.dispatchEvent(keyEvents.down);
      element.dispatchEvent(keyEvents.press);
      element.value = element.value + character;
      (0, _fireEvent.default)(element, 'input');
      element.dispatchEvent(keyEvents.up);
    };
  }

  // eslint-disable-next-line require-jsdoc
  function delayedExecute(func, delay) {
    return new Ember.RSVP.Promise(resolve => {
      setTimeout(resolve, delay);
    }).then(func);
  }
});
define('@ember/test-helpers/dom/wait-for', ['exports', '@ember/test-helpers/wait-until', '@ember/test-helpers/dom/-get-element', '@ember/test-helpers/dom/-get-elements', '@ember/test-helpers/dom/-to-array', '@ember/test-helpers/-utils'], function (exports, _waitUntil, _getElement, _getElements, _toArray, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = waitFor;


  /**
    Used to wait for a particular selector to appear in the DOM. Due to the fact
    that it does not wait for general settledness, this is quite useful for testing
    interim DOM states (e.g. loading states, pending promises, etc).
  
    @param {string} selector the selector to wait for
    @param {Object} [options] the options to be used
    @param {number} [options.timeout=1000] the time to wait (in ms) for a match
    @param {number} [options.count=null] the number of elements that should match the provided selector (null means one or more)
    @return {Promise<Element|Element[]>} resolves when the element(s) appear on the page
  */
  function waitFor(selector, { timeout = 1000, count = null, timeoutMessage } = {}) {
    return (0, _utils.nextTickPromise)().then(() => {
      if (!selector) {
        throw new Error('Must pass a selector to `waitFor`.');
      }
      if (!timeoutMessage) {
        timeoutMessage = `waitFor timed out waiting for selector "${selector}"`;
      }

      let callback;
      if (count !== null) {
        callback = () => {
          let elements = (0, _getElements.default)(selector);
          if (elements.length === count) {
            return (0, _toArray.default)(elements);
          }
        };
      } else {
        callback = () => (0, _getElement.default)(selector);
      }
      return (0, _waitUntil.default)(callback, { timeout, timeoutMessage });
    });
  }
});
define('@ember/test-helpers/global', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = (() => {
    if (typeof self !== 'undefined') {
      return self;
    } else if (typeof window !== 'undefined') {
      return window;
    } else if (typeof global !== 'undefined') {
      return global;
    } else {
      return Function('return this')();
    }
  })();
});
define('@ember/test-helpers/has-ember-version', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = hasEmberVersion;


  /**
    Checks if the currently running Ember version is greater than or equal to the
    specified major and minor version numbers.
  
    @private
    @param {number} major the major version number to compare
    @param {number} minor the minor version number to compare
    @returns {boolean} true if the Ember version is >= MAJOR.MINOR specified, false otherwise
  */
  function hasEmberVersion(major, minor) {
    var numbers = Ember.VERSION.split('-')[0].split('.');
    var actualMajor = parseInt(numbers[0], 10);
    var actualMinor = parseInt(numbers[1], 10);
    return actualMajor > major || actualMajor === major && actualMinor >= minor;
  }
});
define('@ember/test-helpers/index', ['exports', '@ember/test-helpers/resolver', '@ember/test-helpers/application', '@ember/test-helpers/setup-context', '@ember/test-helpers/teardown-context', '@ember/test-helpers/setup-rendering-context', '@ember/test-helpers/teardown-rendering-context', '@ember/test-helpers/setup-application-context', '@ember/test-helpers/teardown-application-context', '@ember/test-helpers/settled', '@ember/test-helpers/wait-until', '@ember/test-helpers/validate-error-handler', '@ember/test-helpers/dom/click', '@ember/test-helpers/dom/double-click', '@ember/test-helpers/dom/tap', '@ember/test-helpers/dom/focus', '@ember/test-helpers/dom/blur', '@ember/test-helpers/dom/trigger-event', '@ember/test-helpers/dom/trigger-key-event', '@ember/test-helpers/dom/fill-in', '@ember/test-helpers/dom/wait-for', '@ember/test-helpers/dom/get-root-element', '@ember/test-helpers/dom/find', '@ember/test-helpers/dom/find-all', '@ember/test-helpers/dom/type-in'], function (exports, _resolver, _application, _setupContext, _teardownContext, _setupRenderingContext, _teardownRenderingContext, _setupApplicationContext, _teardownApplicationContext, _settled, _waitUntil, _validateErrorHandler, _click, _doubleClick, _tap, _focus, _blur, _triggerEvent, _triggerKeyEvent, _fillIn, _waitFor, _getRootElement, _find, _findAll, _typeIn) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'setResolver', {
    enumerable: true,
    get: function () {
      return _resolver.setResolver;
    }
  });
  Object.defineProperty(exports, 'getResolver', {
    enumerable: true,
    get: function () {
      return _resolver.getResolver;
    }
  });
  Object.defineProperty(exports, 'getApplication', {
    enumerable: true,
    get: function () {
      return _application.getApplication;
    }
  });
  Object.defineProperty(exports, 'setApplication', {
    enumerable: true,
    get: function () {
      return _application.setApplication;
    }
  });
  Object.defineProperty(exports, 'setupContext', {
    enumerable: true,
    get: function () {
      return _setupContext.default;
    }
  });
  Object.defineProperty(exports, 'getContext', {
    enumerable: true,
    get: function () {
      return _setupContext.getContext;
    }
  });
  Object.defineProperty(exports, 'setContext', {
    enumerable: true,
    get: function () {
      return _setupContext.setContext;
    }
  });
  Object.defineProperty(exports, 'unsetContext', {
    enumerable: true,
    get: function () {
      return _setupContext.unsetContext;
    }
  });
  Object.defineProperty(exports, 'pauseTest', {
    enumerable: true,
    get: function () {
      return _setupContext.pauseTest;
    }
  });
  Object.defineProperty(exports, 'resumeTest', {
    enumerable: true,
    get: function () {
      return _setupContext.resumeTest;
    }
  });
  Object.defineProperty(exports, 'teardownContext', {
    enumerable: true,
    get: function () {
      return _teardownContext.default;
    }
  });
  Object.defineProperty(exports, 'setupRenderingContext', {
    enumerable: true,
    get: function () {
      return _setupRenderingContext.default;
    }
  });
  Object.defineProperty(exports, 'render', {
    enumerable: true,
    get: function () {
      return _setupRenderingContext.render;
    }
  });
  Object.defineProperty(exports, 'clearRender', {
    enumerable: true,
    get: function () {
      return _setupRenderingContext.clearRender;
    }
  });
  Object.defineProperty(exports, 'teardownRenderingContext', {
    enumerable: true,
    get: function () {
      return _teardownRenderingContext.default;
    }
  });
  Object.defineProperty(exports, 'setupApplicationContext', {
    enumerable: true,
    get: function () {
      return _setupApplicationContext.default;
    }
  });
  Object.defineProperty(exports, 'visit', {
    enumerable: true,
    get: function () {
      return _setupApplicationContext.visit;
    }
  });
  Object.defineProperty(exports, 'currentRouteName', {
    enumerable: true,
    get: function () {
      return _setupApplicationContext.currentRouteName;
    }
  });
  Object.defineProperty(exports, 'currentURL', {
    enumerable: true,
    get: function () {
      return _setupApplicationContext.currentURL;
    }
  });
  Object.defineProperty(exports, 'teardownApplicationContext', {
    enumerable: true,
    get: function () {
      return _teardownApplicationContext.default;
    }
  });
  Object.defineProperty(exports, 'settled', {
    enumerable: true,
    get: function () {
      return _settled.default;
    }
  });
  Object.defineProperty(exports, 'isSettled', {
    enumerable: true,
    get: function () {
      return _settled.isSettled;
    }
  });
  Object.defineProperty(exports, 'getSettledState', {
    enumerable: true,
    get: function () {
      return _settled.getSettledState;
    }
  });
  Object.defineProperty(exports, 'waitUntil', {
    enumerable: true,
    get: function () {
      return _waitUntil.default;
    }
  });
  Object.defineProperty(exports, 'validateErrorHandler', {
    enumerable: true,
    get: function () {
      return _validateErrorHandler.default;
    }
  });
  Object.defineProperty(exports, 'click', {
    enumerable: true,
    get: function () {
      return _click.default;
    }
  });
  Object.defineProperty(exports, 'doubleClick', {
    enumerable: true,
    get: function () {
      return _doubleClick.default;
    }
  });
  Object.defineProperty(exports, 'tap', {
    enumerable: true,
    get: function () {
      return _tap.default;
    }
  });
  Object.defineProperty(exports, 'focus', {
    enumerable: true,
    get: function () {
      return _focus.default;
    }
  });
  Object.defineProperty(exports, 'blur', {
    enumerable: true,
    get: function () {
      return _blur.default;
    }
  });
  Object.defineProperty(exports, 'triggerEvent', {
    enumerable: true,
    get: function () {
      return _triggerEvent.default;
    }
  });
  Object.defineProperty(exports, 'triggerKeyEvent', {
    enumerable: true,
    get: function () {
      return _triggerKeyEvent.default;
    }
  });
  Object.defineProperty(exports, 'fillIn', {
    enumerable: true,
    get: function () {
      return _fillIn.default;
    }
  });
  Object.defineProperty(exports, 'waitFor', {
    enumerable: true,
    get: function () {
      return _waitFor.default;
    }
  });
  Object.defineProperty(exports, 'getRootElement', {
    enumerable: true,
    get: function () {
      return _getRootElement.default;
    }
  });
  Object.defineProperty(exports, 'find', {
    enumerable: true,
    get: function () {
      return _find.default;
    }
  });
  Object.defineProperty(exports, 'findAll', {
    enumerable: true,
    get: function () {
      return _findAll.default;
    }
  });
  Object.defineProperty(exports, 'typeIn', {
    enumerable: true,
    get: function () {
      return _typeIn.default;
    }
  });
});
define("@ember/test-helpers/resolver", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.setResolver = setResolver;
  exports.getResolver = getResolver;
  var __resolver__;

  /**
    Stores the provided resolver instance so that tests being ran can resolve
    objects in the same way as a normal application.
  
    Used by `setupContext` and `setupRenderingContext` as a fallback when `setApplication` was _not_ used.
  
    @public
    @param {Ember.Resolver} resolver the resolver to be used for testing
  */
  function setResolver(resolver) {
    __resolver__ = resolver;
  }

  /**
    Retrieve the resolver instance stored by `setResolver`.
  
    @public
    @returns {Ember.Resolver} the previously stored resolver
  */
  function getResolver() {
    return __resolver__;
  }
});
define('@ember/test-helpers/settled', ['exports', '@ember/test-helpers/-utils', '@ember/test-helpers/wait-until'], function (exports, _utils, _waitUntil) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports._teardownAJAXHooks = _teardownAJAXHooks;
  exports._setupAJAXHooks = _setupAJAXHooks;
  exports.getSettledState = getSettledState;
  exports.isSettled = isSettled;
  exports.default = settled;


  // Ember internally tracks AJAX requests in the same way that we do here for
  // legacy style "acceptance" tests using the `ember-testing.js` asset provided
  // by emberjs/ember.js itself. When `@ember/test-helpers`'s `settled` utility
  // is used in a legacy acceptance test context any pending AJAX requests are
  // not properly considered during the `isSettled` check below.
  //
  // This utilizes a local utility method present in Ember since around 2.8.0 to
  // properly consider pending AJAX requests done within legacy acceptance tests.
  const _internalPendingRequests = (() => {
    if (Ember.__loader.registry['ember-testing/test/pending_requests']) {
      // Ember <= 3.1
      return Ember.__loader.require('ember-testing/test/pending_requests').pendingRequests;
    } else if (Ember.__loader.registry['ember-testing/lib/test/pending_requests']) {
      // Ember >= 3.2
      return Ember.__loader.require('ember-testing/lib/test/pending_requests').pendingRequests;
    }

    return () => 0;
  })();

  let requests;

  /**
    @private
    @returns {number} the count of pending requests
  */
  function pendingRequests() {
    let localRequestsPending = requests !== undefined ? requests.length : 0;
    let internalRequestsPending = _internalPendingRequests();

    return localRequestsPending + internalRequestsPending;
  }

  /**
    @private
    @param {Event} event (unused)
    @param {XMLHTTPRequest} xhr the XHR that has initiated a request
  */
  function incrementAjaxPendingRequests(event, xhr) {
    requests.push(xhr);
  }

  /**
    @private
    @param {Event} event (unused)
    @param {XMLHTTPRequest} xhr the XHR that has initiated a request
  */
  function decrementAjaxPendingRequests(event, xhr) {
    // In most Ember versions to date (current version is 2.16) RSVP promises are
    // configured to flush in the actions queue of the Ember run loop, however it
    // is possible that in the future this changes to use "true" micro-task
    // queues.
    //
    // The entire point here, is that _whenever_ promises are resolved will be
    // before the next run of the JS event loop. Then in the next event loop this
    // counter will decrement. In the specific case of AJAX, this means that any
    // promises chained off of `$.ajax` will properly have their `.then` called
    // _before_ this is decremented (and testing continues)
    (0, _utils.nextTick)(() => {
      for (let i = 0; i < requests.length; i++) {
        if (xhr === requests[i]) {
          requests.splice(i, 1);
        }
      }
    }, 0);
  }

  /**
    Clears listeners that were previously setup for `ajaxSend` and `ajaxComplete`.
  
    @private
  */
  function _teardownAJAXHooks() {
    // jQuery will not invoke `ajaxComplete` if
    //    1. `transport.send` throws synchronously and
    //    2. it has an `error` option which also throws synchronously

    // We can no longer handle any remaining requests
    requests = [];

    if (!Ember.$) {
      return;
    }

    Ember.$(document).off('ajaxSend', incrementAjaxPendingRequests);
    Ember.$(document).off('ajaxComplete', decrementAjaxPendingRequests);
  }

  /**
    Sets up listeners for `ajaxSend` and `ajaxComplete`.
  
    @private
  */
  function _setupAJAXHooks() {
    requests = [];

    if (!Ember.$) {
      return;
    }

    Ember.$(document).on('ajaxSend', incrementAjaxPendingRequests);
    Ember.$(document).on('ajaxComplete', decrementAjaxPendingRequests);
  }

  let _internalCheckWaiters;
  if (Ember.__loader.registry['ember-testing/test/waiters']) {
    // Ember <= 3.1
    _internalCheckWaiters = Ember.__loader.require('ember-testing/test/waiters').checkWaiters;
  } else if (Ember.__loader.registry['ember-testing/lib/test/waiters']) {
    // Ember >= 3.2
    _internalCheckWaiters = Ember.__loader.require('ember-testing/lib/test/waiters').checkWaiters;
  }

  /**
    @private
    @returns {boolean} true if waiters are still pending
  */
  function checkWaiters() {
    if (_internalCheckWaiters) {
      return _internalCheckWaiters();
    } else if (Ember.Test.waiters) {
      if (Ember.Test.waiters.any(([context, callback]) => !callback.call(context))) {
        return true;
      }
    }

    return false;
  }

  /**
    Check various settledness metrics, and return an object with the following properties:
  
    - `hasRunLoop` - Checks if a run-loop has been started. If it has, this will
      be `true` otherwise it will be `false`.
    - `hasPendingTimers` - Checks if there are scheduled timers in the run-loop.
      These pending timers are primarily registered by `Ember.run.schedule`. If
      there are pending timers, this will be `true`, otherwise `false`.
    - `hasPendingWaiters` - Checks if any registered test waiters are still
      pending (e.g. the waiter returns `true`). If there are pending waiters,
      this will be `true`, otherwise `false`.
    - `hasPendingRequests` - Checks if there are pending AJAX requests (based on
      `ajaxSend` / `ajaxComplete` events triggered by `jQuery.ajax`). If there
      are pending requests, this will be `true`, otherwise `false`.
    - `pendingRequestCount` - The count of pending AJAX requests.
  
    @public
    @returns {Object} object with properties for each of the metrics used to determine settledness
  */
  function getSettledState() {
    let pendingRequestCount = pendingRequests();

    return {
      hasPendingTimers: Boolean(Ember.run.hasScheduledTimers()),
      hasRunLoop: Boolean(Ember.run.currentRunLoop),
      hasPendingWaiters: checkWaiters(),
      hasPendingRequests: pendingRequestCount > 0,
      pendingRequestCount
    };
  }

  /**
    Checks various settledness metrics (via `getSettledState()`) to determine if things are settled or not.
  
    Settled generally means that there are no pending timers, no pending waiters,
    no pending AJAX requests, and no current run loop. However, new settledness
    metrics may be added and used as they become available.
  
    @public
    @returns {boolean} `true` if settled, `false` otherwise
  */
  function isSettled() {
    let { hasPendingTimers, hasRunLoop, hasPendingRequests, hasPendingWaiters } = getSettledState();

    if (hasPendingTimers || hasRunLoop || hasPendingRequests || hasPendingWaiters) {
      return false;
    }

    return true;
  }

  /**
    Returns a promise that resolves when in a settled state (see `isSettled` for
    a definition of "settled state").
  
    @public
    @returns {Promise<void>} resolves when settled
  */
  function settled() {
    return (0, _waitUntil.default)(isSettled, { timeout: Infinity });
  }
});
define('@ember/test-helpers/setup-application-context', ['exports', '@ember/test-helpers/-utils', '@ember/test-helpers/setup-context', '@ember/test-helpers/has-ember-version', '@ember/test-helpers/settled'], function (exports, _utils, _setupContext, _hasEmberVersion, _settled) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.visit = visit;
  exports.currentRouteName = currentRouteName;
  exports.currentURL = currentURL;
  exports.default = setupApplicationContext;


  /**
    Navigate the application to the provided URL.
  
    @public
    @returns {Promise<void>} resolves when settled
  */
  function visit() {
    let context = (0, _setupContext.getContext)();
    let { owner } = context;

    return (0, _utils.nextTickPromise)().then(() => {
      return owner.visit(...arguments);
    }).then(() => {
      if (EmberENV._APPLICATION_TEMPLATE_WRAPPER !== false) {
        context.element = document.querySelector('#ember-testing > .ember-view');
      } else {
        context.element = document.querySelector('#ember-testing');
      }
    }).then(_settled.default);
  }

  /**
    @public
    @returns {string} the currently active route name
  */
  /* globals EmberENV */
  function currentRouteName() {
    let { owner } = (0, _setupContext.getContext)();
    let router = owner.lookup('router:main');
    return Ember.get(router, 'currentRouteName');
  }

  const HAS_CURRENT_URL_ON_ROUTER = (0, _hasEmberVersion.default)(2, 13);

  /**
    @public
    @returns {string} the applications current url
  */
  function currentURL() {
    let { owner } = (0, _setupContext.getContext)();
    let router = owner.lookup('router:main');

    if (HAS_CURRENT_URL_ON_ROUTER) {
      return Ember.get(router, 'currentURL');
    } else {
      return Ember.get(router, 'location').getURL();
    }
  }

  /**
    Used by test framework addons to setup the provided context for working with
    an application (e.g. routing).
  
    `setupContext` must have been run on the provided context prior to calling
    `setupApplicationContext`.
  
    Sets up the basic framework used by application tests.
  
    @public
    @param {Object} context the context to setup
    @returns {Promise<Object>} resolves with the context that was setup
  */
  function setupApplicationContext() {
    return (0, _utils.nextTickPromise)();
  }
});
define('@ember/test-helpers/setup-context', ['exports', '@ember/test-helpers/build-owner', '@ember/test-helpers/settled', '@ember/test-helpers/global', '@ember/test-helpers/resolver', '@ember/test-helpers/application', '@ember/test-helpers/-utils'], function (exports, _buildOwner, _settled, _global, _resolver, _application, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.CLEANUP = undefined;
  exports.setContext = setContext;
  exports.getContext = getContext;
  exports.unsetContext = unsetContext;
  exports.pauseTest = pauseTest;
  exports.resumeTest = resumeTest;

  exports.default = function (context, options = {}) {
    Ember.testing = true;
    setContext(context);

    let contextGuid = Ember.guidFor(context);
    CLEANUP[contextGuid] = [];

    return (0, _utils.nextTickPromise)().then(() => {
      let application = (0, _application.getApplication)();
      if (application) {
        return application.boot();
      }
    }).then(() => {
      let testElementContainer = document.getElementById('ember-testing-container');
      let fixtureResetValue = testElementContainer.innerHTML;

      // push this into the final cleanup bucket, to be ran _after_ the owner
      // is destroyed and settled (e.g. flushed run loops, etc)
      CLEANUP[contextGuid].push(() => {
        testElementContainer.innerHTML = fixtureResetValue;
      });

      let { resolver } = options;

      // This handles precendence, specifying a specific option of
      // resolver always trumps whatever is auto-detected, then we fallback to
      // the suite-wide registrations
      //
      // At some later time this can be extended to support specifying a custom
      // engine or application...
      if (resolver) {
        return (0, _buildOwner.default)(null, resolver);
      }

      return (0, _buildOwner.default)((0, _application.getApplication)(), (0, _resolver.getResolver)());
    }).then(owner => {
      Object.defineProperty(context, 'owner', {
        configurable: true,
        enumerable: true,
        value: owner,
        writable: false
      });

      Object.defineProperty(context, 'set', {
        configurable: true,
        enumerable: true,
        value(key, value) {
          let ret = Ember.run(function () {
            return Ember.set(context, key, value);
          });

          return ret;
        },
        writable: false
      });

      Object.defineProperty(context, 'setProperties', {
        configurable: true,
        enumerable: true,
        value(hash) {
          let ret = Ember.run(function () {
            return Ember.setProperties(context, hash);
          });

          return ret;
        },
        writable: false
      });

      Object.defineProperty(context, 'get', {
        configurable: true,
        enumerable: true,
        value(key) {
          return Ember.get(context, key);
        },
        writable: false
      });

      Object.defineProperty(context, 'getProperties', {
        configurable: true,
        enumerable: true,
        value(...args) {
          return Ember.getProperties(context, args);
        },
        writable: false
      });

      let resume;
      context.resumeTest = function resumeTest() {
        (true && !(resume) && Ember.assert('Testing has not been paused. There is nothing to resume.', resume));

        resume();
        _global.default.resumeTest = resume = undefined;
      };

      context.pauseTest = function pauseTest() {
        console.info('Testing paused. Use `resumeTest()` to continue.'); // eslint-disable-line no-console

        return new Ember.RSVP.Promise(resolve => {
          resume = resolve;
          _global.default.resumeTest = resumeTest;
        }, 'TestAdapter paused promise');
      };

      (0, _settled._setupAJAXHooks)();

      return context;
    });
  };

  let __test_context__;

  /**
    Stores the provided context as the "global testing context".
  
    Generally setup automatically by `setupContext`.
  
    @public
    @param {Object} context the context to use
  */
  function setContext(context) {
    __test_context__ = context;
  }

  /**
    Retrive the "global testing context" as stored by `setContext`.
  
    @public
    @returns {Object} the previously stored testing context
  */
  function getContext() {
    return __test_context__;
  }

  /**
    Clear the "global testing context".
  
    Generally invoked from `teardownContext`.
  
    @public
  */
  function unsetContext() {
    __test_context__ = undefined;
  }

  /**
   * Returns a promise to be used to pauses the current test (due to being
   * returned from the test itself).  This is useful for debugging while testing
   * or for test-driving.  It allows you to inspect the state of your application
   * at any point.
   *
   * The test framework wrapper (e.g. `ember-qunit` or `ember-mocha`) should
   * ensure that when `pauseTest()` is used, any framework specific test timeouts
   * are disabled.
   *
   * @public
   * @returns {Promise<void>} resolves _only_ when `resumeTest()` is invoked
   * @example <caption>Usage via ember-qunit</caption>
   *
   * import { setupRenderingTest } from 'ember-qunit';
   * import { render, click, pauseTest } from '@ember/test-helpers';
   *
   *
   * module('awesome-sauce', function(hooks) {
   *   setupRenderingTest(hooks);
   *
   *   test('does something awesome', async function(assert) {
   *     await render(hbs`{{awesome-sauce}}`);
   *
   *     // added here to visualize / interact with the DOM prior
   *     // to the interaction below
   *     await pauseTest();
   *
   *     click('.some-selector');
   *
   *     assert.equal(this.element.textContent, 'this sauce is awesome!');
   *   });
   * });
   */
  function pauseTest() {
    let context = getContext();

    if (!context || typeof context.pauseTest !== 'function') {
      throw new Error('Cannot call `pauseTest` without having first called `setupTest` or `setupRenderingTest`.');
    }

    return context.pauseTest();
  }

  /**
    Resumes a test previously paused by `await pauseTest()`.
  
    @public
  */
  function resumeTest() {
    let context = getContext();

    if (!context || typeof context.resumeTest !== 'function') {
      throw new Error('Cannot call `resumeTest` without having first called `setupTest` or `setupRenderingTest`.');
    }

    context.resumeTest();
  }

  const CLEANUP = exports.CLEANUP = Object.create(null);

  /**
    Used by test framework addons to setup the provided context for testing.
  
    Responsible for:
  
    - sets the "global testing context" to the provided context (`setContext`)
    - create an owner object and set it on the provided context (e.g. `this.owner`)
    - setup `this.set`, `this.setProperties`, `this.get`, and `this.getProperties` to the provided context
    - setting up AJAX listeners
    - setting up `pauseTest` (also available as `this.pauseTest()`) and `resumeTest` helpers
  
    @public
    @param {Object} context the context to setup
    @param {Object} [options] options used to override defaults
    @param {Resolver} [options.resolver] a resolver to use for customizing normal resolution
    @returns {Promise<Object>} resolves with the context that was setup
  */
});
define('@ember/test-helpers/setup-rendering-context', ['exports', '@ember/test-helpers/global', '@ember/test-helpers/setup-context', '@ember/test-helpers/-utils', '@ember/test-helpers/settled', '@ember/test-helpers/dom/get-root-element'], function (exports, _global, _setupContext, _utils, _settled, _getRootElement) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.RENDERING_CLEANUP = undefined;
  exports.render = render;
  exports.clearRender = clearRender;
  exports.default = setupRenderingContext;
  /* globals EmberENV */
  const RENDERING_CLEANUP = exports.RENDERING_CLEANUP = Object.create(null);
  const OUTLET_TEMPLATE = Ember.HTMLBars.template({
    "id": "em3WhaQV",
    "block": "{\"symbols\":[],\"statements\":[[1,[21,\"outlet\"],false]],\"hasEval\":false}",
    "meta": {}
  });
  const EMPTY_TEMPLATE = Ember.HTMLBars.template({
    "id": "xOcW61lH",
    "block": "{\"symbols\":[],\"statements\":[],\"hasEval\":false}",
    "meta": {}
  });

  /**
    @private
    @param {Ember.ApplicationInstance} owner the current owner instance
    @returns {Template} a template representing {{outlet}}
  */
  function lookupOutletTemplate(owner) {
    let OutletTemplate = owner.lookup('template:-outlet');
    if (!OutletTemplate) {
      owner.register('template:-outlet', OUTLET_TEMPLATE);
      OutletTemplate = owner.lookup('template:-outlet');
    }

    return OutletTemplate;
  }

  /**
    @private
    @param {string} [selector] the selector to search for relative to element
    @returns {jQuery} a jQuery object representing the selector (or element itself if no selector)
  */
  function jQuerySelector(selector) {
    let { element } = (0, _setupContext.getContext)();

    // emulates Ember internal behavor of `this.$` in a component
    // https://github.com/emberjs/ember.js/blob/v2.5.1/packages/ember-views/lib/views/states/has_element.js#L18
    return selector ? _global.default.jQuery(selector, element) : _global.default.jQuery(element);
  }

  let templateId = 0;
  /**
    Renders the provided template and appends it to the DOM.
  
    @public
    @param {CompiledTemplate} template the template to render
    @returns {Promise<void>} resolves when settled
  */
  function render(template) {
    let context = (0, _setupContext.getContext)();

    if (!template) {
      throw new Error('you must pass a template to `render()`');
    }

    return (0, _utils.nextTickPromise)().then(() => {
      let { owner } = context;

      let toplevelView = owner.lookup('-top-level-view:main');
      let OutletTemplate = lookupOutletTemplate(owner);
      templateId += 1;
      let templateFullName = `template:-undertest-${templateId}`;
      owner.register(templateFullName, template);

      let outletState = {
        render: {
          owner,
          into: undefined,
          outlet: 'main',
          name: 'application',
          controller: undefined,
          ViewClass: undefined,
          template: OutletTemplate
        },

        outlets: {
          main: {
            render: {
              owner,
              into: undefined,
              outlet: 'main',
              name: 'index',
              controller: context,
              ViewClass: undefined,
              template: owner.lookup(templateFullName),
              outlets: {}
            },
            outlets: {}
          }
        }
      };
      toplevelView.setOutletState(outletState);

      // returning settled here because the actual rendering does not happen until
      // the renderer detects it is dirty (which happens on backburner's end
      // hook), see the following implementation details:
      //
      // * [view:outlet](https://github.com/emberjs/ember.js/blob/f94a4b6aef5b41b96ef2e481f35e07608df01440/packages/ember-glimmer/lib/views/outlet.js#L129-L145) manually dirties its own tag upon `setOutletState`
      // * [backburner's custom end hook](https://github.com/emberjs/ember.js/blob/f94a4b6aef5b41b96ef2e481f35e07608df01440/packages/ember-glimmer/lib/renderer.js#L145-L159) detects that the current revision of the root is no longer the latest, and triggers a new rendering transaction
      return (0, _settled.default)();
    });
  }

  /**
    Clears any templates previously rendered. This is commonly used for
    confirming behavior that is triggered by teardown (e.g.
    `willDestroyElement`).
  
    @public
    @returns {Promise<void>} resolves when settled
  */
  function clearRender() {
    let context = (0, _setupContext.getContext)();

    if (!context || typeof context.clearRender !== 'function') {
      throw new Error('Cannot call `clearRender` without having first called `setupRenderingContext`.');
    }

    return render(EMPTY_TEMPLATE);
  }

  /**
    Used by test framework addons to setup the provided context for rendering.
  
    `setupContext` must have been ran on the provided context
    prior to calling `setupRenderingContext`.
  
    Responsible for:
  
    - Setup the basic framework used for rendering by the
      `render` helper.
    - Ensuring the event dispatcher is properly setup.
    - Setting `this.element` to the root element of the testing
      container (things rendered via `render` will go _into_ this
      element).
  
    @public
    @param {Object} context the context to setup for rendering
    @returns {Promise<Object>} resolves with the context that was setup
  */
  function setupRenderingContext(context) {
    let contextGuid = Ember.guidFor(context);
    RENDERING_CLEANUP[contextGuid] = [];

    return (0, _utils.nextTickPromise)().then(() => {
      let { owner } = context;

      // these methods being placed on the context itself will be deprecated in
      // a future version (no giant rush) to remove some confusion about which
      // is the "right" way to things...
      Object.defineProperty(context, 'render', {
        configurable: true,
        enumerable: true,
        value: render,
        writable: false
      });
      Object.defineProperty(context, 'clearRender', {
        configurable: true,
        enumerable: true,
        value: clearRender,
        writable: false
      });

      if (_global.default.jQuery) {
        Object.defineProperty(context, '$', {
          configurable: true,
          enumerable: true,
          value: jQuerySelector,
          writable: false
        });
      }

      // When the host app uses `setApplication` (instead of `setResolver`) the event dispatcher has
      // already been setup via `applicationInstance.boot()` in `./build-owner`. If using
      // `setResolver` (instead of `setApplication`) a "mock owner" is created by extending
      // `Ember._ContainerProxyMixin` and `Ember._RegistryProxyMixin` in this scenario we need to
      // manually start the event dispatcher.
      if (owner._emberTestHelpersMockOwner) {
        let dispatcher = owner.lookup('event_dispatcher:main') || Ember.EventDispatcher.create();
        dispatcher.setup({}, '#ember-testing');
      }

      let OutletView = owner.factoryFor ? owner.factoryFor('view:-outlet') : owner._lookupFactory('view:-outlet');
      let toplevelView = OutletView.create();

      owner.register('-top-level-view:main', {
        create() {
          return toplevelView;
        }
      });

      // initially render a simple empty template
      return render(EMPTY_TEMPLATE).then(() => {
        Ember.run(toplevelView, 'appendTo', (0, _getRootElement.default)());

        return (0, _settled.default)();
      });
    }).then(() => {
      Object.defineProperty(context, 'element', {
        configurable: true,
        enumerable: true,
        // ensure the element is based on the wrapping toplevel view
        // Ember still wraps the main application template with a
        // normal tagged view
        //
        // In older Ember versions (2.4) the element itself is not stable,
        // and therefore we cannot update the `this.element` until after the
        // rendering is completed
        value: EmberENV._APPLICATION_TEMPLATE_WRAPPER !== false ? (0, _getRootElement.default)().querySelector('.ember-view') : (0, _getRootElement.default)(),

        writable: false
      });

      return context;
    });
  }
});
define('@ember/test-helpers/teardown-application-context', ['exports', '@ember/test-helpers/settled'], function (exports, _settled) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function () {
    return (0, _settled.default)();
  };
});
define('@ember/test-helpers/teardown-context', ['exports', '@ember/test-helpers/settled', '@ember/test-helpers/setup-context', '@ember/test-helpers/-utils'], function (exports, _settled, _setupContext, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = teardownContext;


  /**
    Used by test framework addons to tear down the provided context after testing is completed.
  
    Responsible for:
  
    - un-setting the "global testing context" (`unsetContext`)
    - destroy the contexts owner object
    - remove AJAX listeners
  
    @public
    @param {Object} context the context to setup
    @returns {Promise<void>} resolves when settled
  */
  function teardownContext(context) {
    return (0, _utils.nextTickPromise)().then(() => {
      let { owner } = context;

      (0, _settled._teardownAJAXHooks)();

      Ember.run(owner, 'destroy');
      Ember.testing = false;

      (0, _setupContext.unsetContext)();

      return (0, _settled.default)();
    }).finally(() => {
      let contextGuid = Ember.guidFor(context);

      (0, _utils.runDestroyablesFor)(_setupContext.CLEANUP, contextGuid);

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/teardown-rendering-context', ['exports', '@ember/test-helpers/setup-rendering-context', '@ember/test-helpers/-utils', '@ember/test-helpers/settled'], function (exports, _setupRenderingContext, _utils, _settled) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = teardownRenderingContext;


  /**
    Used by test framework addons to tear down the provided context after testing is completed.
  
    Responsible for:
  
    - resetting the `ember-testing-container` to its original state (the value
      when `setupRenderingContext` was called).
  
    @public
    @param {Object} context the context to setup
    @returns {Promise<void>} resolves when settled
  */
  function teardownRenderingContext(context) {
    return (0, _utils.nextTickPromise)().then(() => {
      let contextGuid = Ember.guidFor(context);

      (0, _utils.runDestroyablesFor)(_setupRenderingContext.RENDERING_CLEANUP, contextGuid);

      return (0, _settled.default)();
    });
  }
});
define('@ember/test-helpers/validate-error-handler', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = validateErrorHandler;

  const VALID = Object.freeze({ isValid: true, message: null });
  const INVALID = Object.freeze({
    isValid: false,
    message: 'error handler should have re-thrown the provided error'
  });

  /**
   * Validate the provided error handler to confirm that it properly re-throws
   * errors when `Ember.testing` is true.
   *
   * This is intended to be used by test framework hosts (or other libraries) to
   * ensure that `Ember.onerror` is properly configured. Without a check like
   * this, `Ember.onerror` could _easily_ swallow all errors and make it _seem_
   * like everything is just fine (and have green tests) when in reality
   * everything is on fire...
   *
   * @public
   * @param {Function} [callback=Ember.onerror] the callback to validate
   * @returns {Object} object with `isValid` and `message`
   *
   * @example <caption>Example implementation for `ember-qunit`</caption>
   *
   * import { validateErrorHandler } from '@ember/test-helpers';
   *
   * test('Ember.onerror is functioning properly', function(assert) {
   *   let result = validateErrorHandler();
   *   assert.ok(result.isValid, result.message);
   * });
   */
  function validateErrorHandler(callback = Ember.onerror) {
    if (callback === undefined || callback === null) {
      return VALID;
    }

    let error = new Error('Error handler validation error!');

    let originalEmberTesting = Ember.testing;
    Ember.testing = true;
    try {
      callback(error);
    } catch (e) {
      if (e === error) {
        return VALID;
      }
    } finally {
      Ember.testing = originalEmberTesting;
    }

    return INVALID;
  }
});
define('@ember/test-helpers/wait-until', ['exports', '@ember/test-helpers/-utils'], function (exports, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = waitUntil;


  const TIMEOUTS = [0, 1, 2, 5, 7];
  const MAX_TIMEOUT = 10;

  /**
    Wait for the provided callback to return a truthy value.
  
    This does not leverage `settled()`, and as such can be used to manage async
    while _not_ settled (e.g. "loading" or "pending" states).
  
    @public
    @param {Function} callback the callback to use for testing when waiting should stop
    @param {Object} [options] options used to override defaults
    @param {number} [options.timeout=1000] the maximum amount of time to wait
    @param {string} [options.timeoutMessage='waitUntil timed out'] the message to use in the reject on timeout
    @returns {Promise} resolves with the callback value when it returns a truthy value
  */
  function waitUntil(callback, options = {}) {
    let timeout = 'timeout' in options ? options.timeout : 1000;
    let timeoutMessage = 'timeoutMessage' in options ? options.timeoutMessage : 'waitUntil timed out';

    // creating this error eagerly so it has the proper invocation stack
    let waitUntilTimedOut = new Error(timeoutMessage);

    return new Ember.RSVP.Promise(function (resolve, reject) {
      let time = 0;

      // eslint-disable-next-line require-jsdoc
      function scheduleCheck(timeoutsIndex) {
        let interval = TIMEOUTS[timeoutsIndex];
        if (interval === undefined) {
          interval = MAX_TIMEOUT;
        }

        (0, _utils.futureTick)(function () {
          time += interval;

          let value;
          try {
            value = callback();
          } catch (error) {
            reject(error);
          }

          if (value) {
            resolve(value);
          } else if (time < timeout) {
            scheduleCheck(timeoutsIndex + 1);
          } else {
            reject(waitUntilTimedOut);
          }
        }, interval);
      }

      scheduleCheck(0);
    });
  }
});
define("ember-basic-dropdown/test-support/helpers", ["exports", "@ember/test-helpers"], function (_exports, _testHelpers) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.nativeTap = nativeTap;
  _exports.clickTrigger = clickTrigger;
  _exports.tapTrigger = tapTrigger;
  _exports.fireKeydown = fireKeydown;
  _exports.default = _default;

  function nativeTap(selector, options = {}) {
    let touchStartEvent = new window.Event('touchstart', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    Object.keys(options).forEach(key => touchStartEvent[key] = options[key]);
    Ember.run(() => document.querySelector(selector).dispatchEvent(touchStartEvent));
    let touchEndEvent = new window.Event('touchend', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    Object.keys(options).forEach(key => touchEndEvent[key] = options[key]);
    Ember.run(() => document.querySelector(selector).dispatchEvent(touchEndEvent));
  }

  function clickTrigger(scope, options = {}) {
    let selector = '.ember-basic-dropdown-trigger';

    if (scope) {
      let element = document.querySelector(scope);

      if (element.classList.contains('ember-basic-dropdown-trigger')) {
        selector = scope;
      } else {
        selector = scope + ' ' + selector;
      }
    }

    (0, _testHelpers.click)(selector, options);
    return (0, _testHelpers.settled)();
  }

  function tapTrigger(scope, options = {}) {
    let selector = '.ember-basic-dropdown-trigger';

    if (scope) {
      selector = scope + ' ' + selector;
    }

    nativeTap(selector, options);
  }

  function fireKeydown(selector, k) {
    let oEvent = document.createEvent('Events');
    oEvent.initEvent('keydown', true, true);
    Ember.merge(oEvent, {
      view: window,
      ctrlKey: false,
      altKey: false,
      shiftKey: false,
      metaKey: false,
      keyCode: k,
      charCode: k
    });
    Ember.run(() => document.querySelector(selector).dispatchEvent(oEvent));
  } // acceptance helpers


  function _default() {
    Ember.Test.registerAsyncHelper('clickDropdown', function (app, cssPath, options = {}) {
      (true && !(false) && Ember.deprecate('Using the global `clickDropdown` acceptance helper from ember-basic-dropdown is deprecated. Please, explicitly import the `clickTrigger` or just use `click` helper from `@ember/test-helpers`.', false, {
        until: '1.0.0',
        id: 'ember-basic-dropdown-click-dropdown'
      }));
      clickTrigger(cssPath, options);
    });
    Ember.Test.registerAsyncHelper('tapDropdown', function (app, cssPath, options = {}) {
      (true && !(false) && Ember.deprecate('Using the global `tapDropdown` acceptance helper from ember-basic-dropdown is deprecated. Please, explicitly import the `tapTrigger` or just use `tap` helper from `@ember/test-helpers`.', false, {
        until: '1.0.0',
        id: 'ember-basic-dropdown-click-dropdown'
      }));
      tapTrigger(cssPath, options);
    });
  }
});
define('ember-cli-qunit', ['exports', 'ember-qunit'], function (exports, _emberQunit) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'TestLoader', {
    enumerable: true,
    get: function () {
      return _emberQunit.TestLoader;
    }
  });
  Object.defineProperty(exports, 'setupTestContainer', {
    enumerable: true,
    get: function () {
      return _emberQunit.setupTestContainer;
    }
  });
  Object.defineProperty(exports, 'loadTests', {
    enumerable: true,
    get: function () {
      return _emberQunit.loadTests;
    }
  });
  Object.defineProperty(exports, 'startTests', {
    enumerable: true,
    get: function () {
      return _emberQunit.startTests;
    }
  });
  Object.defineProperty(exports, 'setupTestAdapter', {
    enumerable: true,
    get: function () {
      return _emberQunit.setupTestAdapter;
    }
  });
  Object.defineProperty(exports, 'start', {
    enumerable: true,
    get: function () {
      return _emberQunit.start;
    }
  });
});
define('ember-cli-test-loader/test-support/index', ['exports'], function (exports) {
  /* globals requirejs, require */
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.addModuleIncludeMatcher = addModuleIncludeMatcher;
  exports.addModuleExcludeMatcher = addModuleExcludeMatcher;
  let moduleIncludeMatchers = [];
  let moduleExcludeMatchers = [];

  function addModuleIncludeMatcher(fn) {
    moduleIncludeMatchers.push(fn);
  }

  function addModuleExcludeMatcher(fn) {
    moduleExcludeMatchers.push(fn);
  }

  function checkMatchers(matchers, moduleName) {
    return matchers.some(matcher => matcher(moduleName));
  }

  class TestLoader {
    static load() {
      new TestLoader().loadModules();
    }

    constructor() {
      this._didLogMissingUnsee = false;
    }

    shouldLoadModule(moduleName) {
      return moduleName.match(/[-_]test$/);
    }

    listModules() {
      return Object.keys(requirejs.entries);
    }

    listTestModules() {
      let moduleNames = this.listModules();
      let testModules = [];
      let moduleName;

      for (let i = 0; i < moduleNames.length; i++) {
        moduleName = moduleNames[i];

        if (checkMatchers(moduleExcludeMatchers, moduleName)) {
          continue;
        }

        if (checkMatchers(moduleIncludeMatchers, moduleName) || this.shouldLoadModule(moduleName)) {
          testModules.push(moduleName);
        }
      }

      return testModules;
    }

    loadModules() {
      let testModules = this.listTestModules();
      let testModule;

      for (let i = 0; i < testModules.length; i++) {
        testModule = testModules[i];
        this.require(testModule);
        this.unsee(testModule);
      }
    }

    require(moduleName) {
      try {
        require(moduleName);
      } catch (e) {
        this.moduleLoadFailure(moduleName, e);
      }
    }

    unsee(moduleName) {
      if (typeof require.unsee === 'function') {
        require.unsee(moduleName);
      } else if (!this._didLogMissingUnsee) {
        this._didLogMissingUnsee = true;
        if (typeof console !== 'undefined') {
          console.warn('unable to require.unsee, please upgrade loader.js to >= v3.3.0');
        }
      }
    }

    moduleLoadFailure(moduleName, error) {
      console.error('Error loading: ' + moduleName, error.stack);
    }
  }exports.default = TestLoader;
  ;
});
define("ember-paypal-button/test-support/mock", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = mock;
  exports.reset = reset;
  function mock({
    window = window,
    render = () => {}
  } = {}) {
    window.paypal = {
      Button: {
        render
      }
    };
  }

  function reset({
    window = window
  }) {
    delete window.paypal;
  }
});
define('ember-power-select/test-support/helpers', ['exports', '@ember/test-helpers', 'ember-power-select/test-support/index'], function (exports, _testHelpers, _index) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.findContains = findContains;
  exports.nativeMouseDown = nativeMouseDown;
  exports.nativeMouseUp = nativeMouseUp;
  exports.triggerKeydown = triggerKeydown;
  exports.typeInSearch = typeInSearch;
  exports.clickTrigger = clickTrigger;
  exports.nativeTouch = nativeTouch;
  exports.touchTrigger = touchTrigger;
  exports.selectChoose = selectChoose;
  exports.selectSearch = selectSearch;
  exports.removeMultipleOption = removeMultipleOption;
  exports.clearSelected = clearSelected;

  exports.default = function () {
    Ember.Test.registerAsyncHelper('selectChoose', function (_, cssPathOrTrigger, valueOrSelector, optionIndex) {
      (true && !(true) && Ember.deprecate('Using the implicit global async helper `selectChoose` is deprecated. Please, import it explicitly with `import { selectChoose } from "ember-power-select/test-support"`', true, { id: 'ember-power-select-global-select-choose', until: '2.0.0' }));

      return (0, _index.selectChoose)(cssPathOrTrigger, valueOrSelector, optionIndex);
    });

    Ember.Test.registerAsyncHelper('selectSearch', async function (app, cssPathOrTrigger, value) {
      (true && !(true) && Ember.deprecate('Using the implicit global async helper `selectSearch` is deprecated. Please, import it explicitly with `import { selectSearch } from "ember-power-select/test-support"`', true, { id: 'ember-power-select-global-select-search', until: '2.0.0' }));

      return (0, _index.selectSearch)(cssPathOrTrigger, value);
    });

    Ember.Test.registerAsyncHelper('removeMultipleOption', async function (app, cssPath, value) {
      (true && !(true) && Ember.deprecate('Using the implicit global async helper `removeMultipleOption` is deprecated. Please, import it explicitly with `import { removeMultipleOption } from "ember-power-select/test-support"`', true, { id: 'ember-power-select-global-remove-multiple-option', until: '2.0.0' }));

      return (0, _index.removeMultipleOption)(cssPath, value);
    });

    Ember.Test.registerAsyncHelper('clearSelected', async function (app, cssPath) {
      (true && !(true) && Ember.deprecate('Using the implicit global async helper `clearSelected` is deprecated. Please, import it explicitly with `import { clearSelected } from "ember-power-select/test-support"`', true, { id: 'ember-power-select-global-clear-selected', until: '2.0.0' }));

      return (0, _index.clearSelected)(cssPath);
    });
  };

  /**
   * @private
   * @param {String} selector CSS3 selector of the elements to check the content
   * @param {String} text Substring that the selected element must contain
   * @returns HTMLElement The first element that maches the given selector and contains the
   *                      given text
   */
  function findContains(selector, text) {
    return [].slice.apply(document.querySelectorAll(selector)).filter(e => {
      return e.textContent.trim().indexOf(text) > -1;
    })[0];
  }

  async function nativeMouseDown(selectorOrDomElement, options) {
    return (0, _testHelpers.triggerEvent)(selectorOrDomElement, 'mousedown', options);
  }

  async function nativeMouseUp(selectorOrDomElement, options) {
    return (0, _testHelpers.triggerEvent)(selectorOrDomElement, 'mouseup', options);
  }

  async function triggerKeydown(domElement, k) {
    return (0, _testHelpers.triggerKeyEvent)(domElement, 'keydown', k);
  }

  function typeInSearch(scopeOrText, text) {
    let scope = '';

    if (typeof text === 'undefined') {
      text = scopeOrText;
    } else {
      scope = scopeOrText;
    }

    let selectors = ['.ember-power-select-search-input', '.ember-power-select-search input', '.ember-power-select-trigger-multiple-input', 'input[type="search"]'].map(selector => `${scope} ${selector}`).join(', ');

    return (0, _testHelpers.fillIn)(selectors, text);
  }

  async function clickTrigger(scope, options = {}) {
    let selector = '.ember-power-select-trigger';
    if (scope) {
      selector = `${scope} ${selector}`;
    }
    return (0, _testHelpers.click)(selector, options);
  }

  async function nativeTouch(selectorOrDomElement) {
    (0, _testHelpers.triggerEvent)(selectorOrDomElement, 'touchstart');
    return (0, _testHelpers.triggerEvent)(selectorOrDomElement, 'touchend');
  }

  async function touchTrigger() {
    return nativeTouch('.ember-power-select-trigger');
  }

  async function selectChoose(cssPathOrTrigger, valueOrSelector, optionIndex) {
    return (0, _index.selectChoose)(cssPathOrTrigger, valueOrSelector, optionIndex);
  }

  async function selectSearch(cssPathOrTrigger, value) {
    return (0, _index.selectSearch)(cssPathOrTrigger, value);
  }

  async function removeMultipleOption(cssPath, value) {
    return (0, _index.removeMultipleOption)(cssPath, value);
  }

  async function clearSelected(cssPath) {
    return (0, _index.clearSelected)(cssPath);
  }

  // Helpers for acceptance tests
});
define('ember-power-select/test-support/index', ['exports', '@ember/test-helpers'], function (exports, _testHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.selectChoose = selectChoose;
  exports.selectSearch = selectSearch;
  exports.removeMultipleOption = removeMultipleOption;
  exports.clearSelected = clearSelected;
  async function selectChoose(cssPathOrTrigger, valueOrSelector, optionIndex) {
    let trigger, target;
    if (cssPathOrTrigger instanceof HTMLElement) {
      if (cssPathOrTrigger.classList.contains('ember-power-select-trigger')) {
        trigger = cssPathOrTrigger;
      } else {
        trigger = cssPathOrTrigger.querySelector('.ember-power-select-trigger');
      }
    } else {
      trigger = document.querySelector(`${cssPathOrTrigger} .ember-power-select-trigger`);

      if (!trigger) {
        trigger = document.querySelector(cssPathOrTrigger);
      }

      if (!trigger) {
        throw new Error(`You called "selectChoose('${cssPathOrTrigger}', '${valueOrSelector}')" but no select was found using selector "${cssPathOrTrigger}"`);
      }
    }

    if (trigger.scrollIntoView) {
      trigger.scrollIntoView();
    }

    let contentId = `${trigger.attributes['aria-owns'].value}`;
    let content = document.querySelector(`#${contentId}`);
    // If the dropdown is closed, open it
    if (!content || content.classList.contains('ember-basic-dropdown-content-placeholder')) {
      await (0, _testHelpers.click)(trigger);
      await (0, _testHelpers.settled)();
    }

    // Select the option with the given text
    let options = document.querySelectorAll(`#${contentId} .ember-power-select-option`);
    let potentialTargets = [].slice.apply(options).filter(opt => opt.textContent.indexOf(valueOrSelector) > -1);
    if (potentialTargets.length === 0) {
      potentialTargets = document.querySelectorAll(`#${contentId} ${valueOrSelector}`);
    }
    if (potentialTargets.length > 1) {
      let filteredTargets = [].slice.apply(potentialTargets).filter(t => t.textContent.trim() === valueOrSelector);
      if (optionIndex === undefined) {
        target = filteredTargets[0] || potentialTargets[0];
      } else {
        target = filteredTargets[optionIndex] || potentialTargets[optionIndex];
      }
    } else {
      target = potentialTargets[0];
    }
    if (!target) {
      throw new Error(`You called "selectChoose('${cssPathOrTrigger}', '${valueOrSelector}')" but "${valueOrSelector}" didn't match any option`);
    }
    await (0, _testHelpers.click)(target);
    return (0, _testHelpers.settled)();
  }

  async function selectSearch(cssPathOrTrigger, value) {
    let trigger;
    if (cssPathOrTrigger instanceof HTMLElement) {
      trigger = cssPathOrTrigger;
    } else {
      let triggerPath = `${cssPathOrTrigger} .ember-power-select-trigger`;
      trigger = document.querySelector(triggerPath);
      if (!trigger) {
        triggerPath = cssPathOrTrigger;
        trigger = document.querySelector(triggerPath);
      }

      if (!trigger) {
        throw new Error(`You called "selectSearch('${cssPathOrTrigger}', '${value}')" but no select was found using selector "${cssPathOrTrigger}"`);
      }
    }

    if (trigger.scrollIntoView) {
      trigger.scrollIntoView();
    }

    let contentId = `${trigger.attributes['aria-owns'].value}`;
    let isMultipleSelect = !!trigger.querySelector('.ember-power-select-trigger-multiple-input');

    let content = document.querySelector(`#${contentId}`);
    let dropdownIsClosed = !content || content.classList.contains('ember-basic-dropdown-content-placeholder');
    if (dropdownIsClosed) {
      await (0, _testHelpers.click)(trigger);
      await (0, _testHelpers.settled)();
    }
    let isDefaultSingleSelect = !!document.querySelector('.ember-power-select-search-input');

    if (isMultipleSelect) {
      await (0, _testHelpers.fillIn)(trigger.querySelector('.ember-power-select-trigger-multiple-input'), value);
    } else if (isDefaultSingleSelect) {
      await (0, _testHelpers.fillIn)('.ember-power-select-search-input', value);
    } else {
      // It's probably a customized version
      let inputIsInTrigger = !!trigger.querySelector('.ember-power-select-trigger input[type=search]');
      if (inputIsInTrigger) {
        await (0, _testHelpers.fillIn)(trigger.querySelector('input[type=search]'), value);
      } else {
        await (0, _testHelpers.fillIn)(`#${contentId} .ember-power-select-search-input[type=search]`, 'input');
      }
    }
    return (0, _testHelpers.settled)();
  }

  async function removeMultipleOption(cssPath, value) {
    let elem;
    let items = document.querySelectorAll(`${cssPath} .ember-power-select-multiple-options > li`);
    let item = [].slice.apply(items).find(el => el.textContent.indexOf(value) > -1);
    if (item) {
      elem = item.querySelector('.ember-power-select-multiple-remove-btn');
    }
    try {
      await (0, _testHelpers.click)(elem);
      return (0, _testHelpers.settled)();
    } catch (e) {
      (true && Ember.warn('css path to remove btn not found'));

      throw e;
    }
  }

  async function clearSelected(cssPath) {
    let elem = document.querySelector(`${cssPath} .ember-power-select-clear-btn`);
    try {
      await (0, _testHelpers.click)(elem);
      return (0, _testHelpers.settled)();
    } catch (e) {
      (true && Ember.warn('css path to clear btn not found'));

      throw e;
    }
  }
});
define('ember-qunit/adapter', ['exports', 'qunit', '@ember/test-helpers/has-ember-version'], function (exports, _qunit, _hasEmberVersion) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  function unhandledRejectionAssertion(current, error) {
    let message, source;

    if (typeof error === 'object' && error !== null) {
      message = error.message;
      source = error.stack;
    } else if (typeof error === 'string') {
      message = error;
      source = 'unknown source';
    } else {
      message = 'unhandledRejection occured, but it had no message';
      source = 'unknown source';
    }

    current.assert.pushResult({
      result: false,
      actual: false,
      expected: true,
      message: message,
      source: source
    });
  }

  let Adapter = Ember.Test.Adapter.extend({
    init() {
      this.doneCallbacks = [];
    },

    asyncStart() {
      this.doneCallbacks.push(_qunit.default.config.current ? _qunit.default.config.current.assert.async() : null);
    },

    asyncEnd() {
      let done = this.doneCallbacks.pop();
      // This can be null if asyncStart() was called outside of a test
      if (done) {
        done();
      }
    },

    // clobber default implementation of `exception` will be added back for Ember
    // < 2.17 just below...
    exception: null
  });

  // Ember 2.17 and higher do not require the test adapter to have an `exception`
  // method When `exception` is not present, the unhandled rejection is
  // automatically re-thrown and will therefore hit QUnit's own global error
  // handler (therefore appropriately causing test failure)
  if (!(0, _hasEmberVersion.default)(2, 17)) {
    Adapter = Adapter.extend({
      exception(error) {
        unhandledRejectionAssertion(_qunit.default.config.current, error);
      }
    });
  }

  exports.default = Adapter;
});
define('ember-qunit/index', ['exports', 'ember-qunit/legacy-2-x/module-for', 'ember-qunit/legacy-2-x/module-for-component', 'ember-qunit/legacy-2-x/module-for-model', 'ember-qunit/adapter', 'qunit', 'ember-qunit/test-loader', '@ember/test-helpers', 'ember-qunit/test-isolation-validation'], function (exports, _moduleFor, _moduleForComponent, _moduleForModel, _adapter, _qunit, _testLoader, _testHelpers, _testIsolationValidation) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.loadTests = exports.todo = exports.only = exports.skip = exports.test = exports.module = exports.QUnitAdapter = exports.moduleForModel = exports.moduleForComponent = exports.moduleFor = undefined;
  Object.defineProperty(exports, 'moduleFor', {
    enumerable: true,
    get: function () {
      return _moduleFor.default;
    }
  });
  Object.defineProperty(exports, 'moduleForComponent', {
    enumerable: true,
    get: function () {
      return _moduleForComponent.default;
    }
  });
  Object.defineProperty(exports, 'moduleForModel', {
    enumerable: true,
    get: function () {
      return _moduleForModel.default;
    }
  });
  Object.defineProperty(exports, 'QUnitAdapter', {
    enumerable: true,
    get: function () {
      return _adapter.default;
    }
  });
  Object.defineProperty(exports, 'module', {
    enumerable: true,
    get: function () {
      return _qunit.module;
    }
  });
  Object.defineProperty(exports, 'test', {
    enumerable: true,
    get: function () {
      return _qunit.test;
    }
  });
  Object.defineProperty(exports, 'skip', {
    enumerable: true,
    get: function () {
      return _qunit.skip;
    }
  });
  Object.defineProperty(exports, 'only', {
    enumerable: true,
    get: function () {
      return _qunit.only;
    }
  });
  Object.defineProperty(exports, 'todo', {
    enumerable: true,
    get: function () {
      return _qunit.todo;
    }
  });
  Object.defineProperty(exports, 'loadTests', {
    enumerable: true,
    get: function () {
      return _testLoader.loadTests;
    }
  });
  exports.setResolver = setResolver;
  exports.render = render;
  exports.clearRender = clearRender;
  exports.settled = settled;
  exports.pauseTest = pauseTest;
  exports.resumeTest = resumeTest;
  exports.setupTest = setupTest;
  exports.setupRenderingTest = setupRenderingTest;
  exports.setupApplicationTest = setupApplicationTest;
  exports.setupTestContainer = setupTestContainer;
  exports.startTests = startTests;
  exports.setupTestAdapter = setupTestAdapter;
  exports.setupEmberTesting = setupEmberTesting;
  exports.setupEmberOnerrorValidation = setupEmberOnerrorValidation;
  exports.setupTestIsolationValidation = setupTestIsolationValidation;
  exports.start = start;
  function setResolver() {
    (true && !(false) && Ember.deprecate('`setResolver` should be imported from `@ember/test-helpers`, but was imported from `ember-qunit`', false, {
      id: 'ember-qunit.deprecated-reexports.setResolver',
      until: '4.0.0'
    }));


    return (0, _testHelpers.setResolver)(...arguments);
  }

  function render() {
    (true && !(false) && Ember.deprecate('`render` should be imported from `@ember/test-helpers`, but was imported from `ember-qunit`', false, {
      id: 'ember-qunit.deprecated-reexports.render',
      until: '4.0.0'
    }));


    return (0, _testHelpers.render)(...arguments);
  }

  function clearRender() {
    (true && !(false) && Ember.deprecate('`clearRender` should be imported from `@ember/test-helpers`, but was imported from `ember-qunit`', false, {
      id: 'ember-qunit.deprecated-reexports.clearRender',
      until: '4.0.0'
    }));


    return (0, _testHelpers.clearRender)(...arguments);
  }

  function settled() {
    (true && !(false) && Ember.deprecate('`settled` should be imported from `@ember/test-helpers`, but was imported from `ember-qunit`', false, {
      id: 'ember-qunit.deprecated-reexports.settled',
      until: '4.0.0'
    }));


    return (0, _testHelpers.settled)(...arguments);
  }

  function pauseTest() {
    (true && !(false) && Ember.deprecate('`pauseTest` should be imported from `@ember/test-helpers`, but was imported from `ember-qunit`', false, {
      id: 'ember-qunit.deprecated-reexports.pauseTest',
      until: '4.0.0'
    }));


    return (0, _testHelpers.pauseTest)(...arguments);
  }

  function resumeTest() {
    (true && !(false) && Ember.deprecate('`resumeTest` should be imported from `@ember/test-helpers`, but was imported from `ember-qunit`', false, {
      id: 'ember-qunit.deprecated-reexports.resumeTest',
      until: '4.0.0'
    }));


    return (0, _testHelpers.resumeTest)(...arguments);
  }

  function setupTest(hooks, options) {
    hooks.beforeEach(function (assert) {
      return (0, _testHelpers.setupContext)(this, options).then(() => {
        let originalPauseTest = this.pauseTest;
        this.pauseTest = function QUnit_pauseTest() {
          assert.timeout(-1); // prevent the test from timing out

          return originalPauseTest.call(this);
        };
      });
    });

    hooks.afterEach(function () {
      return (0, _testHelpers.teardownContext)(this);
    });
  }

  function setupRenderingTest(hooks, options) {
    setupTest(hooks, options);

    hooks.beforeEach(function () {
      return (0, _testHelpers.setupRenderingContext)(this);
    });

    hooks.afterEach(function () {
      return (0, _testHelpers.teardownRenderingContext)(this);
    });
  }

  function setupApplicationTest(hooks, options) {
    setupTest(hooks, options);

    hooks.beforeEach(function () {
      return (0, _testHelpers.setupApplicationContext)(this);
    });

    hooks.afterEach(function () {
      return (0, _testHelpers.teardownApplicationContext)(this);
    });
  }

  /**
     Uses current URL configuration to setup the test container.
  
     * If `?nocontainer` is set, the test container will be hidden.
     * If `?dockcontainer` or `?devmode` are set the test container will be
       absolutely positioned.
     * If `?devmode` is set, the test container will be made full screen.
  
     @method setupTestContainer
   */
  function setupTestContainer() {
    let testContainer = document.getElementById('ember-testing-container');
    if (!testContainer) {
      return;
    }

    let params = _qunit.default.urlParams;

    let containerVisibility = params.nocontainer ? 'hidden' : 'visible';
    let containerPosition = params.dockcontainer || params.devmode ? 'fixed' : 'relative';

    if (params.devmode) {
      testContainer.className = ' full-screen';
    }

    testContainer.style.visibility = containerVisibility;
    testContainer.style.position = containerPosition;

    let qunitContainer = document.getElementById('qunit');
    if (params.dockcontainer) {
      qunitContainer.style.marginBottom = window.getComputedStyle(testContainer).height;
    }
  }

  /**
     Instruct QUnit to start the tests.
     @method startTests
   */
  function startTests() {
    _qunit.default.start();
  }

  /**
     Sets up the `Ember.Test` adapter for usage with QUnit 2.x.
  
     @method setupTestAdapter
   */
  function setupTestAdapter() {
    Ember.Test.adapter = _adapter.default.create();
  }

  /**
    Ensures that `Ember.testing` is set to `true` before each test begins
    (including `before` / `beforeEach`), and reset to `false` after each test is
    completed. This is done via `QUnit.testStart` and `QUnit.testDone`.
  
   */
  function setupEmberTesting() {
    _qunit.default.testStart(() => {
      Ember.testing = true;
    });

    _qunit.default.testDone(() => {
      Ember.testing = false;
    });
  }

  /**
    Ensures that `Ember.onerror` (if present) is properly configured to re-throw
    errors that occur while `Ember.testing` is `true`.
  */
  function setupEmberOnerrorValidation() {
    _qunit.default.module('ember-qunit: Ember.onerror validation', function () {
      _qunit.default.test('Ember.onerror is functioning properly', function (assert) {
        assert.expect(1);
        let result = (0, _testHelpers.validateErrorHandler)();
        assert.ok(result.isValid, `Ember.onerror handler with invalid testing behavior detected. An Ember.onerror handler _must_ rethrow exceptions when \`Ember.testing\` is \`true\` or the test suite is unreliable. See https://git.io/vbine for more details.`);
      });
    });
  }

  function setupTestIsolationValidation() {
    _qunit.default.testDone(_testIsolationValidation.detectIfTestNotIsolated);
    _qunit.default.done(_testIsolationValidation.reportIfTestNotIsolated);
  }

  /**
     @method start
     @param {Object} [options] Options to be used for enabling/disabling behaviors
     @param {Boolean} [options.loadTests] If `false` tests will not be loaded automatically.
     @param {Boolean} [options.setupTestContainer] If `false` the test container will not
     be setup based on `devmode`, `dockcontainer`, or `nocontainer` URL params.
     @param {Boolean} [options.startTests] If `false` tests will not be automatically started
     (you must run `QUnit.start()` to kick them off).
     @param {Boolean} [options.setupTestAdapter] If `false` the default Ember.Test adapter will
     not be updated.
     @param {Boolean} [options.setupEmberTesting] `false` opts out of the
     default behavior of setting `Ember.testing` to `true` before all tests and
     back to `false` after each test will.
     @param {Boolean} [options.setupEmberOnerrorValidation] If `false` validation
     of `Ember.onerror` will be disabled.
     @param {Boolean} [options.setupTestIsolationValidation] If `false` test isolation validation
     will be disabled.
   */
  function start(options = {}) {
    if (options.loadTests !== false) {
      (0, _testLoader.loadTests)();
    }

    if (options.setupTestContainer !== false) {
      setupTestContainer();
    }

    if (options.setupTestAdapter !== false) {
      setupTestAdapter();
    }

    if (options.setupEmberTesting !== false) {
      setupEmberTesting();
    }

    if (options.setupEmberOnerrorValidation !== false) {
      setupEmberOnerrorValidation();
    }

    if (typeof options.setupTestIsolationValidation !== 'undefined' && options.setupTestIsolationValidation !== false) {
      setupTestIsolationValidation();
    }

    if (options.startTests !== false) {
      startTests();
    }
  }
});
define('ember-qunit/legacy-2-x/module-for-component', ['exports', 'ember-qunit/legacy-2-x/qunit-module', 'ember-test-helpers'], function (exports, _qunitModule, _emberTestHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = moduleForComponent;
  function moduleForComponent(name, description, callbacks) {
    (0, _qunitModule.createModule)(_emberTestHelpers.TestModuleForComponent, name, description, callbacks);
  }
});
define('ember-qunit/legacy-2-x/module-for-model', ['exports', 'ember-qunit/legacy-2-x/qunit-module', 'ember-test-helpers'], function (exports, _qunitModule, _emberTestHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = moduleForModel;
  function moduleForModel(name, description, callbacks) {
    (0, _qunitModule.createModule)(_emberTestHelpers.TestModuleForModel, name, description, callbacks);
  }
});
define('ember-qunit/legacy-2-x/module-for', ['exports', 'ember-qunit/legacy-2-x/qunit-module', 'ember-test-helpers'], function (exports, _qunitModule, _emberTestHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = moduleFor;
  function moduleFor(name, description, callbacks) {
    (0, _qunitModule.createModule)(_emberTestHelpers.TestModule, name, description, callbacks);
  }
});
define('ember-qunit/legacy-2-x/qunit-module', ['exports', 'qunit'], function (exports, _qunit) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.createModule = createModule;


  function noop() {}

  function callbackFor(name, callbacks) {
    if (typeof callbacks !== 'object') {
      return noop;
    }
    if (!callbacks) {
      return noop;
    }

    var callback = noop;

    if (callbacks[name]) {
      callback = callbacks[name];
      delete callbacks[name];
    }

    return callback;
  }

  function createModule(Constructor, name, description, callbacks) {
    if (!callbacks && typeof description === 'object') {
      callbacks = description;
      description = name;
    }

    var before = callbackFor('before', callbacks);
    var beforeEach = callbackFor('beforeEach', callbacks);
    var afterEach = callbackFor('afterEach', callbacks);
    var after = callbackFor('after', callbacks);

    var module;
    var moduleName = typeof description === 'string' ? description : name;

    (0, _qunit.module)(moduleName, {
      before() {
        // storing this in closure scope to avoid exposing these
        // private internals to the test context
        module = new Constructor(name, description, callbacks);
        return before.apply(this, arguments);
      },

      beforeEach() {
        // provide the test context to the underlying module
        module.setContext(this);

        return module.setup(...arguments).then(() => {
          return beforeEach.apply(this, arguments);
        });
      },

      afterEach() {
        let result = afterEach.apply(this, arguments);
        return Ember.RSVP.resolve(result).then(() => module.teardown(...arguments));
      },

      after() {
        try {
          return after.apply(this, arguments);
        } finally {
          after = afterEach = before = beforeEach = callbacks = module = null;
        }
      }
    });
  }
});
define('ember-qunit/test-isolation-validation', ['exports', '@ember/test-helpers'], function (exports, _testHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.detectIfTestNotIsolated = detectIfTestNotIsolated;
  exports.reportIfTestNotIsolated = reportIfTestNotIsolated;
  exports.getMessage = getMessage;


  const TESTS_NOT_ISOLATED = [];

  /**
   * Detects if a specific test isn't isolated. A test is considered
   * not isolated if it:
   *
   * - has pending timers
   * - is in a runloop
   * - has pending AJAX requests
   * - has pending test waiters
   *
   * @function detectIfTestNotIsolated
   * @param {Object} testInfo
   * @param {string} testInfo.module The name of the test module
   * @param {string} testInfo.name The test name
   */
  function detectIfTestNotIsolated({ module, name }) {
    if (!(0, _testHelpers.isSettled)()) {
      TESTS_NOT_ISOLATED.push(`${module}: ${name}`);
      Ember.run.cancelTimers();
    }
  }

  /**
   * Reports if a test isn't isolated. Please see above for what
   * constitutes a test being isolated.
   *
   * @function reportIfTestNotIsolated
   * @throws Error if tests are not isolated
   */
  function reportIfTestNotIsolated() {
    if (TESTS_NOT_ISOLATED.length > 0) {
      let leakyTests = TESTS_NOT_ISOLATED.slice();
      TESTS_NOT_ISOLATED.length = 0;

      throw new Error(getMessage(leakyTests.length, leakyTests.join('\n')));
    }
  }

  function getMessage(testCount, testsToReport) {
    return `TESTS ARE NOT ISOLATED
    The following (${testCount}) tests have one or more of pending timers, pending AJAX requests, pending test waiters, or are still in a runloop: \n
    ${testsToReport}
  `;
  }
});
define('ember-qunit/test-loader', ['exports', 'qunit', 'ember-cli-test-loader/test-support/index'], function (exports, _qunit, _index) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.TestLoader = undefined;
  exports.loadTests = loadTests;


  (0, _index.addModuleExcludeMatcher)(function (moduleName) {
    return _qunit.default.urlParams.nolint && moduleName.match(/\.(jshint|lint-test)$/);
  });

  (0, _index.addModuleIncludeMatcher)(function (moduleName) {
    return moduleName.match(/\.jshint$/);
  });

  let moduleLoadFailures = [];

  _qunit.default.done(function () {
    let length = moduleLoadFailures.length;

    try {
      if (length === 0) {
        // do nothing
      } else if (length === 1) {
        throw moduleLoadFailures[0];
      } else {
        throw new Error('\n' + moduleLoadFailures.join('\n'));
      }
    } finally {
      // ensure we release previously captured errors.
      moduleLoadFailures = [];
    }
  });

  class TestLoader extends _index.default {
    moduleLoadFailure(moduleName, error) {
      moduleLoadFailures.push(error);

      _qunit.default.module('TestLoader Failures');
      _qunit.default.test(moduleName + ': could not be loaded', function () {
        throw error;
      });
    }
  }

  exports.TestLoader = TestLoader;
  /**
     Load tests following the default patterns:
  
     * The module name ends with `-test`
     * The module name ends with `.jshint`
  
     Excludes tests that match the following
     patterns when `?nolint` URL param is set:
  
     * The module name ends with `.jshint`
     * The module name ends with `-lint-test`
  
     @method loadTests
   */
  function loadTests() {
    new TestLoader().loadModules();
  }
});
define('ember-test-helpers/has-ember-version', ['exports', '@ember/test-helpers/has-ember-version'], function (exports, _hasEmberVersion) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _hasEmberVersion.default;
    }
  });
});
define('ember-test-helpers/index', ['exports', '@ember/test-helpers', 'ember-test-helpers/legacy-0-6-x/test-module', 'ember-test-helpers/legacy-0-6-x/test-module-for-acceptance', 'ember-test-helpers/legacy-0-6-x/test-module-for-component', 'ember-test-helpers/legacy-0-6-x/test-module-for-model'], function (exports, _testHelpers, _testModule, _testModuleForAcceptance, _testModuleForComponent, _testModuleForModel) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.keys(_testHelpers).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
      enumerable: true,
      get: function () {
        return _testHelpers[key];
      }
    });
  });
  Object.defineProperty(exports, 'TestModule', {
    enumerable: true,
    get: function () {
      return _testModule.default;
    }
  });
  Object.defineProperty(exports, 'TestModuleForAcceptance', {
    enumerable: true,
    get: function () {
      return _testModuleForAcceptance.default;
    }
  });
  Object.defineProperty(exports, 'TestModuleForComponent', {
    enumerable: true,
    get: function () {
      return _testModuleForComponent.default;
    }
  });
  Object.defineProperty(exports, 'TestModuleForModel', {
    enumerable: true,
    get: function () {
      return _testModuleForModel.default;
    }
  });
});
define('ember-test-helpers/legacy-0-6-x/-legacy-overrides', ['exports', 'ember-test-helpers/has-ember-version'], function (exports, _hasEmberVersion) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.preGlimmerSetupIntegrationForComponent = preGlimmerSetupIntegrationForComponent;
  function preGlimmerSetupIntegrationForComponent() {
    var module = this;
    var context = this.context;

    this.actionHooks = {};

    context.dispatcher = this.container.lookup('event_dispatcher:main') || Ember.EventDispatcher.create();
    context.dispatcher.setup({}, '#ember-testing');
    context.actions = module.actionHooks;

    (this.registry || this.container).register('component:-test-holder', Ember.Component.extend());

    context.render = function (template) {
      // in case `this.render` is called twice, make sure to teardown the first invocation
      module.teardownComponent();

      if (!template) {
        throw new Error('in a component integration test you must pass a template to `render()`');
      }
      if (Ember.isArray(template)) {
        template = template.join('');
      }
      if (typeof template === 'string') {
        template = Ember.Handlebars.compile(template);
      }
      module.component = module.container.lookupFactory('component:-test-holder').create({
        layout: template
      });

      module.component.set('context', context);
      module.component.set('controller', context);

      Ember.run(function () {
        module.component.appendTo('#ember-testing');
      });

      context._element = module.component.element;
    };

    context.$ = function () {
      return module.component.$.apply(module.component, arguments);
    };

    context.set = function (key, value) {
      var ret = Ember.run(function () {
        return Ember.set(context, key, value);
      });

      if ((0, _hasEmberVersion.default)(2, 0)) {
        return ret;
      }
    };

    context.setProperties = function (hash) {
      var ret = Ember.run(function () {
        return Ember.setProperties(context, hash);
      });

      if ((0, _hasEmberVersion.default)(2, 0)) {
        return ret;
      }
    };

    context.get = function (key) {
      return Ember.get(context, key);
    };

    context.getProperties = function () {
      var args = Array.prototype.slice.call(arguments);
      return Ember.getProperties(context, args);
    };

    context.on = function (actionName, handler) {
      module.actionHooks[actionName] = handler;
    };

    context.send = function (actionName) {
      var hook = module.actionHooks[actionName];
      if (!hook) {
        throw new Error('integration testing template received unexpected action ' + actionName);
      }
      hook.apply(module, Array.prototype.slice.call(arguments, 1));
    };

    context.clearRender = function () {
      module.teardownComponent();
    };
  }
});
define('ember-test-helpers/legacy-0-6-x/abstract-test-module', ['exports', 'ember-test-helpers/legacy-0-6-x/ext/rsvp', '@ember/test-helpers/settled', '@ember/test-helpers'], function (exports, _rsvp, _settled, _testHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = class {
    constructor(name, options) {
      this.context = undefined;
      this.name = name;
      this.callbacks = options || {};

      this.initSetupSteps();
      this.initTeardownSteps();
    }

    setup(assert) {
      Ember.testing = true;
      return this.invokeSteps(this.setupSteps, this, assert).then(() => {
        this.contextualizeCallbacks();
        return this.invokeSteps(this.contextualizedSetupSteps, this.context, assert);
      });
    }

    teardown(assert) {
      return this.invokeSteps(this.contextualizedTeardownSteps, this.context, assert).then(() => {
        return this.invokeSteps(this.teardownSteps, this, assert);
      }).then(() => {
        this.cache = null;
        this.cachedCalls = null;
      }).finally(function () {
        Ember.testing = false;
      });
    }

    initSetupSteps() {
      this.setupSteps = [];
      this.contextualizedSetupSteps = [];

      if (this.callbacks.beforeSetup) {
        this.setupSteps.push(this.callbacks.beforeSetup);
        delete this.callbacks.beforeSetup;
      }

      this.setupSteps.push(this.setupContext);
      this.setupSteps.push(this.setupTestElements);
      this.setupSteps.push(this.setupAJAXListeners);
      this.setupSteps.push(this.setupPromiseListeners);

      if (this.callbacks.setup) {
        this.contextualizedSetupSteps.push(this.callbacks.setup);
        delete this.callbacks.setup;
      }
    }

    invokeSteps(steps, context, assert) {
      steps = steps.slice();

      function nextStep() {
        var step = steps.shift();
        if (step) {
          // guard against exceptions, for example missing components referenced from needs.
          return new Ember.RSVP.Promise(resolve => {
            resolve(step.call(context, assert));
          }).then(nextStep);
        } else {
          return Ember.RSVP.resolve();
        }
      }
      return nextStep();
    }

    contextualizeCallbacks() {}

    initTeardownSteps() {
      this.teardownSteps = [];
      this.contextualizedTeardownSteps = [];

      if (this.callbacks.teardown) {
        this.contextualizedTeardownSteps.push(this.callbacks.teardown);
        delete this.callbacks.teardown;
      }

      this.teardownSteps.push(this.teardownContext);
      this.teardownSteps.push(this.teardownTestElements);
      this.teardownSteps.push(this.teardownAJAXListeners);
      this.teardownSteps.push(this.teardownPromiseListeners);

      if (this.callbacks.afterTeardown) {
        this.teardownSteps.push(this.callbacks.afterTeardown);
        delete this.callbacks.afterTeardown;
      }
    }

    setupTestElements() {
      let testElementContainer = document.querySelector('#ember-testing-container');
      if (!testElementContainer) {
        testElementContainer = document.createElement('div');
        testElementContainer.setAttribute('id', 'ember-testing-container');
        document.body.appendChild(testElementContainer);
      }

      let testEl = document.querySelector('#ember-testing');
      if (!testEl) {
        let element = document.createElement('div');
        element.setAttribute('id', 'ember-testing');

        testElementContainer.appendChild(element);
        this.fixtureResetValue = '';
      } else {
        this.fixtureResetValue = testElementContainer.innerHTML;
      }
    }

    setupContext(options) {
      let context = this.getContext();

      Ember.assign(context, {
        dispatcher: null,
        inject: {}
      }, options);

      this.setToString();
      (0, _testHelpers.setContext)(context);
      this.context = context;
    }

    setContext(context) {
      this.context = context;
    }

    getContext() {
      if (this.context) {
        return this.context;
      }

      return this.context = (0, _testHelpers.getContext)() || {};
    }

    setToString() {
      this.context.toString = () => {
        if (this.subjectName) {
          return `test context for: ${this.subjectName}`;
        }

        if (this.name) {
          return `test context for: ${this.name}`;
        }
      };
    }

    setupAJAXListeners() {
      (0, _settled._setupAJAXHooks)();
    }

    teardownAJAXListeners() {
      (0, _settled._teardownAJAXHooks)();
    }

    setupPromiseListeners() {
      (0, _rsvp._setupPromiseListeners)();
    }

    teardownPromiseListeners() {
      (0, _rsvp._teardownPromiseListeners)();
    }

    teardownTestElements() {
      document.getElementById('ember-testing-container').innerHTML = this.fixtureResetValue;

      // Ember 2.0.0 removed Ember.View as public API, so only do this when
      // Ember.View is present
      if (Ember.View && Ember.View.views) {
        Ember.View.views = {};
      }
    }

    teardownContext() {
      var context = this.context;
      this.context = undefined;
      (0, _testHelpers.unsetContext)();

      if (context && context.dispatcher && !context.dispatcher.isDestroyed) {
        Ember.run(function () {
          context.dispatcher.destroy();
        });
      }
    }
  };
});
define('ember-test-helpers/legacy-0-6-x/build-registry', ['exports', 'require'], function (exports, _require2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (resolver) {
    var fallbackRegistry, registry, container;
    var namespace = Ember.Object.create({
      Resolver: {
        create() {
          return resolver;
        }
      }
    });

    function register(name, factory) {
      var thingToRegisterWith = registry || container;

      if (!(container.factoryFor ? container.factoryFor(name) : container.lookupFactory(name))) {
        thingToRegisterWith.register(name, factory);
      }
    }

    if (Ember.Application.buildRegistry) {
      fallbackRegistry = Ember.Application.buildRegistry(namespace);
      fallbackRegistry.register('component-lookup:main', Ember.ComponentLookup);

      registry = new Ember.Registry({
        fallback: fallbackRegistry
      });

      if (Ember.ApplicationInstance && Ember.ApplicationInstance.setupRegistry) {
        Ember.ApplicationInstance.setupRegistry(registry);
      }

      // these properties are set on the fallback registry by `buildRegistry`
      // and on the primary registry within the ApplicationInstance constructor
      // but we need to manually recreate them since ApplicationInstance's are not
      // exposed externally
      registry.normalizeFullName = fallbackRegistry.normalizeFullName;
      registry.makeToString = fallbackRegistry.makeToString;
      registry.describe = fallbackRegistry.describe;

      var owner = Owner.create({
        __registry__: registry,
        __container__: null
      });

      container = registry.container({ owner: owner });
      owner.__container__ = container;

      exposeRegistryMethodsWithoutDeprecations(container);
    } else {
      container = Ember.Application.buildContainer(namespace);
      container.register('component-lookup:main', Ember.ComponentLookup);
    }

    // Ember 1.10.0 did not properly add `view:toplevel` or `view:default`
    // to the registry in Ember.Application.buildRegistry :(
    //
    // Ember 2.0.0 removed Ember.View as public API, so only do this when
    // Ember.View is present
    if (Ember.View) {
      register('view:toplevel', Ember.View.extend());
    }

    // Ember 2.0.0 removed Ember._MetamorphView from the Ember global, so only
    // do this when present
    if (Ember._MetamorphView) {
      register('view:default', Ember._MetamorphView);
    }

    var globalContext = typeof global === 'object' && global || self;
    if (requirejs.entries['ember-data/setup-container']) {
      // ember-data is a proper ember-cli addon since 2.3; if no 'import
      // 'ember-data'' is present somewhere in the tests, there is also no `DS`
      // available on the globalContext and hence ember-data wouldn't be setup
      // correctly for the tests; that's why we import and call setupContainer
      // here; also see https://github.com/emberjs/data/issues/4071 for context
      var setupContainer = (0, _require2.default)('ember-data/setup-container')['default'];
      setupContainer(registry || container);
    } else if (globalContext.DS) {
      var DS = globalContext.DS;
      if (DS._setupContainer) {
        DS._setupContainer(registry || container);
      } else {
        register('transform:boolean', DS.BooleanTransform);
        register('transform:date', DS.DateTransform);
        register('transform:number', DS.NumberTransform);
        register('transform:string', DS.StringTransform);
        register('serializer:-default', DS.JSONSerializer);
        register('serializer:-rest', DS.RESTSerializer);
        register('adapter:-rest', DS.RESTAdapter);
      }
    }

    return {
      registry,
      container,
      owner
    };
  };

  /* globals global, self, requirejs */

  function exposeRegistryMethodsWithoutDeprecations(container) {
    var methods = ['register', 'unregister', 'resolve', 'normalize', 'typeInjection', 'injection', 'factoryInjection', 'factoryTypeInjection', 'has', 'options', 'optionsForType'];

    function exposeRegistryMethod(container, method) {
      if (method in container) {
        container[method] = function () {
          return container._registry[method].apply(container._registry, arguments);
        };
      }
    }

    for (var i = 0, l = methods.length; i < l; i++) {
      exposeRegistryMethod(container, methods[i]);
    }
  }

  var Owner = function () {
    if (Ember._RegistryProxyMixin && Ember._ContainerProxyMixin) {
      return Ember.Object.extend(Ember._RegistryProxyMixin, Ember._ContainerProxyMixin, {
        _emberTestHelpersMockOwner: true
      });
    }

    return Ember.Object.extend({
      _emberTestHelpersMockOwner: true
    });
  }();
});
define('ember-test-helpers/legacy-0-6-x/ext/rsvp', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports._setupPromiseListeners = _setupPromiseListeners;
  exports._teardownPromiseListeners = _teardownPromiseListeners;


  let originalAsync;

  /**
    Configures `RSVP` to resolve promises on the run-loop's action queue. This is
    done by Ember internally since Ember 1.7 and it is only needed to
    provide a consistent testing experience for users of Ember < 1.7.
  
    @private
  */
  function _setupPromiseListeners() {
    originalAsync = Ember.RSVP.configure('async');

    Ember.RSVP.configure('async', function (callback, promise) {
      Ember.run.backburner.schedule('actions', () => {
        callback(promise);
      });
    });
  }

  /**
    Resets `RSVP`'s `async` to its prior value.
  
    @private
  */
  function _teardownPromiseListeners() {
    Ember.RSVP.configure('async', originalAsync);
  }
});
define('ember-test-helpers/legacy-0-6-x/test-module-for-acceptance', ['exports', 'ember-test-helpers/legacy-0-6-x/abstract-test-module', '@ember/test-helpers'], function (exports, _abstractTestModule, _testHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = class extends _abstractTestModule.default {
    setupContext() {
      super.setupContext({ application: this.createApplication() });
    }

    teardownContext() {
      Ember.run(() => {
        (0, _testHelpers.getContext)().application.destroy();
      });

      super.teardownContext();
    }

    createApplication() {
      let { Application, config } = this.callbacks;
      let application;

      Ember.run(() => {
        application = Application.create(config);
        application.setupForTesting();
        application.injectTestHelpers();
      });

      return application;
    }
  };
});
define('ember-test-helpers/legacy-0-6-x/test-module-for-component', ['exports', 'ember-test-helpers/legacy-0-6-x/test-module', 'ember-test-helpers/has-ember-version', 'ember-test-helpers/legacy-0-6-x/-legacy-overrides'], function (exports, _testModule, _hasEmberVersion, _legacyOverrides) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.setupComponentIntegrationTest = setupComponentIntegrationTest;
  /* globals EmberENV */
  let ACTION_KEY;
  if ((0, _hasEmberVersion.default)(2, 0)) {
    ACTION_KEY = 'actions';
  } else {
    ACTION_KEY = '_actions';
  }

  const isPreGlimmer = !(0, _hasEmberVersion.default)(1, 13);

  exports.default = class extends _testModule.default {
    constructor(componentName, description, callbacks) {
      // Allow `description` to be omitted
      if (!callbacks && typeof description === 'object') {
        callbacks = description;
        description = null;
      } else if (!callbacks) {
        callbacks = {};
      }

      let integrationOption = callbacks.integration;
      let hasNeeds = Array.isArray(callbacks.needs);

      super('component:' + componentName, description, callbacks);

      this.componentName = componentName;

      if (hasNeeds || callbacks.unit || integrationOption === false) {
        this.isUnitTest = true;
      } else if (integrationOption) {
        this.isUnitTest = false;
      } else {
        Ember.deprecate('the component:' + componentName + ' test module is implicitly running in unit test mode, ' + 'which will change to integration test mode by default in an upcoming version of ' + 'ember-test-helpers. Add `unit: true` or a `needs:[]` list to explicitly opt in to unit ' + 'test mode.', false, {
          id: 'ember-test-helpers.test-module-for-component.test-type',
          until: '0.6.0'
        });
        this.isUnitTest = true;
      }

      if (!this.isUnitTest && !this.isLegacy) {
        callbacks.integration = true;
      }

      if (this.isUnitTest || this.isLegacy) {
        this.setupSteps.push(this.setupComponentUnitTest);
      } else {
        this.callbacks.subject = function () {
          throw new Error("component integration tests do not support `subject()`. Instead, render the component as if it were HTML: `this.render('<my-component foo=true>');`. For more information, read: http://guides.emberjs.com/current/testing/testing-components/");
        };
        this.setupSteps.push(this.setupComponentIntegrationTest);
        this.teardownSteps.unshift(this.teardownComponent);
      }

      if (Ember.View && Ember.View.views) {
        this.setupSteps.push(this._aliasViewRegistry);
        this.teardownSteps.unshift(this._resetViewRegistry);
      }
    }

    initIntegration(options) {
      this.isLegacy = options.integration === 'legacy';
      this.isIntegration = options.integration !== 'legacy';
    }

    _aliasViewRegistry() {
      this._originalGlobalViewRegistry = Ember.View.views;
      var viewRegistry = this.container.lookup('-view-registry:main');

      if (viewRegistry) {
        Ember.View.views = viewRegistry;
      }
    }

    _resetViewRegistry() {
      Ember.View.views = this._originalGlobalViewRegistry;
    }

    setupComponentUnitTest() {
      var _this = this;
      var resolver = this.resolver;
      var context = this.context;

      var layoutName = 'template:components/' + this.componentName;

      var layout = resolver.resolve(layoutName);

      var thingToRegisterWith = this.registry || this.container;
      if (layout) {
        thingToRegisterWith.register(layoutName, layout);
        thingToRegisterWith.injection(this.subjectName, 'layout', layoutName);
      }
      var eventDispatcher = resolver.resolve('event_dispatcher:main');
      if (eventDispatcher) {
        thingToRegisterWith.register('event_dispatcher:main', eventDispatcher);
      }

      context.dispatcher = this.container.lookup('event_dispatcher:main') || Ember.EventDispatcher.create();
      context.dispatcher.setup({}, '#ember-testing');

      context._element = null;

      this.callbacks.render = function () {
        var subject;

        Ember.run(function () {
          subject = context.subject();
          subject.appendTo('#ember-testing');
        });

        context._element = subject.element;

        _this.teardownSteps.unshift(function () {
          Ember.run(function () {
            Ember.tryInvoke(subject, 'destroy');
          });
        });
      };

      this.callbacks.append = function () {
        Ember.deprecate('this.append() is deprecated. Please use this.render() or this.$() instead.', false, {
          id: 'ember-test-helpers.test-module-for-component.append',
          until: '0.6.0'
        });
        return context.$();
      };

      context.$ = function () {
        this.render();
        var subject = this.subject();

        return subject.$.apply(subject, arguments);
      };
    }

    setupComponentIntegrationTest() {
      if (isPreGlimmer) {
        return _legacyOverrides.preGlimmerSetupIntegrationForComponent.apply(this, arguments);
      } else {
        return setupComponentIntegrationTest.apply(this, arguments);
      }
    }

    setupContext() {
      super.setupContext();

      // only setup the injection if we are running against a version
      // of Ember that has `-view-registry:main` (Ember >= 1.12)
      if (this.container.factoryFor ? this.container.factoryFor('-view-registry:main') : this.container.lookupFactory('-view-registry:main')) {
        (this.registry || this.container).injection('component', '_viewRegistry', '-view-registry:main');
      }

      if (!this.isUnitTest && !this.isLegacy) {
        this.context.factory = function () {};
      }
    }

    teardownComponent() {
      var component = this.component;
      if (component) {
        Ember.run(component, 'destroy');
        this.component = null;
      }
    }
  };
  function setupComponentIntegrationTest() {
    var module = this;
    var context = this.context;

    this.actionHooks = context[ACTION_KEY] = {};
    context.dispatcher = this.container.lookup('event_dispatcher:main') || Ember.EventDispatcher.create();
    context.dispatcher.setup({}, '#ember-testing');

    var hasRendered = false;
    var OutletView = module.container.factoryFor ? module.container.factoryFor('view:-outlet') : module.container.lookupFactory('view:-outlet');
    var OutletTemplate = module.container.lookup('template:-outlet');
    var toplevelView = module.component = OutletView.create();
    var hasOutletTemplate = !!OutletTemplate;
    var outletState = {
      render: {
        owner: Ember.getOwner ? Ember.getOwner(module.container) : undefined,
        into: undefined,
        outlet: 'main',
        name: 'application',
        controller: module.context,
        ViewClass: undefined,
        template: OutletTemplate
      },

      outlets: {}
    };

    var element = document.getElementById('ember-testing');
    var templateId = 0;

    if (hasOutletTemplate) {
      Ember.run(() => {
        toplevelView.setOutletState(outletState);
      });
    }

    context.render = function (template) {
      if (!template) {
        throw new Error('in a component integration test you must pass a template to `render()`');
      }
      if (Ember.isArray(template)) {
        template = template.join('');
      }
      if (typeof template === 'string') {
        template = Ember.Handlebars.compile(template);
      }

      var templateFullName = 'template:-undertest-' + ++templateId;
      this.registry.register(templateFullName, template);
      var stateToRender = {
        owner: Ember.getOwner ? Ember.getOwner(module.container) : undefined,
        into: undefined,
        outlet: 'main',
        name: 'index',
        controller: module.context,
        ViewClass: undefined,
        template: module.container.lookup(templateFullName),
        outlets: {}
      };

      if (hasOutletTemplate) {
        stateToRender.name = 'index';
        outletState.outlets.main = { render: stateToRender, outlets: {} };
      } else {
        stateToRender.name = 'application';
        outletState = { render: stateToRender, outlets: {} };
      }

      Ember.run(() => {
        toplevelView.setOutletState(outletState);
      });

      if (!hasRendered) {
        Ember.run(module.component, 'appendTo', '#ember-testing');
        hasRendered = true;
      }

      if (EmberENV._APPLICATION_TEMPLATE_WRAPPER !== false) {
        // ensure the element is based on the wrapping toplevel view
        // Ember still wraps the main application template with a
        // normal tagged view
        context._element = element = document.querySelector('#ember-testing > .ember-view');
      } else {
        context._element = element = document.querySelector('#ember-testing');
      }
    };

    context.$ = function (selector) {
      // emulates Ember internal behavor of `this.$` in a component
      // https://github.com/emberjs/ember.js/blob/v2.5.1/packages/ember-views/lib/views/states/has_element.js#L18
      return selector ? Ember.$(selector, element) : Ember.$(element);
    };

    context.set = function (key, value) {
      var ret = Ember.run(function () {
        return Ember.set(context, key, value);
      });

      if ((0, _hasEmberVersion.default)(2, 0)) {
        return ret;
      }
    };

    context.setProperties = function (hash) {
      var ret = Ember.run(function () {
        return Ember.setProperties(context, hash);
      });

      if ((0, _hasEmberVersion.default)(2, 0)) {
        return ret;
      }
    };

    context.get = function (key) {
      return Ember.get(context, key);
    };

    context.getProperties = function () {
      var args = Array.prototype.slice.call(arguments);
      return Ember.getProperties(context, args);
    };

    context.on = function (actionName, handler) {
      module.actionHooks[actionName] = handler;
    };

    context.send = function (actionName) {
      var hook = module.actionHooks[actionName];
      if (!hook) {
        throw new Error('integration testing template received unexpected action ' + actionName);
      }
      hook.apply(module.context, Array.prototype.slice.call(arguments, 1));
    };

    context.clearRender = function () {
      Ember.run(function () {
        toplevelView.setOutletState({
          render: {
            owner: module.container,
            into: undefined,
            outlet: 'main',
            name: 'application',
            controller: module.context,
            ViewClass: undefined,
            template: undefined
          },
          outlets: {}
        });
      });
    };
  }
});
define('ember-test-helpers/legacy-0-6-x/test-module-for-model', ['exports', 'require', 'ember-test-helpers/legacy-0-6-x/test-module'], function (exports, _require2, _testModule) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = class extends _testModule.default {
    constructor(modelName, description, callbacks) {
      super('model:' + modelName, description, callbacks);

      this.modelName = modelName;

      this.setupSteps.push(this.setupModel);
    }

    setupModel() {
      var container = this.container;
      var defaultSubject = this.defaultSubject;
      var callbacks = this.callbacks;
      var modelName = this.modelName;

      var adapterFactory = container.factoryFor ? container.factoryFor('adapter:application') : container.lookupFactory('adapter:application');
      if (!adapterFactory) {
        if (requirejs.entries['ember-data/adapters/json-api']) {
          adapterFactory = (0, _require2.default)('ember-data/adapters/json-api')['default'];
        }

        // when ember-data/adapters/json-api is provided via ember-cli shims
        // using Ember Data 1.x the actual JSONAPIAdapter isn't found, but the
        // above require statement returns a bizzaro object with only a `default`
        // property (circular reference actually)
        if (!adapterFactory || !adapterFactory.create) {
          adapterFactory = DS.JSONAPIAdapter || DS.FixtureAdapter;
        }

        var thingToRegisterWith = this.registry || this.container;
        thingToRegisterWith.register('adapter:application', adapterFactory);
      }

      callbacks.store = function () {
        var container = this.container;
        return container.lookup('service:store') || container.lookup('store:main');
      };

      if (callbacks.subject === defaultSubject) {
        callbacks.subject = function (options) {
          var container = this.container;

          return Ember.run(function () {
            var store = container.lookup('service:store') || container.lookup('store:main');
            return store.createRecord(modelName, options);
          });
        };
      }
    }
  };
});
define('ember-test-helpers/legacy-0-6-x/test-module', ['exports', 'ember-test-helpers/legacy-0-6-x/abstract-test-module', '@ember/test-helpers', 'ember-test-helpers/legacy-0-6-x/build-registry', '@ember/test-helpers/has-ember-version'], function (exports, _abstractTestModule, _testHelpers, _buildRegistry, _hasEmberVersion) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = class extends _abstractTestModule.default {
    constructor(subjectName, description, callbacks) {
      // Allow `description` to be omitted, in which case it should
      // default to `subjectName`
      if (!callbacks && typeof description === 'object') {
        callbacks = description;
        description = subjectName;
      }

      super(description || subjectName, callbacks);

      this.subjectName = subjectName;
      this.description = description || subjectName;
      this.resolver = this.callbacks.resolver || (0, _testHelpers.getResolver)();

      if (this.callbacks.integration && this.callbacks.needs) {
        throw new Error("cannot declare 'integration: true' and 'needs' in the same module");
      }

      if (this.callbacks.integration) {
        this.initIntegration(callbacks);
        delete callbacks.integration;
      }

      this.initSubject();
      this.initNeeds();
    }

    initIntegration(options) {
      if (options.integration === 'legacy') {
        throw new Error("`integration: 'legacy'` is only valid for component tests.");
      }
      this.isIntegration = true;
    }

    initSubject() {
      this.callbacks.subject = this.callbacks.subject || this.defaultSubject;
    }

    initNeeds() {
      this.needs = [this.subjectName];
      if (this.callbacks.needs) {
        this.needs = this.needs.concat(this.callbacks.needs);
        delete this.callbacks.needs;
      }
    }

    initSetupSteps() {
      this.setupSteps = [];
      this.contextualizedSetupSteps = [];

      if (this.callbacks.beforeSetup) {
        this.setupSteps.push(this.callbacks.beforeSetup);
        delete this.callbacks.beforeSetup;
      }

      this.setupSteps.push(this.setupContainer);
      this.setupSteps.push(this.setupContext);
      this.setupSteps.push(this.setupTestElements);
      this.setupSteps.push(this.setupAJAXListeners);
      this.setupSteps.push(this.setupPromiseListeners);

      if (this.callbacks.setup) {
        this.contextualizedSetupSteps.push(this.callbacks.setup);
        delete this.callbacks.setup;
      }
    }

    initTeardownSteps() {
      this.teardownSteps = [];
      this.contextualizedTeardownSteps = [];

      if (this.callbacks.teardown) {
        this.contextualizedTeardownSteps.push(this.callbacks.teardown);
        delete this.callbacks.teardown;
      }

      this.teardownSteps.push(this.teardownSubject);
      this.teardownSteps.push(this.teardownContainer);
      this.teardownSteps.push(this.teardownContext);
      this.teardownSteps.push(this.teardownTestElements);
      this.teardownSteps.push(this.teardownAJAXListeners);
      this.teardownSteps.push(this.teardownPromiseListeners);

      if (this.callbacks.afterTeardown) {
        this.teardownSteps.push(this.callbacks.afterTeardown);
        delete this.callbacks.afterTeardown;
      }
    }

    setupContainer() {
      if (this.isIntegration || this.isLegacy) {
        this._setupIntegratedContainer();
      } else {
        this._setupIsolatedContainer();
      }
    }

    setupContext() {
      var subjectName = this.subjectName;
      var container = this.container;

      var factory = function () {
        return container.factoryFor ? container.factoryFor(subjectName) : container.lookupFactory(subjectName);
      };

      super.setupContext({
        container: this.container,
        registry: this.registry,
        factory: factory,
        register() {
          var target = this.registry || this.container;
          return target.register.apply(target, arguments);
        }
      });

      if (Ember.setOwner) {
        Ember.setOwner(this.context, this.container.owner);
      }

      this.setupInject();
    }

    setupInject() {
      var module = this;
      var context = this.context;

      if (Ember.inject) {
        var keys = (Object.keys || keys)(Ember.inject);

        keys.forEach(function (typeName) {
          context.inject[typeName] = function (name, opts) {
            var alias = opts && opts.as || name;
            Ember.run(function () {
              Ember.set(context, alias, module.container.lookup(typeName + ':' + name));
            });
          };
        });
      }
    }

    teardownSubject() {
      var subject = this.cache.subject;

      if (subject) {
        Ember.run(function () {
          Ember.tryInvoke(subject, 'destroy');
        });
      }
    }

    teardownContainer() {
      var container = this.container;
      Ember.run(function () {
        container.destroy();
      });
    }

    defaultSubject(options, factory) {
      return factory.create(options);
    }

    // allow arbitrary named factories, like rspec let
    contextualizeCallbacks() {
      var callbacks = this.callbacks;
      var context = this.context;

      this.cache = this.cache || {};
      this.cachedCalls = this.cachedCalls || {};

      var keys = (Object.keys || keys)(callbacks);
      var keysLength = keys.length;

      if (keysLength) {
        var deprecatedContext = this._buildDeprecatedContext(this, context);
        for (var i = 0; i < keysLength; i++) {
          this._contextualizeCallback(context, keys[i], deprecatedContext);
        }
      }
    }

    _contextualizeCallback(context, key, callbackContext) {
      var _this = this;
      var callbacks = this.callbacks;
      var factory = context.factory;

      context[key] = function (options) {
        if (_this.cachedCalls[key]) {
          return _this.cache[key];
        }

        var result = callbacks[key].call(callbackContext, options, factory());

        _this.cache[key] = result;
        _this.cachedCalls[key] = true;

        return result;
      };
    }

    /*
      Builds a version of the passed in context that contains deprecation warnings
      for accessing properties that exist on the module.
    */
    _buildDeprecatedContext(module, context) {
      var deprecatedContext = Object.create(context);

      var keysForDeprecation = Object.keys(module);

      for (var i = 0, l = keysForDeprecation.length; i < l; i++) {
        this._proxyDeprecation(module, deprecatedContext, keysForDeprecation[i]);
      }

      return deprecatedContext;
    }

    /*
      Defines a key on an object to act as a proxy for deprecating the original.
    */
    _proxyDeprecation(obj, proxy, key) {
      if (typeof proxy[key] === 'undefined') {
        Object.defineProperty(proxy, key, {
          get() {
            Ember.deprecate('Accessing the test module property "' + key + '" from a callback is deprecated.', false, {
              id: 'ember-test-helpers.test-module.callback-context',
              until: '0.6.0'
            });
            return obj[key];
          }
        });
      }
    }

    _setupContainer(isolated) {
      var resolver = this.resolver;

      var items = (0, _buildRegistry.default)(!isolated ? resolver : Object.create(resolver, {
        resolve: {
          value() {}
        }
      }));

      this.container = items.container;
      this.registry = items.registry;

      if ((0, _hasEmberVersion.default)(1, 13)) {
        var thingToRegisterWith = this.registry || this.container;
        var router = resolver.resolve('router:main');
        router = router || Ember.Router.extend();
        thingToRegisterWith.register('router:main', router);
      }
    }

    _setupIsolatedContainer() {
      var resolver = this.resolver;
      this._setupContainer(true);

      var thingToRegisterWith = this.registry || this.container;

      for (var i = this.needs.length; i > 0; i--) {
        var fullName = this.needs[i - 1];
        var normalizedFullName = resolver.normalize(fullName);
        thingToRegisterWith.register(fullName, resolver.resolve(normalizedFullName));
      }

      if (!this.registry) {
        this.container.resolver = function () {};
      }
    }

    _setupIntegratedContainer() {
      this._setupContainer();
    }
  };
});
define('ember-test-helpers/wait', ['exports', '@ember/test-helpers/settled', '@ember/test-helpers'], function (exports, _settled, _testHelpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports._teardownPromiseListeners = exports._teardownAJAXHooks = exports._setupPromiseListeners = exports._setupAJAXHooks = undefined;
  Object.defineProperty(exports, '_setupAJAXHooks', {
    enumerable: true,
    get: function () {
      return _settled._setupAJAXHooks;
    }
  });
  Object.defineProperty(exports, '_setupPromiseListeners', {
    enumerable: true,
    get: function () {
      return _settled._setupPromiseListeners;
    }
  });
  Object.defineProperty(exports, '_teardownAJAXHooks', {
    enumerable: true,
    get: function () {
      return _settled._teardownAJAXHooks;
    }
  });
  Object.defineProperty(exports, '_teardownPromiseListeners', {
    enumerable: true,
    get: function () {
      return _settled._teardownPromiseListeners;
    }
  });
  exports.default = wait;


  /**
    Returns a promise that resolves when in a settled state (see `isSettled` for
    a definition of "settled state").
  
    @private
    @deprecated
    @param {Object} [options={}] the options to be used for waiting
    @param {boolean} [options.waitForTimers=true] should timers be waited upon
    @param {boolean} [options.waitForAjax=true] should $.ajax requests be waited upon
    @param {boolean} [options.waitForWaiters=true] should test waiters be waited upon
    @returns {Promise<void>} resolves when settled
  */
  function wait(options = {}) {
    if (typeof options !== 'object' || options === null) {
      options = {};
    }

    return (0, _testHelpers.waitUntil)(() => {
      let waitForTimers = 'waitForTimers' in options ? options.waitForTimers : true;
      let waitForAJAX = 'waitForAJAX' in options ? options.waitForAJAX : true;
      let waitForWaiters = 'waitForWaiters' in options ? options.waitForWaiters : true;

      let {
        hasPendingTimers,
        hasRunLoop,
        hasPendingRequests,
        hasPendingWaiters
      } = (0, _testHelpers.getSettledState)();

      if (waitForTimers && (hasPendingTimers || hasRunLoop)) {
        return false;
      }

      if (waitForAJAX && hasPendingRequests) {
        return false;
      }

      if (waitForWaiters && hasPendingWaiters) {
        return false;
      }

      return true;
    }, { timeout: Infinity });
  }
});
define("ember-window-mock/-private/mock/local-storage", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  function argumentError(f, required, given) {
    return new TypeError("Failed to execute '" + f + "' on 'Storage': " + required + 'arguments required, but only ' + given + 'present.');
  }

  class Storage {
    constructor() {
      Object.defineProperty(this, 'getItem', {
        value(key) {
          if (arguments.length < 1) {
            throw argumentError('getItem', 1, arguments.length);
          }
          if (this.hasOwnProperty(key)) {
            return this[key];
          }
          return null;
        },
        enumerable: false
      });

      Object.defineProperty(this, 'setItem', {
        value(key, value) {
          if (arguments.length < 2) {
            throw argumentError('setItem', 2, arguments.length);
          }
          this[key] = value + '';
        },
        enumerable: false
      });

      Object.defineProperty(this, 'removeItem', {
        value(key) {
          if (arguments.length < 1) {
            throw argumentError('removeItem', 1, arguments.length);
          }
          delete this[key];
        },
        enumerable: false
      });

      Object.defineProperty(this, 'key', {
        value(index) {
          if (arguments.length < 1) {
            throw argumentError('key', 1, arguments.length);
          }
          index = parseInt(index, 10);

          if (Number.isNaN(index)) {
            index = 0;
          }

          if (index < 0 || index > this.length - 1) {
            return null;
          }

          return Object.keys(this)[index];
        },
        enumerable: false
      });

      Object.defineProperty(this, 'clear', {
        value() {
          Object.keys(this).forEach(key => delete this[key]);
        },
        enumerable: false
      });

      Object.defineProperty(this, 'toString', {
        value() {
          return '[object Storage]';
        },
        enumerable: false
      });
    }

    get length() {
      return Object.keys(this).length;
    }
  }
  exports.default = Storage;
});
define('ember-window-mock/-private/mock/location', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = locationFactory;
  const mappedPorperties = [
  // 'href',
  'port', 'host', 'hostname', 'hash', 'origin', 'search', 'pathname', 'protocol', 'username', 'password'];

  /**
   * mock implementation of window.mock
   *
   * based on https://github.com/RyanV/jasmine-fake-window/blob/master/src/jasmine_fake_window.js
   *
   * @type {Object}
   * @public
   */
  function locationFactory(defaultUrl) {
    let location = {};
    let url = new URL(defaultUrl);

    mappedPorperties.forEach(propertyName => {
      Object.defineProperty(location, propertyName, {
        get() {
          return url[propertyName];
        },
        set(value) {
          url[propertyName] = value;
        }
      });
    });

    Object.defineProperty(location, 'href', {
      get() {
        return url.href;
      },
      set(value) {
        try {
          // check if it's a valid absolute URL
          new URL(value);
          url.href = value;
        } catch (e) {
          // absolute path
          if (value.charAt(0) === '/') {
            url.href = url.origin + value;
          } else {
            // replace last part of path with new value
            let parts = url.pathname.split('/').filter((item, index, array) => index !== array.length - 1).concat(value);
            url.href = url.origin + parts.join('/');
          }
        }
      }
    });

    location.reload = function () {};
    location.assign = function (url) {
      this.href = url;
    };
    location.replace = location.assign;
    location.toString = function () {
      return this.href;
    };

    return location;
  }
});
define('ember-window-mock/-private/mock/proxy', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = proxyFactory;
  function proxyFactory(original) {
    let holder = {};

    let proxy = new Proxy(original, {
      get(target, name) {
        if (name === '_reset') {
          return () => holder = {};
        }
        if (name in holder) {
          return holder[name];
        }
        if (typeof target[name] === 'function') {
          return target[name].bind(target);
        }
        if (typeof target[name] === 'object') {
          let proxy = proxyFactory(target[name]);
          holder[name] = proxy;
          return proxy;
        }
        return target[name];
      },
      set(target, name, value) {
        holder[name] = value;
        return true;
      },
      has(target, prop) {
        return prop in holder || prop in target;
      },
      deleteProperty(target, prop) {
        delete holder[prop];
        delete target[prop];
        return true;
      }
    });

    return proxy;
  }
});
define('ember-window-mock/-private/setup-window-mock', ['exports', 'ember-window-mock'], function (exports, _emberWindowMock) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = setupWindowMock;


  //
  // Used to reset window-mock for a test.
  //
  // NOTE: the `hooks = self` is for mocha support
  //
  function setupWindowMock(hooks = self) {
    hooks.beforeEach(_emberWindowMock.reset);
  }
});
define('ember-window-mock/-private/window', ['exports', 'ember-window-mock/-private/mock/location', 'ember-window-mock/-private/mock/local-storage', 'ember-window-mock/-private/mock/proxy'], function (exports, _location, _localStorage, _proxy) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.reset = reset;


  const originalWindow = window;

  let location = (0, _location.default)(originalWindow.location.href);
  let localStorage = new _localStorage.default();
  let holder = {};

  function noop() {}

  exports.default = new Proxy(window, {
    get(target, name) {
      switch (name) {
        case 'location':
          return location;
        case 'localStorage':
          return localStorage;
        case 'alert':
        case 'confirm':
        case 'prompt':
          return name in holder ? holder[name] : noop;
        default:
          if (name in holder) {
            return holder[name];
          }
          if (typeof window[name] === 'function') {
            return window[name].bind(window);
          }
          if (typeof window[name] === 'object') {
            let proxy = (0, _proxy.default)(window[name]);
            holder[name] = proxy;
            return proxy;
          }
          return target[name];
      }
    },
    set(target, name, value, receiver) {
      switch (name) {
        case 'location':
          // setting window.location is equivalent to setting window.location.href
          receiver.location.href = value;
          return true;
        default:
          holder[name] = value;
          return true;
      }
    },
    has(target, prop) {
      return prop in holder || prop in target;
    },
    deleteProperty(target, prop) {
      delete holder[prop];
      delete target[prop];
      return true;
    }
  });
  function reset() {
    location = (0, _location.default)(originalWindow.location.href);
    localStorage = new _localStorage.default();
    holder = {};
  }
});
define('ember-window-mock/index', ['exports', 'ember-window-mock/-private/window', 'ember-window-mock/-private/setup-window-mock'], function (exports, _window, _setupWindowMock) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _window.default;
    }
  });
  Object.defineProperty(exports, 'reset', {
    enumerable: true,
    get: function () {
      return _window.reset;
    }
  });
  Object.defineProperty(exports, 'setupWindowMock', {
    enumerable: true,
    get: function () {
      return _setupWindowMock.default;
    }
  });
});
define('cifunhi/tests/helpers/create-offline-ref', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = createOfflineRef;


  /**
   * Creates an offline firebase reference with optional initial data and url.
   *
   * Be sure to `stubfirebase()` and `unstubfirebase()` in your tests!
   *
   * @param  {!Object} [initialData]
   * @param  {string} [url]
   * @param  {string} [apiKey]
   * @return {!firebase.database.Reference}
   */
  function createOfflineRef(initialData, url = 'https://emberfire-tests-2c814.firebaseio.com', apiKey = 'AIzaSyC9-ndBb1WR05rRF1msVQDV6EBqB752m6o') {

    if (!_firebase.default._unStub) {
      throw new Error('Please use stubFirebase() before calling this method');
    }

    const config = {
      apiKey: apiKey,
      authDomain: 'emberfire-tests-2c814.firebaseapp.com',
      databaseURL: url,
      storageBucket: ''
    };

    let app;

    try {
      app = _firebase.default.app();
    } catch (e) {
      app = _firebase.default.initializeApp(config);
    }

    const ref = app.database().ref();

    app.database().goOffline(); // must be called after the ref is created

    if (initialData) {
      ref.set(initialData);
    }

    return ref;
  }
});
define('cifunhi/tests/helpers/destroy-firebase-apps', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyFirebaseApps;


  const { run } = Ember;

  /**
   * Destroy all Firebase apps.
   */
  function destroyFirebaseApps() {
    const deletions = _firebase.default.apps.map(app => app.delete());
    Ember.RSVP.all(deletions).then(() => run(() => {
      // NOOP to delay run loop until the apps are destroyed
    }));
  }
});
define('cifunhi/tests/helpers/ember-power-select', ['exports', 'ember-power-select/test-support/helpers'], function (exports, _helpers) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.selectChoose = exports.touchTrigger = exports.nativeTouch = exports.clickTrigger = exports.typeInSearch = exports.triggerKeydown = exports.nativeMouseUp = exports.nativeMouseDown = exports.findContains = undefined;
  exports.default = deprecatedRegisterHelpers;


  function deprecateHelper(fn, name) {
    return function (...args) {
      (true && !(false) && Ember.deprecate(`DEPRECATED \`import { ${name} } from '../../tests/helpers/ember-power-select';\` is deprecated. Please, replace it with \`import { ${name} } from 'ember-power-select/test-support/helpers';\``, false, { until: '1.11.0', id: `ember-power-select-test-support-${name}` }));

      return fn(...args);
    };
  }

  let findContains = deprecateHelper(_helpers.findContains, 'findContains');
  let nativeMouseDown = deprecateHelper(_helpers.nativeMouseDown, 'nativeMouseDown');
  let nativeMouseUp = deprecateHelper(_helpers.nativeMouseUp, 'nativeMouseUp');
  let triggerKeydown = deprecateHelper(_helpers.triggerKeydown, 'triggerKeydown');
  let typeInSearch = deprecateHelper(_helpers.typeInSearch, 'typeInSearch');
  let clickTrigger = deprecateHelper(_helpers.clickTrigger, 'clickTrigger');
  let nativeTouch = deprecateHelper(_helpers.nativeTouch, 'nativeTouch');
  let touchTrigger = deprecateHelper(_helpers.touchTrigger, 'touchTrigger');
  let selectChoose = deprecateHelper(_helpers.selectChoose, 'selectChoose');

  function deprecatedRegisterHelpers() {
    (true && !(false) && Ember.deprecate("DEPRECATED `import registerPowerSelectHelpers from '../../tests/helpers/ember-power-select';` is deprecated. Please, replace it with `import registerPowerSelectHelpers from 'ember-power-select/test-support/helpers';`", false, { until: '1.11.0', id: 'ember-power-select-test-support-register-helpers' }));

    return (0, _helpers.default)();
  }

  exports.findContains = findContains;
  exports.nativeMouseDown = nativeMouseDown;
  exports.nativeMouseUp = nativeMouseUp;
  exports.triggerKeydown = triggerKeydown;
  exports.typeInSearch = typeInSearch;
  exports.clickTrigger = clickTrigger;
  exports.nativeTouch = nativeTouch;
  exports.touchTrigger = touchTrigger;
  exports.selectChoose = selectChoose;
});
define('cifunhi/tests/helpers/replace-app-ref', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = replaceAppRef;
  /**
   * Updates the supplied app adapter's Firebase reference.
   *
   * @param  {!Ember.Application} app
   * @param  {!firebase.database.Reference} ref
   * @param  {string} [model]  The model, if overriding a model specific adapter
   */
  function replaceAppRef(app, ref, model = 'application') {
    app.register('service:firebaseMock', ref, { instantiate: false, singleton: true });
    app.inject('adapter:firebase', 'firebase', 'service:firebaseMock');
    app.inject('adapter:' + model, 'firebase', 'service:firebaseMock');
  }
});
define('cifunhi/tests/helpers/replace-firebase-app-service', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = replaceFirebaseAppService;
  /**
   * Replaces the `firebaseApp` service with your own using injection overrides.
   *
   * This is usually not needed in test modules, where you can re-register over
   * existing names in the registry, but in acceptance tests, some registry/inject
   * magic is needed.
   *
   * @param  {!Ember.Application} app
   * @param  {!Object} newService
   */
  function replaceFirebaseAppService(app, newService) {
    app.register('service:firebaseAppMock', newService, { instantiate: false, singleton: true });
    app.inject('torii-provider:firebase', 'firebaseApp', 'service:firebaseAppMock');
    app.inject('torii-adapter:firebase', 'firebaseApp', 'service:firebaseAppMock');
  }
});
define('cifunhi/tests/helpers/stub-firebase', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = stubFirebase;


  /**
   * When a reference is in offline mode it will not call any callbacks
   * until it goes online and resyncs. The ref will have already
   * updated its internal cache with the changed values so we shortcut
   * the process and call the supplied callbacks immediately (asynchronously).
   */
  function stubFirebase() {
    // check for existing stubbing
    if (!_firebase.default._unStub) {
      var originalSet = _firebase.default.database.Reference.prototype.set;
      var originalUpdate = _firebase.default.database.Reference.prototype.update;
      var originalRemove = _firebase.default.database.Reference.prototype.remove;

      _firebase.default._unStub = function () {
        _firebase.default.database.Reference.prototype.set = originalSet;
        _firebase.default.database.Reference.prototype.update = originalUpdate;
        _firebase.default.database.Reference.prototype.remove = originalRemove;
      };

      _firebase.default.database.Reference.prototype.set = function (data, cb) {
        originalSet.call(this, data);
        if (typeof cb === 'function') {
          setTimeout(cb, 0);
        }
      };

      _firebase.default.database.Reference.prototype.update = function (data, cb) {
        originalUpdate.call(this, data);
        if (typeof cb === 'function') {
          setTimeout(cb, 0);
        }
      };

      _firebase.default.database.Reference.prototype.remove = function (cb) {
        originalRemove.call(this);
        if (typeof cb === 'function') {
          setTimeout(cb, 0);
        }
      };
    }
  }
});
define('cifunhi/tests/helpers/torii', ['exports', 'cifunhi/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.stubValidSession = stubValidSession;


  const {
    torii: { sessionServiceName }
  } = _environment.default;

  function stubValidSession(application, sessionData) {
    let session = application.__container__.lookup(`service:${sessionServiceName}`);

    let sm = session.get('stateMachine');
    Ember.run(() => {
      sm.send('startOpen');
      sm.send('finishOpen', sessionData);
    });
  }
});
define('cifunhi/tests/helpers/unstub-firebase', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = unstubFirebase;
  function unstubFirebase() {
    if (typeof _firebase.default._unStub === 'function') {
      _firebase.default._unStub();
      delete _firebase.default._unStub;
    }
  }
});
define('cifunhi/tests/integration/components/kid-anexos-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | kid-anexos', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "0Ja0GWJo",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"kid-anexos\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "mx7Y0u5E",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"kid-anexos\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('cifunhi/tests/integration/components/kid-documents-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | kid-documents', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "X/rMwMRl",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"kid-documents\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "a4YwDPtN",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"kid-documents\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('cifunhi/tests/integration/components/kid-general-data-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | kid-general-data', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "SPoUENFf",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"kid-general-data\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "si/VbRD1",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"kid-general-data\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('cifunhi/tests/integration/components/pregunta-abierta-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | pregunta-abierta', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "T1iXY2s4",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"pregunta-abierta\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "8kvIT8R4",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"pregunta-abierta\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('cifunhi/tests/integration/components/pregunta-una-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | pregunta-una', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "haLSKpRX",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"pregunta-una\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "C34VWTwx",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"pregunta-una\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('cifunhi/tests/integration/components/pregunta-varias-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | pregunta-varias', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "RH79PL/U",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"pregunta-varias\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "ZmJswZzD",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"pregunta-varias\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('cifunhi/tests/lint/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('adapters/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass ESLint\n\n');
  });

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/kid-anexos.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/kid-anexos.js should pass ESLint\n\n64:15 - \'Swal\' is not defined. (no-undef)\n83:17 - \'anexo\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('components/kid-documents.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/kid-documents.js should pass ESLint\n\n22:5 - \'$\' is not defined. (no-undef)');
  });

  QUnit.test('components/kid-general-data.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/kid-general-data.js should pass ESLint\n\n');
  });

  QUnit.test('components/pregunta-abierta.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/pregunta-abierta.js should pass ESLint\n\n8:13 - \'swal\' is not defined. (no-undef)');
  });

  QUnit.test('components/pregunta-una.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/pregunta-una.js should pass ESLint\n\n8:13 - Unexpected console statement. (no-console)\n13:17 - Unexpected console statement. (no-console)\n19:13 - \'swal\' is not defined. (no-undef)');
  });

  QUnit.test('components/pregunta-varias.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/pregunta-varias.js should pass ESLint\n\n8:13 - Unexpected console statement. (no-console)\n12:17 - Unexpected console statement. (no-console)\n18:13 - \'swal\' is not defined. (no-undef)');
  });

  QUnit.test('controllers/administrador.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/administrador.js should pass ESLint\n\n5:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n7:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n7:12 - \'swal\' is not defined. (no-undef)\n8:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n9:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n10:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n11:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n12:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n13:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n14:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n15:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n16:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n17:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n18:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n19:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n20:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n22:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)');
  });

  QUnit.test('controllers/administrador/cuestionarios/aplicar.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/administrador/cuestionarios/aplicar.js should pass ESLint\n\n33:4 - Unexpected console statement. (no-console)');
  });

  QUnit.test('controllers/administrador/cuestionarios/editar.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/administrador/cuestionarios/editar.js should pass ESLint\n\n7:13 - Unexpected console statement. (no-console)\n13:17 - Unexpected console statement. (no-console)\n17:13 - Unexpected console statement. (no-console)\n34:17 - \'swal\' is not defined. (no-undef)');
  });

  QUnit.test('controllers/administrador/cuestionarios/index.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/administrador/cuestionarios/index.js should pass ESLint\n\n6:13 - \'swal\' is not defined. (no-undef)');
  });

  QUnit.test('controllers/administrador/cuestionarios/nuevo.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/administrador/cuestionarios/nuevo.js should pass ESLint\n\n7:13 - Unexpected console statement. (no-console)\n26:17 - Unexpected console statement. (no-console)\n30:13 - Unexpected console statement. (no-console)\n47:17 - \'swal\' is not defined. (no-undef)');
  });

  QUnit.test('controllers/administrador/expedientes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/administrador/expedientes/index.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/administrador/expedientes/nuevo/anexos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/administrador/expedientes/nuevo/anexos.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/administrador/expedientes/nuevo/datos-generales.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/administrador/expedientes/nuevo/datos-generales.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/administrador/expedientes/nuevo/documentos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/administrador/expedientes/nuevo/documentos.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/administrador/expedientes/ver/cuestionarios.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/administrador/expedientes/ver/cuestionarios.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/administrador/expedientes/ver/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/administrador/expedientes/ver/index.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/administrador/fotos.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/administrador/fotos.js should pass ESLint\n\n66:15 - \'Swal\' is not defined. (no-undef)');
  });

  QUnit.test('controllers/cancel.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/cancel.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/cuestionario.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/cuestionario.js should pass ESLint\n\n19:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n20:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n21:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n22:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n24:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n25:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n26:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n27:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n28:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n29:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n30:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n31:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n32:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n33:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n34:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n35:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)\n36:2 - Mixed spaces and tabs. (no-mixed-spaces-and-tabs)');
  });

  QUnit.test('controllers/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/index.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/login.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/login.js should pass ESLint\n\n17:31 - \'validations\' is defined but never used. (no-unused-vars)\n47:31 - \'validations\' is defined but never used. (no-unused-vars)\n60:13 - Unexpected console statement. (no-console)\n70:11 - \'swal\' is not defined. (no-undef)\n76:17 - \'error\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('controllers/success.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/success.js should pass ESLint\n\n');
  });

  QUnit.test('models/admin.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/admin.js should pass ESLint\n\n');
  });

  QUnit.test('models/anexo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/anexo.js should pass ESLint\n\n');
  });

  QUnit.test('models/answer-kid.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/answer-kid.js should pass ESLint\n\n');
  });

  QUnit.test('models/answer.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/answer.js should pass ESLint\n\n');
  });

  QUnit.test('models/caracteristicas-fisicas.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/caracteristicas-fisicas.js should pass ESLint\n\n');
  });

  QUnit.test('models/cuestionario.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/cuestionario.js should pass ESLint\n\n');
  });

  QUnit.test('models/datos-generales.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/datos-generales.js should pass ESLint\n\n');
  });

  QUnit.test('models/documento.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/documento.js should pass ESLint\n\n');
  });

  QUnit.test('models/kid.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'models/kid.js should pass ESLint\n\n12:13 - Use brace expansion (ember/use-brace-expansion)');
  });

  QUnit.test('models/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/photo.js should pass ESLint\n\n');
  });

  QUnit.test('models/question-kid.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/question-kid.js should pass ESLint\n\n');
  });

  QUnit.test('models/question.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/question.js should pass ESLint\n\n');
  });

  QUnit.test('models/questionary-kid.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/questionary-kid.js should pass ESLint\n\n');
  });

  QUnit.test('models/servicio-medico.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/servicio-medico.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'router.js should pass ESLint\n\n19:9 - Unexpected capital letter in route\'s name (ember/no-capital-letters-in-routes)\n24:9 - Unexpected capital letter in route\'s name (ember/no-capital-letters-in-routes)');
  });

  QUnit.test('routes/administrador.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/cuestionarios/aplicar.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/cuestionarios/aplicar.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/cuestionarios/detalles.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/cuestionarios/detalles.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/cuestionarios/editar.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/cuestionarios/editar.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/cuestionarios/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/cuestionarios/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/cuestionarios/nuevo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/cuestionarios/nuevo.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/editar.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/editar.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/editar/anexos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/editar/anexos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/editar/datos-generales.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/editar/datos-generales.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/editar/documentos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/editar/documentos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/nuevo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/nuevo.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/nuevo/anexos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/nuevo/anexos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/nuevo/datos-generales.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/nuevo/datos-generales.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/nuevo/documentos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/nuevo/documentos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/ver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/ver.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/ver/cuestionarios.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/ver/cuestionarios.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/expedientes/ver/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/expedientes/ver/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/fotos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/fotos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/administrador/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/administrador/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cancel.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cancel.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cuestionario.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cuestionario.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cuestionarios/aplicar.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cuestionarios/aplicar.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cuestionarios/editar.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cuestionarios/editar.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cuestionarios/nuevo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cuestionarios/nuevo.js should pass ESLint\n\n');
  });

  QUnit.test('routes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/login.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/login.js should pass ESLint\n\n');
  });

  QUnit.test('routes/success.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/success.js should pass ESLint\n\n');
  });

  QUnit.test('services/ajax.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/ajax.js should pass ESLint\n\n');
  });

  QUnit.test('torii-adapters/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'torii-adapters/application.js should pass ESLint\n\n');
  });
});
define('cifunhi/tests/lint/templates.template.lint-test', [], function () {
  'use strict';

  QUnit.module('TemplateLint');

  QUnit.test('cifunhi/templates/administrador.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador.hbs\n  11:13  error  Incorrect indentation of htmlAttribute \'style\' beginning at L11:C13. Expected \'style\' to be at L12:C10.  attribute-indentation\n  11:57  error  Incorrect indentation of htmlAttribute \'src\' beginning at L11:C57. Expected \'src\' to be at L13:C10.  attribute-indentation\n  11:82  error  Incorrect indentation of htmlAttribute \'width\' beginning at L11:C82. Expected \'width\' to be at L14:C10.  attribute-indentation\n  11:95  error  Incorrect indentation of close bracket \'>\' for the element \'<img>\' beginning at L11:C95. Expected \'<img>\' to be at L15:C8.  attribute-indentation\n  13:12  error  Incorrect indentation of htmlAttribute \'style\' beginning at L13:C12. Expected \'style\' to be at L14:C9.  attribute-indentation\n  13:96  error  Incorrect indentation of htmlAttribute \'class\' beginning at L13:C96. Expected \'class\' to be at L15:C9.  attribute-indentation\n  13:119  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L13:C119. Expected \'<div>\' to be at L16:C7.  attribute-indentation\n  15:6  error  Incorrect indentation of close tag \'</div>\' for element \'<div>\' beginning at L15:C6. Expected \'</div>\' to be at L15:C7.  attribute-indentation\n  1:2  error  Incorrect indentation for `<style>` beginning at L1:C2. Expected `<style>` to be at an indentation of 0, but was found at 2.  block-indentation\n  16:19  error  Incorrect indentation for `link-to` beginning at L9:C6. Expected `{{/link-to}}` ending at L16:C19 to be at an indentation of 6 but was found at 7.  block-indentation\n  10:6  error  Incorrect indentation for `<div>` beginning at L10:C6. Expected `<div>` to be at an indentation of 8 but was found at 6.  block-indentation\n  13:7  error  Incorrect indentation for `<div>` beginning at L13:C7. Expected `<div>` to be at an indentation of 8 but was found at 7.  block-indentation\n  15:12  error  Incorrect indentation for `div` beginning at L13:C7. Expected `</div>` ending at L15:C12 to be at an indentation of 7 but was found at 6.  block-indentation\n  32:33  error  Incorrect indentation for `\n  $(document).ready(function(){\n    $(\'.sidenav\').sidenav();\n  });\n  ` beginning at L32:C33. Expected `\n  $(document).ready(function(){\n    $(\'.sidenav\').sidenav();\n  });\n  ` to be at an indentation of 4 but was found at 2.  block-indentation\n  11:8  error  img tags must have an alt attribute  img-alt-attributes\n  7:9  error  elements cannot have inline styles  no-inline-styles\n  10:11  error  elements cannot have inline styles  no-inline-styles\n  11:13  error  elements cannot have inline styles  no-inline-styles\n  13:12  error  elements cannot have inline styles  no-inline-styles\n  19:60  error  elements cannot have inline styles  no-inline-styles\n  38:7  error  elements cannot have inline styles  no-inline-styles\n  20:31  error  Interaction added to non-interactive element  no-invalid-interactive\n  27:23  error  Interaction added to non-interactive element  no-invalid-interactive\n');
  });

  QUnit.test('cifunhi/templates/administrador/cuestionarios/aplicar.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/cuestionarios/aplicar.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/cuestionarios/aplicar.hbs\n  7:5  error  Incorrect indentation of attribute \'options\' beginning at L7:C5. Expected \'options\' to be at L7:C4.  attribute-indentation\n  8:5  error  Incorrect indentation of attribute \'selected\' beginning at L8:C5. Expected \'selected\' to be at L8:C4.  attribute-indentation\n  9:5  error  Incorrect indentation of attribute \'onchange\' beginning at L9:C5. Expected \'onchange\' to be at L9:C4.  attribute-indentation\n  10:5  error  Incorrect indentation of attribute \'searchPlaceholder\' beginning at L10:C5. Expected \'searchPlaceholder\' to be at L10:C4.  attribute-indentation\n  11:5  error  Incorrect indentation of attribute \'searchField\' beginning at L11:C5. Expected \'searchField\' to be at L11:C4.  attribute-indentation\n  12:5  error  Incorrect indentation of block params \'as |kid|}}\' beginning at L12:C5. Expecting the block params to be at L12:C2.  attribute-indentation\n  12:13  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{power-select}}\' beginning at L12:C13. Expected \'{{power-select}}\' to be at L13:C2.  attribute-indentation\n  2:1  error  Incorrect indentation for `<h1>` beginning at L2:C1. Expected `<h1>` to be at an indentation of 2 but was found at 1.  block-indentation\n  3:1  error  Incorrect indentation for `{{log}}` beginning at L3:C1. Expected `{{log}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  4:1  error  Incorrect indentation for `<form>` beginning at L4:C1. Expected `<form>` to be at an indentation of 2 but was found at 1.  block-indentation\n  22:1  error  Incorrect indentation for `{{#if}}` beginning at L22:C1. Expected `{{#if}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  5:2  error  Incorrect indentation for `<p>` beginning at L5:C2. Expected `<p>` to be at an indentation of 3 but was found at 2.  block-indentation\n  6:2  error  Incorrect indentation for `{{#power-select}}` beginning at L6:C2. Expected `{{#power-select}}` to be at an indentation of 3 but was found at 2.  block-indentation\n  16:2  error  Incorrect indentation for `<p>` beginning at L16:C2. Expected `<p>` to be at an indentation of 3 but was found at 2.  block-indentation\n  18:2  error  Incorrect indentation for `<button>` beginning at L18:C2. Expected `<button>` to be at an indentation of 3 but was found at 2.  block-indentation\n  14:20  error  Incorrect indentation for `power-select` beginning at L6:C2. Expected `{{/power-select}}` ending at L14:C20 to be at an indentation of 2 but was found at 3.  block-indentation\n  13:5  error  Incorrect indentation for `<strong>` beginning at L13:C5. Expected `<strong>` to be at an indentation of 4 but was found at 5.  block-indentation\n  23:2  error  Incorrect indentation for `<h3>` beginning at L23:C2. Expected `<h3>` to be at an indentation of 3 but was found at 2.  block-indentation\n  3:1  error  Unexpected {{log}} usage.  no-log\n  4:16  error  you must use double quotes in templates  quotes\n  11:17  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/cuestionarios/detalles.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/cuestionarios/detalles.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/cuestionarios/detalles.hbs\n  2:4  error  Incorrect indentation for `<div>` beginning at L2:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  5:6  error  Incorrect indentation for `{{#each}}` beginning at L5:C6. Expected `{{#each}}` to be at an indentation of 2 but was found at 6.  block-indentation\n  3:8  error  Incorrect indentation for `<h5>` beginning at L3:C8. Expected `<h5>` to be at an indentation of 6 but was found at 8.  block-indentation\n  6:10  error  Incorrect indentation for `{{#if}}` beginning at L6:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  10:10  error  Incorrect indentation for `{{#if}}` beginning at L10:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  23:10  error  Incorrect indentation for `{{#if}}` beginning at L23:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  13:12  error  Incorrect indentation for `{{#each}}` beginning at L13:C12. Expected `{{#each}}` to be at an indentation of 14 but was found at 12.  block-indentation\n  14:12  error  Incorrect indentation for `<div>` beginning at L14:C12. Expected `<div>` to be at an indentation of 14 but was found at 12.  block-indentation\n  15:12  error  Incorrect indentation for `<label>` beginning at L15:C12. Expected `<label>` to be at an indentation of 14 but was found at 12.  block-indentation\n  16:16  error  Incorrect indentation for `<input>` beginning at L16:C16. Expected `<input>` to be at an indentation of 14 but was found at 16.  block-indentation\n  17:16  error  Incorrect indentation for `<span>` beginning at L17:C16. Expected `<span>` to be at an indentation of 14 but was found at 16.  block-indentation\n  26:12  error  Incorrect indentation for `{{#each}}` beginning at L26:C12. Expected `{{#each}}` to be at an indentation of 14 but was found at 12.  block-indentation\n  27:12  error  Incorrect indentation for `<div>` beginning at L27:C12. Expected `<div>` to be at an indentation of 14 but was found at 12.  block-indentation\n  28:12  error  Incorrect indentation for `<label>` beginning at L28:C12. Expected `<label>` to be at an indentation of 14 but was found at 12.  block-indentation\n  29:16  error  Incorrect indentation for `<input>` beginning at L29:C16. Expected `<input>` to be at an indentation of 14 but was found at 16.  block-indentation\n  30:16  error  Incorrect indentation for `<span>` beginning at L30:C16. Expected `<span>` to be at an indentation of 14 but was found at 16.  block-indentation\n  29:28  error  Unnecessary string concatenation. Use {{pregunta.id}} instead of "{{pregunta.id}}".  no-unnecessary-concat\n  6:34  error  you must use double quotes in templates  quotes\n  10:34  error  you must use double quotes in templates  quotes\n  23:34  error  you must use double quotes in templates  quotes\n  16:16  error  Self-closing a void element is redundant  self-closing-void-elements\n  29:16  error  Self-closing a void element is redundant  self-closing-void-elements\n');
  });

  QUnit.test('cifunhi/templates/administrador/cuestionarios/editar.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/cuestionarios/editar.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/cuestionarios/editar.hbs\n  3:5  error  Incorrect indentation of htmlAttribute \'class\' beginning at L3:C5. Expected \'class\' to be at L4:C4.  attribute-indentation\n  3:51  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L3:C51. Expected \'data-position\' to be at L5:C4.  attribute-indentation\n  3:72  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L3:C72. Expected \'data-tooltip\' to be at L6:C4.  attribute-indentation\n  3:107  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L3:C107. Expected \'<a>\' to be at L7:C2.  attribute-indentation\n  7:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L7:C16. Expected \'class\' to be at L8:C6.  attribute-indentation\n  7:53  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L7:C53. Expected \'data-position\' to be at L9:C6.  attribute-indentation\n  7:74  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L7:C74. Expected \'data-tooltip\' to be at L10:C6.  attribute-indentation\n  7:97  error  Incorrect indentation of element modifier \'action\' beginning at L7:C97. Expected \'action\' to be at L11:C6.  attribute-indentation\n  7:133  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L7:C133. Expected \'<button>\' to be at L12:C4.  attribute-indentation\n  7:175  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L7:C175. Expected \'</button>\' to be at L7:C8.  attribute-indentation\n  8:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L8:C16. Expected \'class\' to be at L9:C6.  attribute-indentation\n  8:54  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L8:C54. Expected \'data-position\' to be at L10:C6.  attribute-indentation\n  8:75  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L8:C75. Expected \'data-tooltip\' to be at L11:C6.  attribute-indentation\n  8:100  error  Incorrect indentation of element modifier \'action\' beginning at L8:C100. Expected \'action\' to be at L12:C6.  attribute-indentation\n  8:135  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L8:C135. Expected \'<button>\' to be at L13:C4.  attribute-indentation\n  8:175  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L8:C175. Expected \'</button>\' to be at L8:C8.  attribute-indentation\n  9:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L9:C16. Expected \'class\' to be at L10:C6.  attribute-indentation\n  9:55  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L9:C55. Expected \'data-position\' to be at L11:C6.  attribute-indentation\n  9:76  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L9:C76. Expected \'data-tooltip\' to be at L12:C6.  attribute-indentation\n  9:107  error  Incorrect indentation of element modifier \'action\' beginning at L9:C107. Expected \'action\' to be at L13:C6.  attribute-indentation\n  9:139  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L9:C139. Expected \'<button>\' to be at L14:C4.  attribute-indentation\n  9:190  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L9:C190. Expected \'</button>\' to be at L9:C8.  attribute-indentation\n  15:18  error  Incorrect indentation of attribute \'type\' beginning at L15:C18. Expected \'type\' to be at L16:C12.  attribute-indentation\n  15:30  error  Incorrect indentation of attribute \'id\' beginning at L15:C30. Expected \'id\' to be at L17:C12.  attribute-indentation\n  15:54  error  Incorrect indentation of attribute \'value\' beginning at L15:C54. Expected \'value\' to be at L18:C12.  attribute-indentation\n  15:74  error  Incorrect indentation of attribute \'class\' beginning at L15:C74. Expected \'class\' to be at L19:C12.  attribute-indentation\n  15:90  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L15:C90. Expected \'{{input}}\' to be at L20:C10.  attribute-indentation\n  14:8  error  Incorrect indentation for `<div>` beginning at L14:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  19:8  error  Incorrect indentation for `<div>` beginning at L19:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  24:8  error  Incorrect indentation for `{{#if}}` beginning at L24:C8. Expected `{{#if}}` to be at an indentation of 6 but was found at 8.  block-indentation\n  26:10  error  Incorrect indentation for `{{#if}}` beginning at L26:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  29:10  error  Incorrect indentation for `{{#if}}` beginning at L29:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  32:10  error  Incorrect indentation for `{{#if}}` beginning at L32:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  27:14  error  Incorrect indentation for `{{pregunta-abierta}}` beginning at L27:C14. Expected `{{pregunta-abierta}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  30:14  error  Incorrect indentation for `{{pregunta-varias}}` beginning at L30:C14. Expected `{{pregunta-varias}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  33:14  error  Incorrect indentation for `{{pregunta-una}}` beginning at L33:C14. Expected `{{pregunta-una}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  26:34  error  you must use double quotes in templates  quotes\n  29:34  error  you must use double quotes in templates  quotes\n  32:34  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/cuestionarios/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/cuestionarios/index.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/cuestionarios/index.hbs\n  13:14  error  Incorrect indentation of positional param \'administrador.cuestionarios.nuevo\' beginning at L13:C14. Expected \'administrador.cuestionarios.nuevo\' to be at L14:C5.  attribute-indentation\n  13:50  error  Incorrect indentation of attribute \'class\' beginning at L13:C50. Expected \'class\' to be at L15:C5.  attribute-indentation\n  13:86  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{link-to}}\' beginning at L13:C86. Expected \'{{link-to}}\' to be at L16:C3.  attribute-indentation\n  38:26  error  Incorrect indentation of htmlAttribute \'class\' beginning at L38:C26. Expected \'class\' to be at L39:C20.  attribute-indentation\n  38:84  error  Incorrect indentation of element modifier \'action\' beginning at L38:C84. Expected \'action\' to be at L40:C20.  attribute-indentation\n  38:113  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L38:C113. Expected \'<button>\' to be at L41:C18.  attribute-indentation\n  38:163  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L38:C163. Expected \'</button>\' to be at L38:C18.  attribute-indentation\n  39:26  error  Incorrect indentation of htmlAttribute \'class\' beginning at L39:C26. Expected \'class\' to be at L40:C20.  attribute-indentation\n  39:83  error  Incorrect indentation of element modifier \'action\' beginning at L39:C83. Expected \'action\' to be at L41:C20.  attribute-indentation\n  39:129  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L39:C129. Expected \'<button>\' to be at L42:C18.  attribute-indentation\n  39:179  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L39:C179. Expected \'</button>\' to be at L39:C18.  attribute-indentation\n  40:26  error  Incorrect indentation of htmlAttribute \'class\' beginning at L40:C26. Expected \'class\' to be at L41:C20.  attribute-indentation\n  40:79  error  Incorrect indentation of element modifier \'action\' beginning at L40:C79. Expected \'action\' to be at L42:C20.  attribute-indentation\n  40:111  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L40:C111. Expected \'<button>\' to be at L43:C18.  attribute-indentation\n  40:157  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L40:C157. Expected \'</button>\' to be at L40:C18.  attribute-indentation\n  41:26  error  Incorrect indentation of htmlAttribute \'class\' beginning at L41:C26. Expected \'class\' to be at L42:C20.  attribute-indentation\n  41:86  error  Incorrect indentation of element modifier \'action\' beginning at L41:C86. Expected \'action\' to be at L43:C20.  attribute-indentation\n  41:119  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L41:C119. Expected \'<button>\' to be at L44:C18.  attribute-indentation\n  41:172  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L41:C172. Expected \'</button>\' to be at L41:C18.  attribute-indentation\n  22:0  error  Incorrect indentation for `{{#if}}` beginning at L22:C0. Expected `{{#if}}` to be at an indentation of 2 but was found at 0.  block-indentation\n  20:6  error  Incorrect indentation for `div` beginning at L7:C2. Expected `</div>` ending at L20:C6 to be at an indentation of 2 but was found at 0.  block-indentation\n  8:1  error  Incorrect indentation for `<div>` beginning at L8:C1. Expected `<div>` to be at an indentation of 4 but was found at 1.  block-indentation\n  12:1  error  Incorrect indentation for `<div>` beginning at L12:C1. Expected `<div>` to be at an indentation of 4 but was found at 1.  block-indentation\n  9:2  error  Incorrect indentation for `<h3>` beginning at L9:C2. Expected `<h3>` to be at an indentation of 3 but was found at 2.  block-indentation\n  14:4  error  Incorrect indentation for `<i>` beginning at L14:C4. Expected `<i>` to be at an indentation of 5 but was found at 4.  block-indentation\n  23:6  error  Incorrect indentation for `<table>` beginning at L23:C6. Expected `<table>` to be at an indentation of 2 but was found at 6.  block-indentation\n  50:0  error  Incorrect indentation for `<h3>` beginning at L50:C0. Expected `<h3>` to be at an indentation of 2 but was found at 0.  block-indentation\n  26:14  error  Incorrect indentation for `<th>` beginning at L26:C14. Expected `<th>` to be at an indentation of 12 but was found at 14.  block-indentation\n  27:14  error  Incorrect indentation for `<th>` beginning at L27:C14. Expected `<th>` to be at an indentation of 12 but was found at 14.  block-indentation\n  28:14  error  Incorrect indentation for `<th>` beginning at L28:C14. Expected `<th>` to be at an indentation of 12 but was found at 14.  block-indentation\n  33:14  error  Incorrect indentation for `{{#unless}}` beginning at L33:C14. Expected `{{#unless}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  34:14  error  Incorrect indentation for `<tr>` beginning at L34:C14. Expected `<tr>` to be at an indentation of 16 but was found at 14.  block-indentation\n  43:18  error  Incorrect indentation for `tr` beginning at L34:C14. Expected `</tr>` ending at L43:C18 to be at an indentation of 14 but was found at 13.  block-indentation\n  38:93  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/cuestionarios/nuevo.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/cuestionarios/nuevo.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/cuestionarios/nuevo.hbs\n  3:5  error  Incorrect indentation of htmlAttribute \'class\' beginning at L3:C5. Expected \'class\' to be at L4:C4.  attribute-indentation\n  3:51  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L3:C51. Expected \'data-position\' to be at L5:C4.  attribute-indentation\n  3:72  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L3:C72. Expected \'data-tooltip\' to be at L6:C4.  attribute-indentation\n  3:107  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L3:C107. Expected \'<a>\' to be at L7:C2.  attribute-indentation\n  7:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L7:C16. Expected \'class\' to be at L8:C6.  attribute-indentation\n  7:53  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L7:C53. Expected \'data-position\' to be at L9:C6.  attribute-indentation\n  7:74  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L7:C74. Expected \'data-tooltip\' to be at L10:C6.  attribute-indentation\n  7:97  error  Incorrect indentation of element modifier \'action\' beginning at L7:C97. Expected \'action\' to be at L11:C6.  attribute-indentation\n  7:133  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L7:C133. Expected \'<button>\' to be at L12:C4.  attribute-indentation\n  7:175  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L7:C175. Expected \'</button>\' to be at L7:C8.  attribute-indentation\n  8:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L8:C16. Expected \'class\' to be at L9:C6.  attribute-indentation\n  8:54  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L8:C54. Expected \'data-position\' to be at L10:C6.  attribute-indentation\n  8:75  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L8:C75. Expected \'data-tooltip\' to be at L11:C6.  attribute-indentation\n  8:100  error  Incorrect indentation of element modifier \'action\' beginning at L8:C100. Expected \'action\' to be at L12:C6.  attribute-indentation\n  8:135  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L8:C135. Expected \'<button>\' to be at L13:C4.  attribute-indentation\n  8:175  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L8:C175. Expected \'</button>\' to be at L8:C8.  attribute-indentation\n  9:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L9:C16. Expected \'class\' to be at L10:C6.  attribute-indentation\n  9:55  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L9:C55. Expected \'data-position\' to be at L11:C6.  attribute-indentation\n  9:76  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L9:C76. Expected \'data-tooltip\' to be at L12:C6.  attribute-indentation\n  9:107  error  Incorrect indentation of element modifier \'action\' beginning at L9:C107. Expected \'action\' to be at L13:C6.  attribute-indentation\n  9:139  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L9:C139. Expected \'<button>\' to be at L14:C4.  attribute-indentation\n  9:190  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L9:C190. Expected \'</button>\' to be at L9:C8.  attribute-indentation\n  15:18  error  Incorrect indentation of attribute \'type\' beginning at L15:C18. Expected \'type\' to be at L16:C12.  attribute-indentation\n  15:30  error  Incorrect indentation of attribute \'id\' beginning at L15:C30. Expected \'id\' to be at L17:C12.  attribute-indentation\n  15:54  error  Incorrect indentation of attribute \'value\' beginning at L15:C54. Expected \'value\' to be at L18:C12.  attribute-indentation\n  15:74  error  Incorrect indentation of attribute \'class\' beginning at L15:C74. Expected \'class\' to be at L19:C12.  attribute-indentation\n  15:90  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L15:C90. Expected \'{{input}}\' to be at L20:C10.  attribute-indentation\n  14:8  error  Incorrect indentation for `<div>` beginning at L14:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  19:8  error  Incorrect indentation for `<div>` beginning at L19:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  24:8  error  Incorrect indentation for `{{#if}}` beginning at L24:C8. Expected `{{#if}}` to be at an indentation of 6 but was found at 8.  block-indentation\n  26:10  error  Incorrect indentation for `{{#if}}` beginning at L26:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  29:10  error  Incorrect indentation for `{{#if}}` beginning at L29:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  32:10  error  Incorrect indentation for `{{#if}}` beginning at L32:C10. Expected `{{#if}}` to be at an indentation of 8 but was found at 10.  block-indentation\n  27:14  error  Incorrect indentation for `{{pregunta-abierta}}` beginning at L27:C14. Expected `{{pregunta-abierta}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  30:14  error  Incorrect indentation for `{{pregunta-varias}}` beginning at L30:C14. Expected `{{pregunta-varias}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  33:14  error  Incorrect indentation for `{{pregunta-una}}` beginning at L33:C14. Expected `{{pregunta-una}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  26:34  error  you must use double quotes in templates  quotes\n  29:34  error  you must use double quotes in templates  quotes\n  32:34  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/editar.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes/editar.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/editar/anexos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes/editar/anexos.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/editar/datos-generales.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes/editar/datos-generales.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/editar/documentos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes/editar/documentos.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/expedientes/index.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/expedientes/index.hbs\n  17:14  error  Incorrect indentation of positional param \'administrador.expedientes.nuevo.datosGenerales\' beginning at L17:C14. Expected \'administrador.expedientes.nuevo.datosGenerales\' to be at L18:C5.  attribute-indentation\n  17:63  error  Incorrect indentation of attribute \'class\' beginning at L17:C63. Expected \'class\' to be at L19:C5.  attribute-indentation\n  17:99  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{link-to}}\' beginning at L17:C99. Expected \'{{link-to}}\' to be at L20:C3.  attribute-indentation\n  1:7  error  Incorrect indentation for `\n\t.table-head{\n\t\tbackground: #717B8B;\n\t\tcolor: white;\n\t\ttext-transform: uppercase;\n\t\tletter-spacing: 1px;\n\t}\n\n` beginning at L1:C7. Expected `\n\t.table-head{\n\t\tbackground: #717B8B;\n\t\tcolor: white;\n\t\ttext-transform: uppercase;\n\t\tletter-spacing: 1px;\n\t}\n\n` to be at an indentation of 2 but was found at 1.  block-indentation\n  12:1  error  Incorrect indentation for `<div>` beginning at L12:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  16:1  error  Incorrect indentation for `<div>` beginning at L16:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  13:2  error  Incorrect indentation for `<h3>` beginning at L13:C2. Expected `<h3>` to be at an indentation of 3 but was found at 2.  block-indentation\n  18:4  error  Incorrect indentation for `<i>` beginning at L18:C4. Expected `<i>` to be at an indentation of 5 but was found at 4.  block-indentation\n  28:1  error  Incorrect indentation for `<table>` beginning at L28:C1. Expected `<table>` to be at an indentation of 2 but was found at 1.  block-indentation\n  45:8  error  Incorrect indentation for `table` beginning at L28:C1. Expected `</table>` ending at L45:C8 to be at an indentation of 1 but was found at 0.  block-indentation\n  29:1  error  Incorrect indentation for `<tr>` beginning at L29:C1. Expected `<tr>` to be at an indentation of 3 but was found at 1.  block-indentation\n  34:1  error  Incorrect indentation for `{{#each}}` beginning at L34:C1. Expected `{{#each}}` to be at an indentation of 3 but was found at 1.  block-indentation\n  30:2  error  Incorrect indentation for `<th>` beginning at L30:C2. Expected `<th>` to be at an indentation of 3 but was found at 2.  block-indentation\n  31:2  error  Incorrect indentation for `<th>` beginning at L31:C2. Expected `<th>` to be at an indentation of 3 but was found at 2.  block-indentation\n  32:2  error  Incorrect indentation for `<th>` beginning at L32:C2. Expected `<th>` to be at an indentation of 3 but was found at 2.  block-indentation\n  35:2  error  Incorrect indentation for `{{#unless}}` beginning at L35:C2. Expected `{{#unless}}` to be at an indentation of 3 but was found at 2.  block-indentation\n  36:2  error  Incorrect indentation for `<tr>` beginning at L36:C2. Expected `<tr>` to be at an indentation of 4 but was found at 2.  block-indentation\n  37:3  error  Incorrect indentation for `<td>` beginning at L37:C3. Expected `<td>` to be at an indentation of 4 but was found at 3.  block-indentation\n  38:3  error  Incorrect indentation for `<td>` beginning at L38:C3. Expected `<td>` to be at an indentation of 4 but was found at 3.  block-indentation\n  39:3  error  Incorrect indentation for `<td>` beginning at L39:C3. Expected `<td>` to be at an indentation of 4 but was found at 3.  block-indentation\n  40:4  error  Incorrect indentation for `<button>` beginning at L40:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  40:33  error  you must use double quotes in templates  quotes\n  28:1  error  Tables must have a table group (thead, tbody or tfoot).  table-groups\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/nuevo.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes/nuevo.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/nuevo/anexos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/expedientes/nuevo/anexos.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/expedientes/nuevo/anexos.hbs\n  6:14  error  Incorrect indentation of attribute \'model\' beginning at L6:C14. Expected \'model\' to be at L7:C3.  attribute-indentation\n  7:5  error  Incorrect indentation of attribute \'edit\' beginning at L7:C5. Expected \'edit\' to be at L8:C3.  attribute-indentation\n  7:14  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{kid-anexos}}\' beginning at L7:C14. Expected \'{{kid-anexos}}\' to be at L9:C1.  attribute-indentation\n  2:1  error  Incorrect indentation for `<h3>` beginning at L2:C1. Expected `<h3>` to be at an indentation of 2 but was found at 1.  block-indentation\n  3:1  error  Incorrect indentation for `<p>` beginning at L3:C1. Expected `<p>` to be at an indentation of 2 but was found at 1.  block-indentation\n  4:1  error  Incorrect indentation for `<p>` beginning at L4:C1. Expected `<p>` to be at an indentation of 2 but was found at 1.  block-indentation\n  6:1  error  Incorrect indentation for `{{kid-anexos}}` beginning at L6:C1. Expected `{{kid-anexos}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  10:1  error  Incorrect indentation for `<button>` beginning at L10:C1. Expected `<button>` to be at an indentation of 2 but was found at 1.  block-indentation\n  10:30  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/nuevo/datos-generales.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/expedientes/nuevo/datos-generales.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/expedientes/nuevo/datos-generales.hbs\n  5:2  error  Incorrect indentation of attribute \'ver\' beginning at L5:C2. Expected \'ver\' to be at L5:C3.  attribute-indentation\n  6:2  error  Incorrect indentation of attribute \'model\' beginning at L6:C2. Expected \'model\' to be at L6:C3.  attribute-indentation\n  7:2  error  Incorrect indentation of attribute \'isSaved\' beginning at L7:C2. Expected \'isSaved\' to be at L7:C3.  attribute-indentation\n  8:2  error  Incorrect indentation of attribute \'onCancelar\' beginning at L8:C2. Expected \'onCancelar\' to be at L8:C3.  attribute-indentation\n  2:1  error  Incorrect indentation for `<h1>` beginning at L2:C1. Expected `<h1>` to be at an indentation of 2 but was found at 1.  block-indentation\n  3:1  error  Incorrect indentation for `<h4>` beginning at L3:C1. Expected `<h4>` to be at an indentation of 2 but was found at 1.  block-indentation\n  4:1  error  Incorrect indentation for `{{kid-general-data}}` beginning at L4:C1. Expected `{{kid-general-data}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  7:18  error  you must use double quotes in templates  quotes\n  8:21  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/nuevo/documentos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/expedientes/nuevo/documentos.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/expedientes/nuevo/documentos.hbs\n  4:16  error  Incorrect indentation of attribute \'model\' beginning at L4:C16. Expected \'model\' to be at L5:C2.  attribute-indentation\n  5:4  error  Incorrect indentation of attribute \'onGuardado\' beginning at L5:C4. Expected \'onGuardado\' to be at L6:C2.  attribute-indentation\n  5:40  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{kid-documents}}\' beginning at L5:C40. Expected \'{{kid-documents}}\' to be at L7:C0.  attribute-indentation\n  2:0  error  Incorrect indentation for `<h1>` beginning at L2:C0. Expected `<h1>` to be at an indentation of 2 but was found at 0.  block-indentation\n  4:0  error  Incorrect indentation for `{{kid-documents}}` beginning at L4:C0. Expected `{{kid-documents}}` to be at an indentation of 2 but was found at 0.  block-indentation\n  5:23  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/ver.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/administrador/expedientes/ver.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/ver/cuestionarios.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/expedientes/ver/cuestionarios.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/expedientes/ver/cuestionarios.hbs\n  3:9  error  Incorrect indentation of htmlAttribute \'class\' beginning at L3:C9. Expected \'class\' to be at L4:C6.  attribute-indentation\n  3:28  error  Incorrect indentation of htmlAttribute \'style\' beginning at L3:C28. Expected \'style\' to be at L5:C6.  attribute-indentation\n  3:93  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L3:C93. Expected \'<div>\' to be at L6:C4.  attribute-indentation\n  3:123  error  Incorrect indentation of close tag \'</div>\' for element \'<div>\' beginning at L3:C123. Expected \'</div>\' to be at L3:C4.  attribute-indentation\n  21:9  error  Incorrect indentation of htmlAttribute \'class\' beginning at L21:C9. Expected \'class\' to be at L22:C6.  attribute-indentation\n  21:28  error  Incorrect indentation of htmlAttribute \'style\' beginning at L21:C28. Expected \'style\' to be at L23:C6.  attribute-indentation\n  21:93  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L21:C93. Expected \'<div>\' to be at L24:C4.  attribute-indentation\n  21:104  error  Incorrect indentation of close tag \'</div>\' for element \'<div>\' beginning at L21:C104. Expected \'</div>\' to be at L21:C4.  attribute-indentation\n  27:15  error  Incorrect indentation of attribute \'type\' beginning at L27:C15. Expected \'type\' to be at L28:C9.  attribute-indentation\n  27:27  error  Incorrect indentation of attribute \'placeholder\' beginning at L27:C27. Expected \'placeholder\' to be at L29:C9.  attribute-indentation\n  27:54  error  Incorrect indentation of attribute \'class\' beginning at L27:C54. Expected \'class\' to be at L30:C9.  attribute-indentation\n  27:71  error  Incorrect indentation of attribute \'required\' beginning at L27:C71. Expected \'required\' to be at L31:C9.  attribute-indentation\n  27:85  error  Incorrect indentation of attribute \'value\' beginning at L27:C85. Expected \'value\' to be at L32:C9.  attribute-indentation\n  28:6  error  Incorrect indentation of attribute \'disabled\' beginning at L28:C6. Expected \'disabled\' to be at L33:C9.  attribute-indentation\n  28:19  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L28:C19. Expected \'{{input}}\' to be at L34:C7.  attribute-indentation\n  52:19  error  Incorrect indentation of htmlAttribute \'name\' beginning at L52:C19. Expected \'name\' to be at L53:C14.  attribute-indentation\n  52:42  error  Incorrect indentation of htmlAttribute \'type\' beginning at L52:C42. Expected \'type\' to be at L54:C14.  attribute-indentation\n  52:55  error  Incorrect indentation of htmlAttribute \'checked\' beginning at L52:C55. Expected \'checked\' to be at L55:C14.  attribute-indentation\n  52:85  error  Incorrect indentation of htmlAttribute \'value\' beginning at L52:C85. Expected \'value\' to be at L53:C14.  attribute-indentation\n  53:14  error  Incorrect indentation of htmlAttribute \'disabled\' beginning at L53:C14. Expected \'disabled\' to be at L54:C14.  attribute-indentation\n  53:30  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L53:C30. Expected \'<input>\' to be at L55:C12.  attribute-indentation\n  24:6  error  Incorrect indentation for `{{#if}}` beginning at L24:C6. Expected `{{#if}}` to be at an indentation of 8 but was found at 6.  block-indentation\n  33:6  error  Incorrect indentation for `{{#if}}` beginning at L33:C6. Expected `{{#if}}` to be at an indentation of 8 but was found at 6.  block-indentation\n  46:6  error  Incorrect indentation for `{{#if}}` beginning at L46:C6. Expected `{{#if}}` to be at an indentation of 8 but was found at 6.  block-indentation\n  25:6  error  Incorrect indentation for `<p>` beginning at L25:C6. Expected `<p>` to be at an indentation of 8 but was found at 6.  block-indentation\n  26:7  error  Incorrect indentation for `{{#each}}` beginning at L26:C7. Expected `{{#each}}` to be at an indentation of 8 but was found at 7.  block-indentation\n  29:17  error  Incorrect indentation for `each` beginning at L26:C7. Expected `{{/each}}` ending at L29:C17 to be at an indentation of 7 but was found at 8.  block-indentation\n  27:7  error  Incorrect indentation for `{{input}}` beginning at L27:C7. Expected `{{input}}` to be at an indentation of 9 but was found at 7.  block-indentation\n  34:6  error  Incorrect indentation for `<p>` beginning at L34:C6. Expected `<p>` to be at an indentation of 8 but was found at 6.  block-indentation\n  35:6  error  Incorrect indentation for `<div>` beginning at L35:C6. Expected `<div>` to be at an indentation of 8 but was found at 6.  block-indentation\n  37:8  error  Incorrect indentation for `<div>` beginning at L37:C8. Expected `<div>` to be at an indentation of 10 but was found at 8.  block-indentation\n  47:6  error  Incorrect indentation for `<p>` beginning at L47:C6. Expected `<p>` to be at an indentation of 8 but was found at 6.  block-indentation\n  48:6  error  Incorrect indentation for `<div>` beginning at L48:C6. Expected `<div>` to be at an indentation of 8 but was found at 6.  block-indentation\n  50:8  error  Incorrect indentation for `<div>` beginning at L50:C8. Expected `<div>` to be at an indentation of 10 but was found at 8.  block-indentation\n  3:28  error  elements cannot have inline styles  no-inline-styles\n  5:10  error  elements cannot have inline styles  no-inline-styles\n  6:9  error  elements cannot have inline styles  no-inline-styles\n  8:10  error  elements cannot have inline styles  no-inline-styles\n  9:9  error  elements cannot have inline styles  no-inline-styles\n  21:28  error  elements cannot have inline styles  no-inline-styles\n  52:24  error  Unnecessary string concatenation. Use {{pregunta.id}} instead of "{{pregunta.id}}".  no-unnecessary-concat\n  52:91  error  Unnecessary string concatenation. Use {{respuesta.respuesta}} instead of "{{respuesta.respuesta}}".  no-unnecessary-concat\n  5:70  error  you must use double quotes in templates  quotes\n  8:72  error  you must use double quotes in templates  quotes\n  24:30  error  you must use double quotes in templates  quotes\n  33:30  error  you must use double quotes in templates  quotes\n  46:30  error  you must use double quotes in templates  quotes\n  52:12  error  Self-closing a void element is redundant  self-closing-void-elements\n');
  });

  QUnit.test('cifunhi/templates/administrador/expedientes/ver/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/expedientes/ver/index.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/expedientes/ver/index.hbs\n  8:9  error  Incorrect indentation of htmlAttribute \'class\' beginning at L8:C9. Expected \'class\' to be at L9:C6.  attribute-indentation\n  8:28  error  Incorrect indentation of htmlAttribute \'style\' beginning at L8:C28. Expected \'style\' to be at L10:C6.  attribute-indentation\n  8:93  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L8:C93. Expected \'<div>\' to be at L11:C4.  attribute-indentation\n  8:107  error  Incorrect indentation of close tag \'</div>\' for element \'<div>\' beginning at L8:C107. Expected \'</div>\' to be at L8:C4.  attribute-indentation\n  22:19  error  Incorrect indentation of positional param \'cuestionario.fechaRespondido\' beginning at L22:C19. Expected \'cuestionario.fechaRespondido\' to be at L23:C12.  attribute-indentation\n  22:48  error  Incorrect indentation of positional param \'moment-format\' beginning at L22:C48. Expected \'moment-format\' to be at L24:C12.  attribute-indentation\n  23:12  error  Incorrect indentation of positional param \'Pendiente\' beginning at L23:C12. Expected \'Pendiente\' to be at L25:C12.  attribute-indentation\n  23:23  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{if}}\' beginning at L23:C23. Expected \'{{if}}\' to be at L26:C10.  attribute-indentation\n  11:6  error  Incorrect indentation for `<table>` beginning at L11:C6. Expected `<table>` to be at an indentation of 8 but was found at 6.  block-indentation\n  39:7  error  Incorrect indentation for `<h4>` beginning at L39:C7. Expected `<h4>` to be at an indentation of 8 but was found at 7.  block-indentation\n  20:8  error  Incorrect indentation for `<tr>` beginning at L20:C8. Expected `<tr>` to be at an indentation of 10 but was found at 8.  block-indentation\n  23:30  error  Incorrect indentation for `td` beginning at L22:C10. Expected `</td>` ending at L23:C30 to be at an indentation of 10 but was found at 25.  block-indentation\n  22:14  error  Incorrect indentation for `{{if}}` beginning at L22:C14. Expected `{{if}}` to be at an indentation of 12 but was found at 14.  block-indentation\n  28:12  error  Incorrect indentation for `<button>` beginning at L28:C12. Expected `<button>` to be at an indentation of 14 but was found at 12.  block-indentation\n  30:0  error  Incorrect indentation for `            Pendiente\n` beginning at L30:C0. Expected `            Pendiente\n` to be at an indentation of 14 but was found at 12.  block-indentation\n  8:28  error  elements cannot have inline styles  no-inline-styles\n  21:63  error  you must use double quotes in templates  quotes\n  22:98  error  you must use double quotes in templates  quotes\n  23:12  error  you must use double quotes in templates  quotes\n  25:48  error  you must use double quotes in templates  quotes\n  25:61  error  you must use double quotes in templates  quotes\n  28:29  error  you must use double quotes in templates  quotes\n  11:6  error  Tables must have a table group (thead, tbody or tfoot).  table-groups\n');
  });

  QUnit.test('cifunhi/templates/administrador/fotos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/fotos.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/fotos.hbs\n  30:12  error  Incorrect indentation of htmlAttribute \'type\' beginning at L30:C12. Expected \'type\' to be at L31:C6.  attribute-indentation\n  30:26  error  Incorrect indentation of htmlAttribute \'class\' beginning at L30:C26. Expected \'class\' to be at L32:C6.  attribute-indentation\n  30:83  error  Incorrect indentation of element modifier \'action\' beginning at L30:C83. Expected \'action\' to be at L33:C6.  attribute-indentation\n  30:104  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L30:C104. Expected \'<button>\' to be at L34:C4.  attribute-indentation\n  30:154  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L30:C154. Expected \'</button>\' to be at L30:C4.  attribute-indentation\n  44:20  error  Incorrect indentation of htmlAttribute \'class\' beginning at L44:C20. Expected \'class\' to be at L45:C14.  attribute-indentation\n  44:82  error  Incorrect indentation of element modifier \'action\' beginning at L44:C82. Expected \'action\' to be at L46:C14.  attribute-indentation\n  44:118  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L44:C118. Expected \'<button>\' to be at L47:C12.  attribute-indentation\n  44:155  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L44:C155. Expected \'</button>\' to be at L44:C12.  attribute-indentation\n  1:7  error  Incorrect indentation for `\n\t.card{\n\t\theight: 400px;\n\t}\n\t.card-image{\n\t\theight: 280px;\n\t}\n\t.card .card-image img{\n\t\theight: 280px;\n\t}\n` beginning at L1:C7. Expected `\n\t.card{\n\t\theight: 400px;\n\t}\n\t.card-image{\n\t\theight: 280px;\n\t}\n\t.card .card-image img{\n\t\theight: 280px;\n\t}\n` to be at an indentation of 2 but was found at 1.  block-indentation\n  13:1  error  Incorrect indentation for `<div>` beginning at L13:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  16:1  error  Incorrect indentation for `{{#unless}}` beginning at L16:C1. Expected `{{#unless}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  14:2  error  Incorrect indentation for `<h3>` beginning at L14:C2. Expected `<h3>` to be at an indentation of 3 but was found at 2.  block-indentation\n  17:1  error  Incorrect indentation for `<div>` beginning at L17:C1. Expected `<div>` to be at an indentation of 3 but was found at 1.  block-indentation\n  24:1  error  Incorrect indentation for `<div>` beginning at L24:C1. Expected `<div>` to be at an indentation of 3 but was found at 1.  block-indentation\n  25:2  error  Incorrect indentation for `<div>` beginning at L25:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  18:2  error  Incorrect indentation for `<button>` beginning at L18:C2. Expected `<button>` to be at an indentation of 3 but was found at 2.  block-indentation\n  20:14  error  Incorrect indentation for `button` beginning at L18:C2. Expected `</button>` ending at L20:C14 to be at an indentation of 2 but was found at 5.  block-indentation\n  19:6  error  Incorrect indentation for `<i>` beginning at L19:C6. Expected `<i>` to be at an indentation of 4 but was found at 6.  block-indentation\n  26:3  error  Incorrect indentation for `<form>` beginning at L26:C3. Expected `<form>` to be at an indentation of 4 but was found at 3.  block-indentation\n  27:4  error  Incorrect indentation for `<p>` beginning at L27:C4. Expected `<p>` to be at an indentation of 5 but was found at 4.  block-indentation\n  28:4  error  Incorrect indentation for `<p>` beginning at L28:C4. Expected `<p>` to be at an indentation of 5 but was found at 4.  block-indentation\n  29:4  error  Incorrect indentation for `<p>` beginning at L29:C4. Expected `<p>` to be at an indentation of 5 but was found at 4.  block-indentation\n  30:4  error  Incorrect indentation for `<button>` beginning at L30:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  31:4  error  Incorrect indentation for `<button>` beginning at L31:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  38:1  error  Incorrect indentation for `<div>` beginning at L38:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  55:8  error  Incorrect indentation for `div` beginning at L38:C1. Expected `</div>` ending at L55:C8 to be at an indentation of 1 but was found at 2.  block-indentation\n  39:2  error  Incorrect indentation for `{{#each}}` beginning at L39:C2. Expected `{{#each}}` to be at an indentation of 3 but was found at 2.  block-indentation\n  52:5  error  Incorrect indentation for inverse block of `{{#each}}` beginning at L39:C2. Expected `{{else}}` starting at L52:C5 to be at an indentation of 2 but was found at 5.  block-indentation\n  54:14  error  Incorrect indentation for `each` beginning at L39:C2. Expected `{{/each}}` ending at L54:C14 to be at an indentation of 2 but was found at 5.  block-indentation\n  40:6  error  Incorrect indentation for `<div>` beginning at L40:C6. Expected `<div>` to be at an indentation of 4 but was found at 6.  block-indentation\n  53:6  error  Incorrect indentation for `<h3>` beginning at L53:C6. Expected `<h3>` to be at an indentation of 4 but was found at 6.  block-indentation\n  47:11  error  Incorrect indentation for `<h5>` beginning at L47:C11. Expected `<h5>` to be at an indentation of 12 but was found at 11.  block-indentation\n  43:12  error  img tags must have an alt attribute  img-alt-attributes\n  18:56  error  you must use double quotes in templates  quotes\n  26:18  error  you must use double quotes in templates  quotes\n  30:92  error  you must use double quotes in templates  quotes\n  44:91  error  you must use double quotes in templates  quotes\n  23:1  error  Using an {{else}} block with {{unless}} should be avoided.  simple-unless\n');
  });

  QUnit.test('cifunhi/templates/administrador/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/administrador/index.hbs should pass TemplateLint.\n\ncifunhi/templates/administrador/index.hbs\n  2:5  error  Incorrect indentation for `<div>` beginning at L2:C5. Expected `<div>` to be at an indentation of 2 but was found at 5.  block-indentation\n  3:4  error  Incorrect indentation for `{{#link-to}}` beginning at L3:C4. Expected `{{#link-to}}` to be at an indentation of 2 but was found at 4.  block-indentation\n  14:4  error  Incorrect indentation for `{{#link-to}}` beginning at L14:C4. Expected `{{#link-to}}` to be at an indentation of 2 but was found at 4.  block-indentation\n  25:5  error  Incorrect indentation for `{{#link-to}}` beginning at L25:C5. Expected `{{#link-to}}` to be at an indentation of 2 but was found at 5.  block-indentation\n  34:5  error  Incorrect indentation for `<div>` beginning at L34:C5. Expected `<div>` to be at an indentation of 2 but was found at 5.  block-indentation\n  5:8  error  Incorrect indentation for `<div>` beginning at L5:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  6:12  error  Incorrect indentation for `<div>` beginning at L6:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  7:16  error  Incorrect indentation for `<img>` beginning at L7:C16. Expected `<img>` to be at an indentation of 14 but was found at 16.  block-indentation\n  8:16  error  Incorrect indentation for `<span>` beginning at L8:C16. Expected `<span>` to be at an indentation of 14 but was found at 16.  block-indentation\n  16:8  error  Incorrect indentation for `<div>` beginning at L16:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  17:12  error  Incorrect indentation for `<div>` beginning at L17:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  18:16  error  Incorrect indentation for `<img>` beginning at L18:C16. Expected `<img>` to be at an indentation of 14 but was found at 16.  block-indentation\n  19:16  error  Incorrect indentation for `<span>` beginning at L19:C16. Expected `<span>` to be at an indentation of 14 but was found at 16.  block-indentation\n  33:16  error  Incorrect indentation for `link-to` beginning at L25:C5. Expected `{{/link-to}}` ending at L33:C16 to be at an indentation of 5 but was found at 4.  block-indentation\n  27:8  error  Incorrect indentation for `<div>` beginning at L27:C8. Expected `<div>` to be at an indentation of 7 but was found at 8.  block-indentation\n  28:12  error  Incorrect indentation for `<div>` beginning at L28:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  29:16  error  Incorrect indentation for `<img>` beginning at L29:C16. Expected `<img>` to be at an indentation of 14 but was found at 16.  block-indentation\n  30:16  error  Incorrect indentation for `<span>` beginning at L30:C16. Expected `<span>` to be at an indentation of 14 but was found at 16.  block-indentation\n  7:16  error  img tags must have an alt attribute  img-alt-attributes\n  18:16  error  img tags must have an alt attribute  img-alt-attributes\n  29:16  error  img tags must have an alt attribute  img-alt-attributes\n  3:15  error  you must use double quotes in templates  quotes\n  14:15  error  you must use double quotes in templates  quotes\n  25:16  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/application.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/application.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/cancel.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/cancel.hbs should pass TemplateLint.\n\ncifunhi/templates/cancel.hbs\n  3:17  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/components/kid-anexos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/components/kid-anexos.hbs should pass TemplateLint.\n\ncifunhi/templates/components/kid-anexos.hbs\n  2:6  error  Incorrect indentation of htmlAttribute \'class\' beginning at L2:C6. Expected \'class\' to be at L3:C3.  attribute-indentation\n  2:25  error  Incorrect indentation of htmlAttribute \'style\' beginning at L2:C25. Expected \'style\' to be at L4:C3.  attribute-indentation\n  2:90  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L2:C90. Expected \'<div>\' to be at L5:C1.  attribute-indentation\n  5:5  error  Incorrect indentation of htmlAttribute \'class\' beginning at L5:C5. Expected \'class\' to be at L6:C4.  attribute-indentation\n  5:48  error  Incorrect indentation of htmlAttribute \'data-position\' beginning at L5:C48. Expected \'data-position\' to be at L7:C4.  attribute-indentation\n  5:69  error  Incorrect indentation of htmlAttribute \'data-tooltip\' beginning at L5:C69. Expected \'data-tooltip\' to be at L8:C4.  attribute-indentation\n  6:3  error  Incorrect indentation of element modifier \'action\' beginning at L6:C3. Expected \'action\' to be at L9:C4.  attribute-indentation\n  6:35  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L6:C35. Expected \'<a>\' to be at L10:C2.  attribute-indentation\n  58:6  error  Incorrect indentation of htmlAttribute \'class\' beginning at L58:C6. Expected \'class\' to be at L59:C3.  attribute-indentation\n  58:25  error  Incorrect indentation of htmlAttribute \'style\' beginning at L58:C25. Expected \'style\' to be at L60:C3.  attribute-indentation\n  58:90  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L58:C90. Expected \'<div>\' to be at L61:C1.  attribute-indentation\n  58:102  error  Incorrect indentation of close tag \'</div>\' for element \'<div>\' beginning at L58:C102. Expected \'</div>\' to be at L58:C1.  attribute-indentation\n  2:1  error  Incorrect indentation for `<div>` beginning at L2:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  13:1  error  Incorrect indentation for `<div>` beginning at L13:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  2:91  error  Incorrect indentation for `Anexos\n` beginning at L2:C91. Expected `Anexos\n` to be at an indentation of 3 but was found at 91.  block-indentation\n  3:2  error  Incorrect indentation for `{{#unless}}` beginning at L3:C2. Expected `{{#unless}}` to be at an indentation of 3 but was found at 2.  block-indentation\n  4:2  error  Incorrect indentation for `<button>` beginning at L4:C2. Expected `<button>` to be at an indentation of 4 but was found at 2.  block-indentation\n  5:2  error  Incorrect indentation for `<a>` beginning at L5:C2. Expected `<a>` to be at an indentation of 4 but was found at 2.  block-indentation\n  10:3  error  Incorrect indentation for `<button>` beginning at L10:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  7:3  error  Incorrect indentation for `<i>` beginning at L7:C3. Expected `<i>` to be at an indentation of 4 but was found at 3.  block-indentation\n  14:2  error  Incorrect indentation for `{{#if}}` beginning at L14:C2. Expected `{{#if}}` to be at an indentation of 3 but was found at 2.  block-indentation\n  15:2  error  Incorrect indentation for `<table>` beginning at L15:C2. Expected `<table>` to be at an indentation of 4 but was found at 2.  block-indentation\n  40:2  error  Incorrect indentation for `<h3>` beginning at L40:C2. Expected `<h3>` to be at an indentation of 4 but was found at 2.  block-indentation\n  16:3  error  Incorrect indentation for `<tr>` beginning at L16:C3. Expected `<tr>` to be at an indentation of 4 but was found at 3.  block-indentation\n  23:3  error  Incorrect indentation for `{{#each}}` beginning at L23:C3. Expected `{{#each}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  17:4  error  Incorrect indentation for `<th>` beginning at L17:C4. Expected `<th>` to be at an indentation of 5 but was found at 4.  block-indentation\n  18:4  error  Incorrect indentation for `<th>` beginning at L18:C4. Expected `<th>` to be at an indentation of 5 but was found at 4.  block-indentation\n  19:4  error  Incorrect indentation for `<th>` beginning at L19:C4. Expected `<th>` to be at an indentation of 5 but was found at 4.  block-indentation\n  24:3  error  Incorrect indentation for `<tr>` beginning at L24:C3. Expected `<tr>` to be at an indentation of 5 but was found at 3.  block-indentation\n  25:4  error  Incorrect indentation for `<td>` beginning at L25:C4. Expected `<td>` to be at an indentation of 5 but was found at 4.  block-indentation\n  26:4  error  Incorrect indentation for `<td>` beginning at L26:C4. Expected `<td>` to be at an indentation of 5 but was found at 4.  block-indentation\n  27:4  error  Incorrect indentation for `<td>` beginning at L27:C4. Expected `<td>` to be at an indentation of 5 but was found at 4.  block-indentation\n  28:5  error  Incorrect indentation for `<a>` beginning at L28:C5. Expected `<a>` to be at an indentation of 6 but was found at 5.  block-indentation\n  29:5  error  Incorrect indentation for `{{#unless}}` beginning at L29:C5. Expected `{{#unless}}` to be at an indentation of 6 but was found at 5.  block-indentation\n  30:6  error  Incorrect indentation for `<button>` beginning at L30:C6. Expected `<button>` to be at an indentation of 7 but was found at 6.  block-indentation\n  57:0  error  Incorrect indentation for `<div>` beginning at L57:C0. Expected `<div>` to be at an indentation of 2 but was found at 0.  block-indentation\n  58:1  error  Incorrect indentation for `<div>` beginning at L58:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  59:1  error  Incorrect indentation for `<div>` beginning at L59:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  60:2  error  Incorrect indentation for `<p>` beginning at L60:C2. Expected `<p>` to be at an indentation of 3 but was found at 2.  block-indentation\n  61:2  error  Incorrect indentation for `<p>` beginning at L61:C2. Expected `<p>` to be at an indentation of 3 but was found at 2.  block-indentation\n  62:2  error  Incorrect indentation for `<br>` beginning at L62:C2. Expected `<br>` to be at an indentation of 3 but was found at 2.  block-indentation\n  63:2  error  Incorrect indentation for `<p>` beginning at L63:C2. Expected `<p>` to be at an indentation of 3 but was found at 2.  block-indentation\n  64:2  error  Incorrect indentation for `<br>` beginning at L64:C2. Expected `<br>` to be at an indentation of 3 but was found at 2.  block-indentation\n  65:2  error  Incorrect indentation for `<button>` beginning at L65:C2. Expected `<button>` to be at an indentation of 3 but was found at 2.  block-indentation\n  66:2  error  Incorrect indentation for `<button>` beginning at L66:C2. Expected `<button>` to be at an indentation of 3 but was found at 2.  block-indentation\n  2:25  error  elements cannot have inline styles  no-inline-styles\n  58:25  error  elements cannot have inline styles  no-inline-styles\n  6:3  error  Interaction added to non-interactive element  no-invalid-interactive\n  28:13  error  Unnecessary string concatenation. Use {{anexo.archivo}} instead of "{{anexo.archivo}}".  no-unnecessary-concat\n  4:19  error  you must use double quotes in templates  quotes\n  4:34  error  you must use double quotes in templates  quotes\n  6:12  error  you must use double quotes in templates  quotes\n  10:20  error  you must use double quotes in templates  quotes\n  10:29  error  you must use double quotes in templates  quotes\n  30:23  error  you must use double quotes in templates  quotes\n  65:31  error  you must use double quotes in templates  quotes\n  66:35  error  you must use double quotes in templates  quotes\n  9:2  error  Using an {{else}} block with {{unless}} should be avoided.  simple-unless\n  29:15  error  Using {{unless}} in combination with other helpers should be avoided. MaxHelpers: 0  simple-unless\n  29:28  error  Using {{unless}} in combination with other helpers should be avoided. MaxHelpers: 0  simple-unless\n  15:2  error  Tables must have a table group (thead, tbody or tfoot).  table-groups\n');
  });

  QUnit.test('cifunhi/templates/components/kid-documents.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/components/kid-documents.hbs should pass TemplateLint.\n\ncifunhi/templates/components/kid-documents.hbs\n  3:6  error  Incorrect indentation of htmlAttribute \'class\' beginning at L3:C6. Expected \'class\' to be at L4:C3.  attribute-indentation\n  3:25  error  Incorrect indentation of htmlAttribute \'style\' beginning at L3:C25. Expected \'style\' to be at L5:C3.  attribute-indentation\n  3:90  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L3:C90. Expected \'<div>\' to be at L6:C1.  attribute-indentation\n  24:8  error  Incorrect indentation of htmlAttribute \'class\' beginning at L24:C8. Expected \'class\' to be at L25:C5.  attribute-indentation\n  24:27  error  Incorrect indentation of htmlAttribute \'style\' beginning at L24:C27. Expected \'style\' to be at L26:C5.  attribute-indentation\n  24:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L24:C92. Expected \'<div>\' to be at L27:C3.  attribute-indentation\n  32:13  error  Incorrect indentation of htmlAttribute \'id\' beginning at L32:C13. Expected \'id\' to be at L33:C8.  attribute-indentation\n  32:39  error  Incorrect indentation of htmlAttribute \'type\' beginning at L32:C39. Expected \'type\' to be at L34:C8.  attribute-indentation\n  32:51  error  Incorrect indentation of htmlAttribute \'onchange\' beginning at L32:C51. Expected \'onchange\' to be at L35:C8.  attribute-indentation\n  32:85  error  Incorrect indentation of htmlAttribute \'accept\' beginning at L32:C85. Expected \'accept\' to be at L33:C8.  attribute-indentation\n  32:107  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L32:C107. Expected \'<input>\' to be at L34:C6.  attribute-indentation\n  42:10  error  Incorrect indentation of htmlAttribute \'id\' beginning at L42:C10. Expected \'id\' to be at L43:C5.  attribute-indentation\n  42:20  error  Incorrect indentation of htmlAttribute \'type\' beginning at L42:C20. Expected \'type\' to be at L44:C5.  attribute-indentation\n  42:32  error  Incorrect indentation of htmlAttribute \'onchange\' beginning at L42:C32. Expected \'onchange\' to be at L45:C5.  attribute-indentation\n  42:66  error  Incorrect indentation of htmlAttribute \'accept\' beginning at L42:C66. Expected \'accept\' to be at L43:C5.  attribute-indentation\n  42:88  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L42:C88. Expected \'<input>\' to be at L44:C3.  attribute-indentation\n  44:10  error  Incorrect indentation of htmlAttribute \'id\' beginning at L44:C10. Expected \'id\' to be at L45:C5.  attribute-indentation\n  44:20  error  Incorrect indentation of htmlAttribute \'type\' beginning at L44:C20. Expected \'type\' to be at L46:C5.  attribute-indentation\n  44:32  error  Incorrect indentation of htmlAttribute \'onchange\' beginning at L44:C32. Expected \'onchange\' to be at L47:C5.  attribute-indentation\n  44:66  error  Incorrect indentation of htmlAttribute \'accept\' beginning at L44:C66. Expected \'accept\' to be at L45:C5.  attribute-indentation\n  44:88  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L44:C88. Expected \'<input>\' to be at L46:C3.  attribute-indentation\n  46:10  error  Incorrect indentation of htmlAttribute \'id\' beginning at L46:C10. Expected \'id\' to be at L47:C5.  attribute-indentation\n  46:19  error  Incorrect indentation of htmlAttribute \'type\' beginning at L46:C19. Expected \'type\' to be at L48:C5.  attribute-indentation\n  46:31  error  Incorrect indentation of htmlAttribute \'onchange\' beginning at L46:C31. Expected \'onchange\' to be at L49:C5.  attribute-indentation\n  46:64  error  Incorrect indentation of htmlAttribute \'accept\' beginning at L46:C64. Expected \'accept\' to be at L47:C5.  attribute-indentation\n  46:86  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L46:C86. Expected \'<input>\' to be at L48:C3.  attribute-indentation\n  48:10  error  Incorrect indentation of htmlAttribute \'id\' beginning at L48:C10. Expected \'id\' to be at L49:C5.  attribute-indentation\n  48:27  error  Incorrect indentation of htmlAttribute \'type\' beginning at L48:C27. Expected \'type\' to be at L50:C5.  attribute-indentation\n  48:39  error  Incorrect indentation of htmlAttribute \'onchange\' beginning at L48:C39. Expected \'onchange\' to be at L51:C5.  attribute-indentation\n  48:80  error  Incorrect indentation of htmlAttribute \'accept\' beginning at L48:C80. Expected \'accept\' to be at L49:C5.  attribute-indentation\n  48:102  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L48:C102. Expected \'<input>\' to be at L50:C3.  attribute-indentation\n  50:10  error  Incorrect indentation of htmlAttribute \'id\' beginning at L50:C10. Expected \'id\' to be at L51:C5.  attribute-indentation\n  50:25  error  Incorrect indentation of htmlAttribute \'type\' beginning at L50:C25. Expected \'type\' to be at L52:C5.  attribute-indentation\n  50:37  error  Incorrect indentation of htmlAttribute \'onchange\' beginning at L50:C37. Expected \'onchange\' to be at L53:C5.  attribute-indentation\n  50:76  error  Incorrect indentation of htmlAttribute \'accept\' beginning at L50:C76. Expected \'accept\' to be at L51:C5.  attribute-indentation\n  50:98  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L50:C98. Expected \'<input>\' to be at L52:C3.  attribute-indentation\n  2:0  error  Incorrect indentation for `<div>` beginning at L2:C0. Expected `<div>` to be at an indentation of 2 but was found at 0.  block-indentation\n  22:1  error  Incorrect indentation for `{{#if}}` beginning at L22:C1. Expected `{{#if}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  3:1  error  Incorrect indentation for `<div>` beginning at L3:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  6:1  error  Incorrect indentation for `<div>` beginning at L6:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  3:91  error  Incorrect indentation for `Archivos\n` beginning at L3:C91. Expected `Archivos\n` to be at an indentation of 3 but was found at 91.  block-indentation\n  4:2  error  Incorrect indentation for `{{! <button {{action \'editar\' \'editarDocs\'}} class="right">Editar</button> }}` beginning at L4:C2. Expected `{{! <button {{action \'editar\' \'editarDocs\'}} class="right">Editar</button> }}` to be at an indentation of 3 but was found at 2.  block-indentation\n  7:2  error  Incorrect indentation for `<ul>` beginning at L7:C2. Expected `<ul>` to be at an indentation of 3 but was found at 2.  block-indentation\n  8:3  error  Incorrect indentation for `{{#each}}` beginning at L8:C3. Expected `{{#each}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  9:3  error  Incorrect indentation for `<a>` beginning at L9:C3. Expected `<a>` to be at an indentation of 5 but was found at 3.  block-indentation\n  10:21  error  Incorrect indentation for `a` beginning at L9:C3. Expected `</a>` ending at L10:C21 to be at an indentation of 3 but was found at 17.  block-indentation\n  9:79  error  Incorrect indentation for `{{or}}` beginning at L9:C79. Expected `{{or}}` to be at an indentation of 5 but was found at 79.  block-indentation\n  10:17  error  Incorrect indentation for `p` beginning at L9:C110. Expected `</p>` ending at L10:C17 to be at an indentation of 110 but was found at 13.  block-indentation\n  9:127  error  Incorrect indentation for `Click\n\t\t\t\t\tpara ver` beginning at L9:C127. Expected `Click\n\t\t\t\t\tpara ver` to be at an indentation of 112 but was found at 127.  block-indentation\n  23:2  error  Incorrect indentation for `<div>` beginning at L23:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  40:2  error  Incorrect indentation for `<form>` beginning at L40:C2. Expected `<form>` to be at an indentation of 3 but was found at 2.  block-indentation\n  24:3  error  Incorrect indentation for `<div>` beginning at L24:C3. Expected `<div>` to be at an indentation of 4 but was found at 3.  block-indentation\n  27:3  error  Incorrect indentation for `<div>` beginning at L27:C3. Expected `<div>` to be at an indentation of 4 but was found at 3.  block-indentation\n  24:93  error  Incorrect indentation for `Archivos\n\t\t\t\t` beginning at L24:C93. Expected `Archivos\n\t\t\t\t` to be at an indentation of 5 but was found at 93.  block-indentation\n  25:4  error  Incorrect indentation for `<button>` beginning at L25:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  28:4  error  Incorrect indentation for `<form>` beginning at L28:C4. Expected `<form>` to be at an indentation of 5 but was found at 4.  block-indentation\n  29:5  error  Incorrect indentation for `<ul>` beginning at L29:C5. Expected `<ul>` to be at an indentation of 6 but was found at 5.  block-indentation\n  30:6  error  Incorrect indentation for `{{#each}}` beginning at L30:C6. Expected `{{#each}}` to be at an indentation of 7 but was found at 6.  block-indentation\n  31:6  error  Incorrect indentation for `<a>` beginning at L31:C6. Expected `<a>` to be at an indentation of 8 but was found at 6.  block-indentation\n  32:6  error  Incorrect indentation for `<input>` beginning at L32:C6. Expected `<input>` to be at an indentation of 8 but was found at 6.  block-indentation\n  41:3  error  Incorrect indentation for `<h5>` beginning at L41:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  42:3  error  Incorrect indentation for `<input>` beginning at L42:C3. Expected `<input>` to be at an indentation of 4 but was found at 3.  block-indentation\n  43:3  error  Incorrect indentation for `<h5>` beginning at L43:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  44:3  error  Incorrect indentation for `<input>` beginning at L44:C3. Expected `<input>` to be at an indentation of 4 but was found at 3.  block-indentation\n  45:3  error  Incorrect indentation for `<h5>` beginning at L45:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  46:3  error  Incorrect indentation for `<input>` beginning at L46:C3. Expected `<input>` to be at an indentation of 4 but was found at 3.  block-indentation\n  47:3  error  Incorrect indentation for `<h5>` beginning at L47:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  48:3  error  Incorrect indentation for `<input>` beginning at L48:C3. Expected `<input>` to be at an indentation of 4 but was found at 3.  block-indentation\n  49:3  error  Incorrect indentation for `<h5>` beginning at L49:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  50:3  error  Incorrect indentation for `<input>` beginning at L50:C3. Expected `<input>` to be at an indentation of 4 but was found at 3.  block-indentation\n  51:3  error  Incorrect indentation for `<br>` beginning at L51:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  52:3  error  Incorrect indentation for `<br>` beginning at L52:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  53:3  error  Incorrect indentation for `<br>` beginning at L53:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  54:3  error  Incorrect indentation for `<button>` beginning at L54:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  55:3  error  Incorrect indentation for `<button>` beginning at L55:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  3:25  error  elements cannot have inline styles  no-inline-styles\n  9:30  error  elements cannot have inline styles  no-inline-styles\n  24:27  error  elements cannot have inline styles  no-inline-styles\n  31:33  error  elements cannot have inline styles  no-inline-styles\n  9:55  error  Unnecessary string concatenation. Use {{documento.archivo}} instead of "{{documento.archivo}}".  no-unnecessary-concat\n  31:58  error  Unnecessary string concatenation. Use {{documento.archivo}} instead of "{{documento.archivo}}".  no-unnecessary-concat\n  32:16  error  Unnecessary string concatenation. Use {{documento.nombre}} instead of "{{documento.nombre}}".  no-unnecessary-concat\n  9:101  error  you must use double quotes in templates  quotes\n  25:21  error  you must use double quotes in templates  quotes\n  25:36  error  you must use double quotes in templates  quotes\n  28:19  error  you must use double quotes in templates  quotes\n  31:104  error  you must use double quotes in templates  quotes\n  32:69  error  you must use double quotes in templates  quotes\n  40:17  error  you must use double quotes in templates  quotes\n  42:50  error  you must use double quotes in templates  quotes\n  44:50  error  you must use double quotes in templates  quotes\n  46:49  error  you must use double quotes in templates  quotes\n  48:57  error  you must use double quotes in templates  quotes\n  50:55  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/components/kid-general-data.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/components/kid-general-data.hbs should pass TemplateLint.\n\ncifunhi/templates/components/kid-general-data.hbs\n  6:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L6:C7. Expected \'class\' to be at L7:C4.  attribute-indentation\n  6:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L6:C26. Expected \'style\' to be at L8:C4.  attribute-indentation\n  6:91  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L6:C91. Expected \'<div>\' to be at L9:C2.  attribute-indentation\n  34:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L34:C7. Expected \'class\' to be at L35:C4.  attribute-indentation\n  34:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L34:C26. Expected \'style\' to be at L36:C4.  attribute-indentation\n  34:91  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L34:C91. Expected \'<div>\' to be at L37:C2.  attribute-indentation\n  71:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L71:C7. Expected \'class\' to be at L72:C4.  attribute-indentation\n  71:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L71:C26. Expected \'style\' to be at L73:C4.  attribute-indentation\n  71:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L71:C92. Expected \'<div>\' to be at L74:C2.  attribute-indentation\n  94:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L94:C7. Expected \'class\' to be at L95:C4.  attribute-indentation\n  94:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L94:C26. Expected \'style\' to be at L96:C4.  attribute-indentation\n  94:91  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L94:C91. Expected \'<div>\' to be at L97:C2.  attribute-indentation\n  117:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L117:C7. Expected \'class\' to be at L118:C4.  attribute-indentation\n  117:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L117:C26. Expected \'style\' to be at L119:C4.  attribute-indentation\n  117:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L117:C92. Expected \'<div>\' to be at L120:C2.  attribute-indentation\n  145:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L145:C7. Expected \'class\' to be at L146:C4.  attribute-indentation\n  145:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L145:C26. Expected \'style\' to be at L147:C4.  attribute-indentation\n  145:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L145:C92. Expected \'<div>\' to be at L148:C2.  attribute-indentation\n  152:6  error  Incorrect indentation of htmlAttribute \'class\' beginning at L152:C6. Expected \'class\' to be at L153:C5.  attribute-indentation\n  152:43  error  Incorrect indentation of element modifier \'action\' beginning at L152:C43. Expected \'action\' to be at L154:C5.  attribute-indentation\n  152:111  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L152:C111. Expected \'<a>\' to be at L155:C3.  attribute-indentation\n  152:118  error  Incorrect indentation of close tag \'</a>\' for element \'<a>\' beginning at L152:C118. Expected \'</a>\' to be at L152:C3.  attribute-indentation\n  160:5  error  Incorrect indentation of block params \'as |colonia|\n\t\t\t}}\' beginning at L160:C5. Expecting the block params to be at L160:C3.  attribute-indentation\n  181:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L181:C7. Expected \'class\' to be at L182:C4.  attribute-indentation\n  181:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L181:C26. Expected \'style\' to be at L183:C4.  attribute-indentation\n  181:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L181:C92. Expected \'<div>\' to be at L184:C2.  attribute-indentation\n  205:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L205:C7. Expected \'class\' to be at L206:C4.  attribute-indentation\n  205:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L205:C26. Expected \'style\' to be at L207:C4.  attribute-indentation\n  205:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L205:C92. Expected \'<div>\' to be at L208:C2.  attribute-indentation\n  212:6  error  Incorrect indentation of htmlAttribute \'class\' beginning at L212:C6. Expected \'class\' to be at L213:C5.  attribute-indentation\n  212:43  error  Incorrect indentation of element modifier \'action\' beginning at L212:C43. Expected \'action\' to be at L214:C5.  attribute-indentation\n  212:111  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L212:C111. Expected \'<a>\' to be at L215:C3.  attribute-indentation\n  212:118  error  Incorrect indentation of close tag \'</a>\' for element \'<a>\' beginning at L212:C118. Expected \'</a>\' to be at L212:C3.  attribute-indentation\n  221:5  error  Incorrect indentation of block params \'as |colonia|\n\t\t\t}}\' beginning at L221:C5. Expecting the block params to be at L221:C3.  attribute-indentation\n  238:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L238:C7. Expected \'class\' to be at L239:C4.  attribute-indentation\n  238:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L238:C26. Expected \'style\' to be at L240:C4.  attribute-indentation\n  238:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L238:C92. Expected \'<div>\' to be at L241:C2.  attribute-indentation\n  261:7  error  Incorrect indentation of htmlAttribute \'class\' beginning at L261:C7. Expected \'class\' to be at L262:C4.  attribute-indentation\n  261:26  error  Incorrect indentation of htmlAttribute \'style\' beginning at L261:C26. Expected \'style\' to be at L263:C4.  attribute-indentation\n  261:92  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L261:C92. Expected \'<div>\' to be at L264:C2.  attribute-indentation\n  5:1  error  Incorrect indentation for `<div>` beginning at L5:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  33:1  error  Incorrect indentation for `<div>` beginning at L33:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  63:1  error  Incorrect indentation for `<div>` beginning at L63:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  6:2  error  Incorrect indentation for `<div>` beginning at L6:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  13:2  error  Incorrect indentation for `<div>` beginning at L13:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  6:92  error  Incorrect indentation for `Datos generales \n\t\t\t \n\t\t\t` beginning at L6:C92. Expected `Datos generales \n\t\t\t \n\t\t\t` to be at an indentation of 4 but was found at 92.  block-indentation\n  8:3  error  Incorrect indentation for `<button>` beginning at L8:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  15:3  error  Incorrect indentation for `<h4>` beginning at L15:C3. Expected `<h4>` to be at an indentation of 4 but was found at 3.  block-indentation\n  16:3  error  Incorrect indentation for `<p>` beginning at L16:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  17:3  error  Incorrect indentation for `<br>` beginning at L17:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  18:3  error  Incorrect indentation for `<h5>` beginning at L18:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  20:3  error  Incorrect indentation for `<p>` beginning at L20:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  21:3  error  Incorrect indentation for `<br>` beginning at L21:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  22:3  error  Incorrect indentation for `{{#if}}` beginning at L22:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  23:3  error  Incorrect indentation for `<p>` beginning at L23:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  25:3  error  Incorrect indentation for `<p>` beginning at L25:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  34:2  error  Incorrect indentation for `<div>` beginning at L34:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  39:2  error  Incorrect indentation for `<div>` beginning at L39:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  34:92  error  Incorrect indentation for `Datos generales \n` beginning at L34:C92. Expected `Datos generales \n` to be at an indentation of 4 but was found at 92.  block-indentation\n  35:3  error  Incorrect indentation for `{{#if}}` beginning at L35:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  36:4  error  Incorrect indentation for `<button>` beginning at L36:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  41:3  error  Incorrect indentation for `<p>` beginning at L41:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  42:3  error  Incorrect indentation for `<p>` beginning at L42:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  43:3  error  Incorrect indentation for `<p>` beginning at L43:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  44:3  error  Incorrect indentation for `{{!IMPORTANTE!!! para guardar una fecha, el HTML lo guarda como string en el formato \'DD/MM/YYYY\', para guardarlo en\n\t\t\tUNIX como tipo number, eso se hace en el controlador }}` beginning at L44:C3. Expected `{{!IMPORTANTE!!! para guardar una fecha, el HTML lo guarda como string en el formato \'DD/MM/YYYY\', para guardarlo en\n\t\t\tUNIX como tipo number, eso se hace en el controlador }}` to be at an indentation of 4 but was found at 3.  block-indentation\n  46:3  error  Incorrect indentation for `<p>` beginning at L46:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  47:3  error  Incorrect indentation for `<p>` beginning at L47:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  53:3  error  Incorrect indentation for `{{#if}}` beginning at L53:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  57:3  error  Incorrect indentation for `{{#if}}` beginning at L57:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  47:6  error  Incorrect indentation for `\xBFTiene hermanos?\n\t\t\t\t` beginning at L47:C6. Expected `\xBFTiene hermanos?\n\t\t\t\t` to be at an indentation of 5 but was found at 6.  block-indentation\n  48:4  error  Incorrect indentation for `<label>` beginning at L48:C4. Expected `<label>` to be at an indentation of 5 but was found at 4.  block-indentation\n  49:5  error  Incorrect indentation for `{{input}}` beginning at L49:C5. Expected `{{input}}` to be at an indentation of 6 but was found at 5.  block-indentation\n  50:5  error  Incorrect indentation for `<span>` beginning at L50:C5. Expected `<span>` to be at an indentation of 6 but was found at 5.  block-indentation\n  54:3  error  Incorrect indentation for `<p>` beginning at L54:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  58:4  error  Incorrect indentation for `<button>` beginning at L58:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  70:1  error  Incorrect indentation for `<div>` beginning at L70:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  93:1  error  Incorrect indentation for `<div>` beginning at L93:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  110:1  error  Incorrect indentation for `<div>` beginning at L110:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  71:2  error  Incorrect indentation for `<div>` beginning at L71:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  74:2  error  Incorrect indentation for `<div>` beginning at L74:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  71:93  error  Incorrect indentation for `Datos de los padres\n\t\t\t` beginning at L71:C93. Expected `Datos de los padres\n\t\t\t` to be at an indentation of 4 but was found at 93.  block-indentation\n  72:3  error  Incorrect indentation for `<button>` beginning at L72:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  75:3  error  Incorrect indentation for `<h5>` beginning at L75:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  76:3  error  Incorrect indentation for `<p>` beginning at L76:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  77:3  error  Incorrect indentation for `<br>` beginning at L77:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  78:3  error  Incorrect indentation for `<h5>` beginning at L78:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  79:3  error  Incorrect indentation for `<p>` beginning at L79:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  80:3  error  Incorrect indentation for `<br>` beginning at L80:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  81:3  error  Incorrect indentation for `<h5>` beginning at L81:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  82:3  error  Incorrect indentation for `<p>` beginning at L82:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  83:3  error  Incorrect indentation for `<br>` beginning at L83:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  84:3  error  Incorrect indentation for `<h5>` beginning at L84:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  85:3  error  Incorrect indentation for `<p>` beginning at L85:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  86:3  error  Incorrect indentation for `<br>` beginning at L86:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  94:2  error  Incorrect indentation for `<div>` beginning at L94:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  99:2  error  Incorrect indentation for `<div>` beginning at L99:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  94:92  error  Incorrect indentation for `Datos de los padres \n` beginning at L94:C92. Expected `Datos de los padres \n` to be at an indentation of 4 but was found at 92.  block-indentation\n  95:3  error  Incorrect indentation for `{{#if}}` beginning at L95:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  96:4  error  Incorrect indentation for `<button>` beginning at L96:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  101:3  error  Incorrect indentation for `<p>` beginning at L101:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  102:3  error  Incorrect indentation for `<p>` beginning at L102:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  103:3  error  Incorrect indentation for `<p>` beginning at L103:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  104:3  error  Incorrect indentation for `<p>` beginning at L104:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  105:3  error  Incorrect indentation for `{{#if}}` beginning at L105:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  106:4  error  Incorrect indentation for `<button>` beginning at L106:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  116:1  error  Incorrect indentation for `<div>` beginning at L116:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  144:1  error  Incorrect indentation for `<div>` beginning at L144:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  176:1  error  Incorrect indentation for `<div>` beginning at L176:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  117:2  error  Incorrect indentation for `<div>` beginning at L117:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  120:2  error  Incorrect indentation for `<div>` beginning at L120:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  117:93  error  Incorrect indentation for `Lugar de nacimiento\n\t\t\t` beginning at L117:C93. Expected `Lugar de nacimiento\n\t\t\t` to be at an indentation of 4 but was found at 93.  block-indentation\n  118:3  error  Incorrect indentation for `<button>` beginning at L118:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  121:3  error  Incorrect indentation for `<h5>` beginning at L121:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  122:3  error  Incorrect indentation for `<p>` beginning at L122:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  123:3  error  Incorrect indentation for `<br>` beginning at L123:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  124:3  error  Incorrect indentation for `<h5>` beginning at L124:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  125:3  error  Incorrect indentation for `<p>` beginning at L125:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  126:3  error  Incorrect indentation for `<br>` beginning at L126:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  127:3  error  Incorrect indentation for `<h5>` beginning at L127:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  128:3  error  Incorrect indentation for `<p>` beginning at L128:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  129:3  error  Incorrect indentation for `<br>` beginning at L129:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  130:3  error  Incorrect indentation for `<h5>` beginning at L130:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  131:3  error  Incorrect indentation for `<p>` beginning at L131:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  132:3  error  Incorrect indentation for `<br>` beginning at L132:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  133:3  error  Incorrect indentation for `<h5>` beginning at L133:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  134:3  error  Incorrect indentation for `<p>` beginning at L134:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  135:3  error  Incorrect indentation for `<br>` beginning at L135:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  136:3  error  Incorrect indentation for `<h5>` beginning at L136:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  137:3  error  Incorrect indentation for `<p>` beginning at L137:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  138:3  error  Incorrect indentation for `<br>` beginning at L138:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  145:2  error  Incorrect indentation for `<div>` beginning at L145:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  150:2  error  Incorrect indentation for `<div>` beginning at L150:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  145:93  error  Incorrect indentation for `Lugar de nacimiento\n` beginning at L145:C93. Expected `Lugar de nacimiento\n` to be at an indentation of 4 but was found at 93.  block-indentation\n  146:3  error  Incorrect indentation for `{{#if}}` beginning at L146:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  147:4  error  Incorrect indentation for `<button>` beginning at L147:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  151:3  error  Incorrect indentation for `<p>` beginning at L151:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  152:3  error  Incorrect indentation for `<a>` beginning at L152:C3. Expected `<a>` to be at an indentation of 4 but was found at 3.  block-indentation\n  154:3  error  Incorrect indentation for `{{#if}}` beginning at L154:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  168:3  error  Incorrect indentation for `<p>` beginning at L168:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  169:3  error  Incorrect indentation for `<p>` beginning at L169:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  171:3  error  Incorrect indentation for `{{#if}}` beginning at L171:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  155:3  error  Incorrect indentation for `<p>` beginning at L155:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  156:3  error  Incorrect indentation for `{{#power-select}}` beginning at L156:C3. Expected `{{#power-select}}` to be at an indentation of 5 but was found at 3.  block-indentation\n  164:3  error  Incorrect indentation for `<p>` beginning at L164:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  165:3  error  Incorrect indentation for `<p>` beginning at L165:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  172:4  error  Incorrect indentation for `<button>` beginning at L172:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  180:1  error  Incorrect indentation for `<div>` beginning at L180:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  204:1  error  Incorrect indentation for `<div>` beginning at L204:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  233:1  error  Incorrect indentation for `<div>` beginning at L233:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  181:2  error  Incorrect indentation for `<div>` beginning at L181:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  184:2  error  Incorrect indentation for `<div>` beginning at L184:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  181:93  error  Incorrect indentation for `Domicilio\n\t\t\t` beginning at L181:C93. Expected `Domicilio\n\t\t\t` to be at an indentation of 4 but was found at 93.  block-indentation\n  182:3  error  Incorrect indentation for `<button>` beginning at L182:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  185:3  error  Incorrect indentation for `<h5>` beginning at L185:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  186:3  error  Incorrect indentation for `<p>` beginning at L186:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  187:3  error  Incorrect indentation for `<br>` beginning at L187:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  188:3  error  Incorrect indentation for `<h5>` beginning at L188:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  189:3  error  Incorrect indentation for `<p>` beginning at L189:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  190:3  error  Incorrect indentation for `<br>` beginning at L190:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  191:3  error  Incorrect indentation for `<h5>` beginning at L191:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  192:3  error  Incorrect indentation for `<p>` beginning at L192:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  193:3  error  Incorrect indentation for `<br>` beginning at L193:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  194:3  error  Incorrect indentation for `<h5>` beginning at L194:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  195:3  error  Incorrect indentation for `<p>` beginning at L195:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  196:3  error  Incorrect indentation for `<br>` beginning at L196:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  205:2  error  Incorrect indentation for `<div>` beginning at L205:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  210:2  error  Incorrect indentation for `<div>` beginning at L210:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  205:93  error  Incorrect indentation for `Domicilio\n` beginning at L205:C93. Expected `Domicilio\n` to be at an indentation of 4 but was found at 93.  block-indentation\n  206:3  error  Incorrect indentation for `{{#if}}` beginning at L206:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  207:4  error  Incorrect indentation for `<button>` beginning at L207:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  211:3  error  Incorrect indentation for `<p>` beginning at L211:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  212:3  error  Incorrect indentation for `<a>` beginning at L212:C3. Expected `<a>` to be at an indentation of 4 but was found at 3.  block-indentation\n  213:3  error  Incorrect indentation for `{{#if}}` beginning at L213:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  228:3  error  Incorrect indentation for `{{#if}}` beginning at L228:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  214:3  error  Incorrect indentation for `<p>` beginning at L214:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  215:3  error  Incorrect indentation for `<p>` beginning at L215:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  216:3  error  Incorrect indentation for `<p>` beginning at L216:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  217:3  error  Incorrect indentation for `{{#power-select}}` beginning at L217:C3. Expected `{{#power-select}}` to be at an indentation of 5 but was found at 3.  block-indentation\n  225:3  error  Incorrect indentation for `<p>` beginning at L225:C3. Expected `<p>` to be at an indentation of 5 but was found at 3.  block-indentation\n  229:4  error  Incorrect indentation for `<button>` beginning at L229:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  237:1  error  Incorrect indentation for `<div>` beginning at L237:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  260:1  error  Incorrect indentation for `<div>` beginning at L260:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  276:1  error  Incorrect indentation for `<div>` beginning at L276:C1. Expected `<div>` to be at an indentation of 2 but was found at 1.  block-indentation\n  238:2  error  Incorrect indentation for `<div>` beginning at L238:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  241:2  error  Incorrect indentation for `<div>` beginning at L241:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  238:93  error  Incorrect indentation for `Caracter\xEDsticas f\xEDsicas\n\t\t\t` beginning at L238:C93. Expected `Caracter\xEDsticas f\xEDsicas\n\t\t\t` to be at an indentation of 4 but was found at 93.  block-indentation\n  239:3  error  Incorrect indentation for `<button>` beginning at L239:C3. Expected `<button>` to be at an indentation of 4 but was found at 3.  block-indentation\n  242:3  error  Incorrect indentation for `<h5>` beginning at L242:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  243:3  error  Incorrect indentation for `<p>` beginning at L243:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  244:3  error  Incorrect indentation for `<br>` beginning at L244:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  245:3  error  Incorrect indentation for `<h5>` beginning at L245:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  246:3  error  Incorrect indentation for `<p>` beginning at L246:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  247:3  error  Incorrect indentation for `<br>` beginning at L247:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  248:3  error  Incorrect indentation for `<h5>` beginning at L248:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  249:3  error  Incorrect indentation for `<p>` beginning at L249:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  250:3  error  Incorrect indentation for `<br>` beginning at L250:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  251:3  error  Incorrect indentation for `<h5>` beginning at L251:C3. Expected `<h5>` to be at an indentation of 4 but was found at 3.  block-indentation\n  252:3  error  Incorrect indentation for `<p>` beginning at L252:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  253:3  error  Incorrect indentation for `<br>` beginning at L253:C3. Expected `<br>` to be at an indentation of 4 but was found at 3.  block-indentation\n  261:2  error  Incorrect indentation for `<div>` beginning at L261:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  266:2  error  Incorrect indentation for `<div>` beginning at L266:C2. Expected `<div>` to be at an indentation of 3 but was found at 2.  block-indentation\n  261:93  error  Incorrect indentation for `Caracter\xEDsticas f\xEDsicas\n` beginning at L261:C93. Expected `Caracter\xEDsticas f\xEDsicas\n` to be at an indentation of 4 but was found at 93.  block-indentation\n  262:3  error  Incorrect indentation for `{{#if}}` beginning at L262:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  263:4  error  Incorrect indentation for `<button>` beginning at L263:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  267:3  error  Incorrect indentation for `<p>` beginning at L267:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  268:3  error  Incorrect indentation for `<p>` beginning at L268:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  269:3  error  Incorrect indentation for `<p>` beginning at L269:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  270:3  error  Incorrect indentation for `<p>` beginning at L270:C3. Expected `<p>` to be at an indentation of 4 but was found at 3.  block-indentation\n  271:3  error  Incorrect indentation for `{{#if}}` beginning at L271:C3. Expected `{{#if}}` to be at an indentation of 4 but was found at 3.  block-indentation\n  272:4  error  Incorrect indentation for `<button>` beginning at L272:C4. Expected `<button>` to be at an indentation of 5 but was found at 4.  block-indentation\n  279:1  error  Incorrect indentation for `{{#unless}}` beginning at L279:C1. Expected `{{#unless}}` to be at an indentation of 2 but was found at 1.  block-indentation\n  280:1  error  Incorrect indentation for `<button>` beginning at L280:C1. Expected `<button>` to be at an indentation of 3 but was found at 1.  block-indentation\n  281:1  error  Incorrect indentation for `<button>` beginning at L281:C1. Expected `<button>` to be at an indentation of 3 but was found at 1.  block-indentation\n  288:8  error  Incorrect indentation for `\n\t$(document).ready(function () {\n\t\t$(\'.datepicker\').datepicker({\n\t\t\tdefaultDate: new Date(2000, 0, 0),\n\t\t\tminDate: new Date(1980, 0, 0),\n\t\t\ti18n: {\n\t\t\t\tmonths: [\n\t\t\t\t\t\'Enero\',\n\t\t\t\t\t\'Febrero\',\n\t\t\t\t\t\'Marzo\',\n\t\t\t\t\t\'Abrio\',\n\t\t\t\t\t\'Mayo\',\n\t\t\t\t\t\'Junio\',\n\t\t\t\t\t\'Julio\',\n\t\t\t\t\t\'Agosto\',\n\t\t\t\t\t\'Septiembre\',\n\t\t\t\t\t\'Octubre\',\n\t\t\t\t\t\'Noviembre\',\n\t\t\t\t\t\'Diciembre\'\n\t\t\t\t],\n\t\t\t\tmonthsShort: [\n\t\t\t\t\t\'Ene\',\n\t\t\t\t\t\'Feb\',\n\t\t\t\t\t\'Mar\',\n\t\t\t\t\t\'Abr\',\n\t\t\t\t\t\'May\',\n\t\t\t\t\t\'Jun\',\n\t\t\t\t\t\'Jul\',\n\t\t\t\t\t\'Ago\',\n\t\t\t\t\t\'Sep\',\n\t\t\t\t\t\'Oct\',\n\t\t\t\t\t\'Nov\',\n\t\t\t\t\t\'Dic\'\n\t\t\t\t],\n\t\t\t\tweekdays: [\n\t\t\t\t\t\'Lunes\',\n\t\t\t\t\t\'Martes\',\n\t\t\t\t\t\'Mi\xE9rcoles\',\n\t\t\t\t\t\'Jueves\',\n\t\t\t\t\t\'Viernes\',\n\t\t\t\t\t\'S\xE1bado\',\n\t\t\t\t\t\'Domingo\'\n\t\t\t\t],\n\t\t\t\tweekdaysShort: [\n\t\t\t\t\t\'Lun\',\n\t\t\t\t\t\'Mar\',\n\t\t\t\t\t\'Mie\',\n\t\t\t\t\t\'Jue\',\n\t\t\t\t\t\'Vie\',\n\t\t\t\t\t\'Sab\',\n\t\t\t\t\t\'Dom\'\n\t\t\t\t],\n\t\t\t\tweekdaysAbbrev: [\n\t\t\t\t\'L\', \'M\', \'M\', \'J\', \'V\', \'S\', \'D\'\n\t\t\t\t],\n\t\t\t\tcancel: \'Cancelar\',\n\t\t\t\tclear: \'Limpiar\',\n\t\t\t\tdone: \'Listo\',\n\t\t\t}\n\n\t\t});\n\t});\n\n` beginning at L288:C8. Expected `\n\t$(document).ready(function () {\n\t\t$(\'.datepicker\').datepicker({\n\t\t\tdefaultDate: new Date(2000, 0, 0),\n\t\t\tminDate: new Date(1980, 0, 0),\n\t\t\ti18n: {\n\t\t\t\tmonths: [\n\t\t\t\t\t\'Enero\',\n\t\t\t\t\t\'Febrero\',\n\t\t\t\t\t\'Marzo\',\n\t\t\t\t\t\'Abrio\',\n\t\t\t\t\t\'Mayo\',\n\t\t\t\t\t\'Junio\',\n\t\t\t\t\t\'Julio\',\n\t\t\t\t\t\'Agosto\',\n\t\t\t\t\t\'Septiembre\',\n\t\t\t\t\t\'Octubre\',\n\t\t\t\t\t\'Noviembre\',\n\t\t\t\t\t\'Diciembre\'\n\t\t\t\t],\n\t\t\t\tmonthsShort: [\n\t\t\t\t\t\'Ene\',\n\t\t\t\t\t\'Feb\',\n\t\t\t\t\t\'Mar\',\n\t\t\t\t\t\'Abr\',\n\t\t\t\t\t\'May\',\n\t\t\t\t\t\'Jun\',\n\t\t\t\t\t\'Jul\',\n\t\t\t\t\t\'Ago\',\n\t\t\t\t\t\'Sep\',\n\t\t\t\t\t\'Oct\',\n\t\t\t\t\t\'Nov\',\n\t\t\t\t\t\'Dic\'\n\t\t\t\t],\n\t\t\t\tweekdays: [\n\t\t\t\t\t\'Lunes\',\n\t\t\t\t\t\'Martes\',\n\t\t\t\t\t\'Mi\xE9rcoles\',\n\t\t\t\t\t\'Jueves\',\n\t\t\t\t\t\'Viernes\',\n\t\t\t\t\t\'S\xE1bado\',\n\t\t\t\t\t\'Domingo\'\n\t\t\t\t],\n\t\t\t\tweekdaysShort: [\n\t\t\t\t\t\'Lun\',\n\t\t\t\t\t\'Mar\',\n\t\t\t\t\t\'Mie\',\n\t\t\t\t\t\'Jue\',\n\t\t\t\t\t\'Vie\',\n\t\t\t\t\t\'Sab\',\n\t\t\t\t\t\'Dom\'\n\t\t\t\t],\n\t\t\t\tweekdaysAbbrev: [\n\t\t\t\t\'L\', \'M\', \'M\', \'J\', \'V\', \'S\', \'D\'\n\t\t\t\t],\n\t\t\t\tcancel: \'Cancelar\',\n\t\t\t\tclear: \'Limpiar\',\n\t\t\t\tdone: \'Listo\',\n\t\t\t}\n\n\t\t});\n\t});\n\n` to be at an indentation of 2 but was found at 1.  block-indentation\n  6:26  error  elements cannot have inline styles  no-inline-styles\n  15:7  error  elements cannot have inline styles  no-inline-styles\n  16:6  error  elements cannot have inline styles  no-inline-styles\n  18:7  error  elements cannot have inline styles  no-inline-styles\n  20:6  error  elements cannot have inline styles  no-inline-styles\n  34:26  error  elements cannot have inline styles  no-inline-styles\n  71:26  error  elements cannot have inline styles  no-inline-styles\n  75:7  error  elements cannot have inline styles  no-inline-styles\n  76:6  error  elements cannot have inline styles  no-inline-styles\n  78:7  error  elements cannot have inline styles  no-inline-styles\n  79:6  error  elements cannot have inline styles  no-inline-styles\n  81:7  error  elements cannot have inline styles  no-inline-styles\n  82:6  error  elements cannot have inline styles  no-inline-styles\n  84:7  error  elements cannot have inline styles  no-inline-styles\n  85:6  error  elements cannot have inline styles  no-inline-styles\n  94:26  error  elements cannot have inline styles  no-inline-styles\n  117:26  error  elements cannot have inline styles  no-inline-styles\n  121:7  error  elements cannot have inline styles  no-inline-styles\n  122:6  error  elements cannot have inline styles  no-inline-styles\n  124:7  error  elements cannot have inline styles  no-inline-styles\n  125:6  error  elements cannot have inline styles  no-inline-styles\n  127:7  error  elements cannot have inline styles  no-inline-styles\n  128:6  error  elements cannot have inline styles  no-inline-styles\n  130:7  error  elements cannot have inline styles  no-inline-styles\n  131:6  error  elements cannot have inline styles  no-inline-styles\n  133:7  error  elements cannot have inline styles  no-inline-styles\n  134:6  error  elements cannot have inline styles  no-inline-styles\n  136:7  error  elements cannot have inline styles  no-inline-styles\n  137:6  error  elements cannot have inline styles  no-inline-styles\n  145:26  error  elements cannot have inline styles  no-inline-styles\n  181:26  error  elements cannot have inline styles  no-inline-styles\n  185:7  error  elements cannot have inline styles  no-inline-styles\n  186:6  error  elements cannot have inline styles  no-inline-styles\n  188:7  error  elements cannot have inline styles  no-inline-styles\n  189:6  error  elements cannot have inline styles  no-inline-styles\n  191:7  error  elements cannot have inline styles  no-inline-styles\n  192:6  error  elements cannot have inline styles  no-inline-styles\n  194:7  error  elements cannot have inline styles  no-inline-styles\n  195:6  error  elements cannot have inline styles  no-inline-styles\n  205:26  error  elements cannot have inline styles  no-inline-styles\n  238:26  error  elements cannot have inline styles  no-inline-styles\n  242:7  error  elements cannot have inline styles  no-inline-styles\n  243:6  error  elements cannot have inline styles  no-inline-styles\n  245:7  error  elements cannot have inline styles  no-inline-styles\n  246:6  error  elements cannot have inline styles  no-inline-styles\n  248:7  error  elements cannot have inline styles  no-inline-styles\n  249:6  error  elements cannot have inline styles  no-inline-styles\n  251:7  error  elements cannot have inline styles  no-inline-styles\n  252:6  error  elements cannot have inline styles  no-inline-styles\n  261:26  error  elements cannot have inline styles  no-inline-styles\n  278:5  error  elements cannot have inline styles  no-inline-styles\n  152:43  error  Interaction added to non-interactive element  no-invalid-interactive\n  212:43  error  Interaction added to non-interactive element  no-invalid-interactive\n  8:20  error  you must use double quotes in templates  quotes\n  8:29  error  you must use double quotes in templates  quotes\n  18:83  error  you must use double quotes in templates  quotes\n  36:21  error  you must use double quotes in templates  quotes\n  36:42  error  you must use double quotes in templates  quotes\n  41:27  error  you must use double quotes in templates  quotes\n  42:37  error  you must use double quotes in templates  quotes\n  43:37  error  you must use double quotes in templates  quotes\n  54:44  error  you must use double quotes in templates  quotes\n  58:21  error  you must use double quotes in templates  quotes\n  58:41  error  you must use double quotes in templates  quotes\n  72:20  error  you must use double quotes in templates  quotes\n  72:29  error  you must use double quotes in templates  quotes\n  75:61  error  you must use double quotes in templates  quotes\n  78:63  error  you must use double quotes in templates  quotes\n  81:61  error  you must use double quotes in templates  quotes\n  84:63  error  you must use double quotes in templates  quotes\n  96:21  error  you must use double quotes in templates  quotes\n  96:42  error  you must use double quotes in templates  quotes\n  106:21  error  you must use double quotes in templates  quotes\n  106:41  error  you must use double quotes in templates  quotes\n  118:20  error  you must use double quotes in templates  quotes\n  118:29  error  you must use double quotes in templates  quotes\n  121:55  error  you must use double quotes in templates  quotes\n  124:62  error  you must use double quotes in templates  quotes\n  127:62  error  you must use double quotes in templates  quotes\n  130:60  error  you must use double quotes in templates  quotes\n  133:62  error  you must use double quotes in templates  quotes\n  136:59  error  you must use double quotes in templates  quotes\n  147:21  error  you must use double quotes in templates  quotes\n  147:42  error  you must use double quotes in templates  quotes\n  151:23  error  you must use double quotes in templates  quotes\n  152:52  error  you must use double quotes in templates  quotes\n  164:30  error  you must use double quotes in templates  quotes\n  165:28  error  you must use double quotes in templates  quotes\n  168:32  error  you must use double quotes in templates  quotes\n  169:29  error  you must use double quotes in templates  quotes\n  172:21  error  you must use double quotes in templates  quotes\n  172:41  error  you must use double quotes in templates  quotes\n  182:20  error  you must use double quotes in templates  quotes\n  182:29  error  you must use double quotes in templates  quotes\n  185:57  error  you must use double quotes in templates  quotes\n  188:58  error  you must use double quotes in templates  quotes\n  191:59  error  you must use double quotes in templates  quotes\n  194:61  error  you must use double quotes in templates  quotes\n  207:21  error  you must use double quotes in templates  quotes\n  207:42  error  you must use double quotes in templates  quotes\n  211:23  error  you must use double quotes in templates  quotes\n  212:52  error  you must use double quotes in templates  quotes\n  214:26  error  you must use double quotes in templates  quotes\n  215:27  error  you must use double quotes in templates  quotes\n  225:30  error  you must use double quotes in templates  quotes\n  229:21  error  you must use double quotes in templates  quotes\n  229:41  error  you must use double quotes in templates  quotes\n  239:20  error  you must use double quotes in templates  quotes\n  239:29  error  you must use double quotes in templates  quotes\n  242:63  error  you must use double quotes in templates  quotes\n  245:65  error  you must use double quotes in templates  quotes\n  248:69  error  you must use double quotes in templates  quotes\n  251:76  error  you must use double quotes in templates  quotes\n  263:21  error  you must use double quotes in templates  quotes\n  263:42  error  you must use double quotes in templates  quotes\n  272:21  error  you must use double quotes in templates  quotes\n  272:41  error  you must use double quotes in templates  quotes\n  280:34  error  you must use double quotes in templates  quotes\n  281:30  error  you must use double quotes in templates  quotes\n  281:54  error  you must use double quotes in templates  quotes\n  281:64  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/components/pregunta-abierta.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/components/pregunta-abierta.hbs should pass TemplateLint.\n\ncifunhi/templates/components/pregunta-abierta.hbs\n  5:14  error  Incorrect indentation of attribute \'type\' beginning at L5:C14. Expected \'type\' to be at L6:C8.  attribute-indentation\n  5:26  error  Incorrect indentation of attribute \'placeholder\' beginning at L5:C26. Expected \'placeholder\' to be at L7:C8.  attribute-indentation\n  5:62  error  Incorrect indentation of attribute \'value\' beginning at L5:C62. Expected \'value\' to be at L8:C8.  attribute-indentation\n  5:86  error  Incorrect indentation of attribute \'class\' beginning at L5:C86. Expected \'class\' to be at L9:C8.  attribute-indentation\n  5:102  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L5:C102. Expected \'{{input}}\' to be at L10:C6.  attribute-indentation\n  9:6  error  Incorrect indentation for `<button>` beginning at L9:C6. Expected `<button>` to be at an indentation of 8 but was found at 6.  block-indentation\n  7:29  error  elements cannot have inline styles  no-inline-styles\n');
  });

  QUnit.test('cifunhi/templates/components/pregunta-una.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/components/pregunta-una.hbs should pass TemplateLint.\n\ncifunhi/templates/components/pregunta-una.hbs\n  5:20  error  Incorrect indentation of attribute \'type\' beginning at L5:C20. Expected \'type\' to be at L6:C14.  attribute-indentation\n  5:32  error  Incorrect indentation of attribute \'placeholder\' beginning at L5:C32. Expected \'placeholder\' to be at L7:C14.  attribute-indentation\n  5:68  error  Incorrect indentation of attribute \'value\' beginning at L5:C68. Expected \'value\' to be at L8:C14.  attribute-indentation\n  5:92  error  Incorrect indentation of attribute \'class\' beginning at L5:C92. Expected \'class\' to be at L9:C14.  attribute-indentation\n  5:108  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L5:C108. Expected \'{{input}}\' to be at L10:C12.  attribute-indentation\n  10:28  error  Incorrect indentation of attribute \'type\' beginning at L10:C28. Expected \'type\' to be at L11:C22.  attribute-indentation\n  10:40  error  Incorrect indentation of attribute \'placeholder\' beginning at L10:C40. Expected \'placeholder\' to be at L12:C22.  attribute-indentation\n  10:67  error  Incorrect indentation of attribute \'value\' beginning at L10:C67. Expected \'value\' to be at L13:C22.  attribute-indentation\n  10:93  error  Incorrect indentation of attribute \'class\' beginning at L10:C93. Expected \'class\' to be at L14:C22.  attribute-indentation\n  10:116  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L10:C116. Expected \'{{input}}\' to be at L15:C20.  attribute-indentation\n  2:4  error  Incorrect indentation for `<div>` beginning at L2:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  3:8  error  Incorrect indentation for `<div>` beginning at L3:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  18:8  error  Incorrect indentation for `<div>` beginning at L18:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  4:12  error  Incorrect indentation for `<span>` beginning at L4:C12. Expected `<span>` to be at an indentation of 10 but was found at 12.  block-indentation\n  5:12  error  Incorrect indentation for `{{input}}` beginning at L5:C12. Expected `{{input}}` to be at an indentation of 10 but was found at 12.  block-indentation\n  7:12  error  Incorrect indentation for `{{#each}}` beginning at L7:C12. Expected `{{#each}}` to be at an indentation of 10 but was found at 12.  block-indentation\n  8:16  error  Incorrect indentation for `<div>` beginning at L8:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  9:20  error  Incorrect indentation for `<i>` beginning at L9:C20. Expected `<i>` to be at an indentation of 18 but was found at 20.  block-indentation\n  10:20  error  Incorrect indentation for `{{input}}` beginning at L10:C20. Expected `{{input}}` to be at an indentation of 18 but was found at 20.  block-indentation\n  11:20  error  Incorrect indentation for `{{#if}}` beginning at L11:C20. Expected `{{#if}}` to be at an indentation of 18 but was found at 20.  block-indentation\n  12:20  error  Incorrect indentation for `<button>` beginning at L12:C20. Expected `<button>` to be at an indentation of 22 but was found at 20.  block-indentation\n  19:12  error  Incorrect indentation for `<button>` beginning at L19:C12. Expected `<button>` to be at an indentation of 10 but was found at 12.  block-indentation\n  20:12  error  Incorrect indentation for `{{#if}}` beginning at L20:C12. Expected `{{#if}}` to be at an indentation of 10 but was found at 12.  block-indentation\n  21:12  error  Incorrect indentation for `<button>` beginning at L21:C12. Expected `<button>` to be at an indentation of 14 but was found at 12.  block-indentation\n  9:53  error  elements cannot have inline styles  no-inline-styles\n');
  });

  QUnit.test('cifunhi/templates/components/pregunta-varias.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/components/pregunta-varias.hbs should pass TemplateLint.\n\ncifunhi/templates/components/pregunta-varias.hbs\n  5:20  error  Incorrect indentation of attribute \'type\' beginning at L5:C20. Expected \'type\' to be at L6:C14.  attribute-indentation\n  5:32  error  Incorrect indentation of attribute \'placeholder\' beginning at L5:C32. Expected \'placeholder\' to be at L7:C14.  attribute-indentation\n  5:68  error  Incorrect indentation of attribute \'value\' beginning at L5:C68. Expected \'value\' to be at L8:C14.  attribute-indentation\n  5:92  error  Incorrect indentation of attribute \'class\' beginning at L5:C92. Expected \'class\' to be at L9:C14.  attribute-indentation\n  5:108  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L5:C108. Expected \'{{input}}\' to be at L10:C12.  attribute-indentation\n  10:28  error  Incorrect indentation of attribute \'type\' beginning at L10:C28. Expected \'type\' to be at L11:C22.  attribute-indentation\n  10:40  error  Incorrect indentation of attribute \'placeholder\' beginning at L10:C40. Expected \'placeholder\' to be at L12:C22.  attribute-indentation\n  10:67  error  Incorrect indentation of attribute \'value\' beginning at L10:C67. Expected \'value\' to be at L13:C22.  attribute-indentation\n  10:93  error  Incorrect indentation of attribute \'class\' beginning at L10:C93. Expected \'class\' to be at L14:C22.  attribute-indentation\n  10:116  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L10:C116. Expected \'{{input}}\' to be at L15:C20.  attribute-indentation\n  2:4  error  Incorrect indentation for `<div>` beginning at L2:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  3:8  error  Incorrect indentation for `<div>` beginning at L3:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  18:8  error  Incorrect indentation for `<div>` beginning at L18:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  4:12  error  Incorrect indentation for `<span>` beginning at L4:C12. Expected `<span>` to be at an indentation of 10 but was found at 12.  block-indentation\n  5:12  error  Incorrect indentation for `{{input}}` beginning at L5:C12. Expected `{{input}}` to be at an indentation of 10 but was found at 12.  block-indentation\n  7:12  error  Incorrect indentation for `{{#each}}` beginning at L7:C12. Expected `{{#each}}` to be at an indentation of 10 but was found at 12.  block-indentation\n  8:16  error  Incorrect indentation for `<div>` beginning at L8:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  9:20  error  Incorrect indentation for `<i>` beginning at L9:C20. Expected `<i>` to be at an indentation of 18 but was found at 20.  block-indentation\n  10:20  error  Incorrect indentation for `{{input}}` beginning at L10:C20. Expected `{{input}}` to be at an indentation of 18 but was found at 20.  block-indentation\n  11:20  error  Incorrect indentation for `{{#if}}` beginning at L11:C20. Expected `{{#if}}` to be at an indentation of 18 but was found at 20.  block-indentation\n  12:20  error  Incorrect indentation for `<button>` beginning at L12:C20. Expected `<button>` to be at an indentation of 22 but was found at 20.  block-indentation\n  19:12  error  Incorrect indentation for `<button>` beginning at L19:C12. Expected `<button>` to be at an indentation of 10 but was found at 12.  block-indentation\n  20:12  error  Incorrect indentation for `{{#if}}` beginning at L20:C12. Expected `{{#if}}` to be at an indentation of 10 but was found at 12.  block-indentation\n  21:12  error  Incorrect indentation for `<button>` beginning at L21:C12. Expected `<button>` to be at an indentation of 14 but was found at 12.  block-indentation\n  9:53  error  elements cannot have inline styles  no-inline-styles\n');
  });

  QUnit.test('cifunhi/templates/cuestionario.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/cuestionario.hbs should pass TemplateLint.\n\ncifunhi/templates/cuestionario.hbs\n  12:13  error  Incorrect indentation of htmlAttribute \'style\' beginning at L12:C13. Expected \'style\' to be at L13:C10.  attribute-indentation\n  12:57  error  Incorrect indentation of htmlAttribute \'src\' beginning at L12:C57. Expected \'src\' to be at L14:C10.  attribute-indentation\n  12:82  error  Incorrect indentation of htmlAttribute \'width\' beginning at L12:C82. Expected \'width\' to be at L15:C10.  attribute-indentation\n  12:94  error  Incorrect indentation of close bracket \'>\' for the element \'<img>\' beginning at L12:C94. Expected \'<img>\' to be at L16:C8.  attribute-indentation\n  15:11  error  Incorrect indentation of htmlAttribute \'style\' beginning at L15:C11. Expected \'style\' to be at L16:C8.  attribute-indentation\n  15:95  error  Incorrect indentation of htmlAttribute \'class\' beginning at L15:C95. Expected \'class\' to be at L17:C8.  attribute-indentation\n  15:117  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L15:C117. Expected \'<div>\' to be at L18:C6.  attribute-indentation\n  43:12  error  Incorrect indentation of attribute \'type\' beginning at L43:C12. Expected \'type\' to be at L44:C6.  attribute-indentation\n  43:24  error  Incorrect indentation of attribute \'placeholder\' beginning at L43:C24. Expected \'placeholder\' to be at L45:C6.  attribute-indentation\n  43:51  error  Incorrect indentation of attribute \'class\' beginning at L43:C51. Expected \'class\' to be at L46:C6.  attribute-indentation\n  43:68  error  Incorrect indentation of attribute \'required\' beginning at L43:C68. Expected \'required\' to be at L47:C6.  attribute-indentation\n  43:82  error  Incorrect indentation of attribute \'value\' beginning at L43:C82. Expected \'value\' to be at L48:C6.  attribute-indentation\n  43:107  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L43:C107. Expected \'{{input}}\' to be at L49:C4.  attribute-indentation\n  67:8  error  Incorrect indentation of attribute \'changed\' beginning at L67:C8. Expected \'changed\' to be at L67:C10.  attribute-indentation\n  68:8  error  Incorrect indentation of attribute \'groupValue\' beginning at L68:C8. Expected \'groupValue\' to be at L68:C10.  attribute-indentation\n  69:8  error  Incorrect indentation of attribute \'name\' beginning at L69:C8. Expected \'name\' to be at L69:C10.  attribute-indentation\n  12:8  error  Incorrect indentation for `<img>` beginning at L12:C8. Expected `<img>` to be at an indentation of 10 but was found at 8.  block-indentation\n  27:2  error  Incorrect indentation for `<h1>` beginning at L27:C2. Expected `<h1>` to be at an indentation of 4 but was found at 2.  block-indentation\n  29:2  error  Incorrect indentation for `{{#unless}}` beginning at L29:C2. Expected `{{#unless}}` to be at an indentation of 4 but was found at 2.  block-indentation\n  36:2  error  Incorrect indentation for `{{#if}}` beginning at L36:C2. Expected `{{#if}}` to be at an indentation of 4 but was found at 2.  block-indentation\n  88:2  error  Incorrect indentation for `<h1>` beginning at L88:C2. Expected `<h1>` to be at an indentation of 4 but was found at 2.  block-indentation\n  89:2  error  Incorrect indentation for `<h2>` beginning at L89:C2. Expected `<h2>` to be at an indentation of 4 but was found at 2.  block-indentation\n  30:2  error  Incorrect indentation for `<form>` beginning at L30:C2. Expected `<form>` to be at an indentation of 4 but was found at 2.  block-indentation\n  37:2  error  Incorrect indentation for `<form>` beginning at L37:C2. Expected `<form>` to be at an indentation of 4 but was found at 2.  block-indentation\n  39:4  error  Incorrect indentation for `{{#if}}` beginning at L39:C4. Expected `{{#if}}` to be at an indentation of 6 but was found at 4.  block-indentation\n  48:4  error  Incorrect indentation for `{{#if}}` beginning at L48:C4. Expected `{{#if}}` to be at an indentation of 6 but was found at 4.  block-indentation\n  61:4  error  Incorrect indentation for `{{#if}}` beginning at L61:C4. Expected `{{#if}}` to be at an indentation of 6 but was found at 4.  block-indentation\n  81:4  error  Incorrect indentation for `<div>` beginning at L81:C4. Expected `<div>` to be at an indentation of 6 but was found at 4.  block-indentation\n  40:4  error  Incorrect indentation for `<h5>` beginning at L40:C4. Expected `<h5>` to be at an indentation of 6 but was found at 4.  block-indentation\n  41:4  error  Incorrect indentation for `{{log}}` beginning at L41:C4. Expected `{{log}}` to be at an indentation of 6 but was found at 4.  block-indentation\n  42:4  error  Incorrect indentation for `{{#each}}` beginning at L42:C4. Expected `{{#each}}` to be at an indentation of 6 but was found at 4.  block-indentation\n  43:4  error  Incorrect indentation for `{{input}}` beginning at L43:C4. Expected `{{input}}` to be at an indentation of 6 but was found at 4.  block-indentation\n  49:4  error  Incorrect indentation for `<h5>` beginning at L49:C4. Expected `<h5>` to be at an indentation of 6 but was found at 4.  block-indentation\n  50:4  error  Incorrect indentation for `<div>` beginning at L50:C4. Expected `<div>` to be at an indentation of 6 but was found at 4.  block-indentation\n  52:6  error  Incorrect indentation for `<div>` beginning at L52:C6. Expected `<div>` to be at an indentation of 8 but was found at 6.  block-indentation\n  62:4  error  Incorrect indentation for `<h5>` beginning at L62:C4. Expected `<h5>` to be at an indentation of 6 but was found at 4.  block-indentation\n  63:4  error  Incorrect indentation for `<div>` beginning at L63:C4. Expected `<div>` to be at an indentation of 6 but was found at 4.  block-indentation\n  65:6  error  Incorrect indentation for `<div>` beginning at L65:C6. Expected `<div>` to be at an indentation of 8 but was found at 6.  block-indentation\n  71:8  error  Incorrect indentation for `<span>` beginning at L71:C8. Expected `<span>` to be at an indentation of 10 but was found at 8.  block-indentation\n  12:8  error  img tags must have an alt attribute  img-alt-attributes\n  8:7  error  elements cannot have inline styles  no-inline-styles\n  10:11  error  elements cannot have inline styles  no-inline-styles\n  12:13  error  elements cannot have inline styles  no-inline-styles\n  15:11  error  elements cannot have inline styles  no-inline-styles\n  81:25  error  elements cannot have inline styles  no-inline-styles\n  41:4  error  Unexpected {{log}} usage.  no-log\n  30:17  error  you must use double quotes in templates  quotes\n  37:17  error  you must use double quotes in templates  quotes\n  39:28  error  you must use double quotes in templates  quotes\n  48:28  error  you must use double quotes in templates  quotes\n  61:28  error  you must use double quotes in templates  quotes\n  67:26  error  you must use double quotes in templates  quotes\n  87:2  error  Using an {{else}} block with {{unless}} should be avoided.  simple-unless\n');
  });

  QUnit.test('cifunhi/templates/cuestionarios/aplicar.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/cuestionarios/aplicar.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/cuestionarios/editar.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/cuestionarios/editar.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/cuestionarios/nuevo.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'cifunhi/templates/cuestionarios/nuevo.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('cifunhi/templates/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/index.hbs should pass TemplateLint.\n\ncifunhi/templates/index.hbs\n  6:10  error  Incorrect indentation of htmlAttribute \'name\' beginning at L6:C10. Expected \'name\' to be at L7:C6.  attribute-indentation\n  6:26  error  Incorrect indentation of htmlAttribute \'content\' beginning at L6:C26. Expected \'content\' to be at L8:C6.  attribute-indentation\n  6:89  error  Incorrect indentation of close bracket \'>\' for the element \'<meta>\' beginning at L6:C89. Expected \'<meta>\' to be at L9:C4.  attribute-indentation\n  48:9  error  Incorrect indentation of htmlAttribute \'id\' beginning at L48:C9. Expected \'id\' to be at L49:C6.  attribute-indentation\n  48:19  error  Incorrect indentation of htmlAttribute \'class\' beginning at L48:C19. Expected \'class\' to be at L50:C6.  attribute-indentation\n  48:66  error  Incorrect indentation of htmlAttribute \'data-jarallax\' beginning at L48:C66. Expected \'data-jarallax\' to be at L51:C6.  attribute-indentation\n  48:97  error  Incorrect indentation of htmlAttribute \'style\' beginning at L48:C97. Expected \'style\' to be at L52:C6.  attribute-indentation\n  48:140  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L48:C140. Expected \'<div>\' to be at L53:C4.  attribute-indentation\n  64:35  error  Incorrect indentation of htmlAttribute \'href\' beginning at L64:C35. Expected \'href\' to be at L65:C34.  attribute-indentation\n  64:88  error  Incorrect indentation of htmlAttribute \'target\' beginning at L64:C88. Expected \'target\' to be at L66:C34.  attribute-indentation\n  64:104  error  Incorrect indentation of htmlAttribute \'class\' beginning at L64:C104. Expected \'class\' to be at L67:C34.  attribute-indentation\n  64:143  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L64:C143. Expected \'<a>\' to be at L68:C32.  attribute-indentation\n  68:35  error  Incorrect indentation of htmlAttribute \'href\' beginning at L68:C35. Expected \'href\' to be at L69:C34.  attribute-indentation\n  68:81  error  Incorrect indentation of htmlAttribute \'target\' beginning at L68:C81. Expected \'target\' to be at L70:C34.  attribute-indentation\n  68:97  error  Incorrect indentation of htmlAttribute \'class\' beginning at L68:C97. Expected \'class\' to be at L71:C34.  attribute-indentation\n  68:135  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L68:C135. Expected \'<a>\' to be at L72:C32.  attribute-indentation\n  144:23  error  Incorrect indentation of htmlAttribute \'class\' beginning at L144:C23. Expected \'class\' to be at L145:C22.  attribute-indentation\n  144:81  error  Incorrect indentation of htmlAttribute \'data-wow-delay\' beginning at L144:C81. Expected \'data-wow-delay\' to be at L146:C22.  attribute-indentation\n  144:102  error  Incorrect indentation of htmlAttribute \'target\' beginning at L144:C102. Expected \'target\' to be at L147:C22.  attribute-indentation\n  144:117  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L144:C117. Expected \'<a>\' to be at L148:C20.  attribute-indentation\n  153:23  error  Incorrect indentation of htmlAttribute \'class\' beginning at L153:C23. Expected \'class\' to be at L154:C22.  attribute-indentation\n  153:81  error  Incorrect indentation of htmlAttribute \'data-wow-delay\' beginning at L153:C81. Expected \'data-wow-delay\' to be at L155:C22.  attribute-indentation\n  153:102  error  Incorrect indentation of htmlAttribute \'target\' beginning at L153:C102. Expected \'target\' to be at L156:C22.  attribute-indentation\n  153:117  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L153:C117. Expected \'<a>\' to be at L157:C20.  attribute-indentation\n  163:23  error  Incorrect indentation of htmlAttribute \'class\' beginning at L163:C23. Expected \'class\' to be at L164:C22.  attribute-indentation\n  163:81  error  Incorrect indentation of htmlAttribute \'data-wow-delay\' beginning at L163:C81. Expected \'data-wow-delay\' to be at L165:C22.  attribute-indentation\n  163:102  error  Incorrect indentation of htmlAttribute \'target\' beginning at L163:C102. Expected \'target\' to be at L166:C22.  attribute-indentation\n  163:117  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L163:C117. Expected \'<a>\' to be at L167:C20.  attribute-indentation\n  172:23  error  Incorrect indentation of htmlAttribute \'class\' beginning at L172:C23. Expected \'class\' to be at L173:C22.  attribute-indentation\n  172:81  error  Incorrect indentation of htmlAttribute \'data-wow-delay\' beginning at L172:C81. Expected \'data-wow-delay\' to be at L174:C22.  attribute-indentation\n  172:102  error  Incorrect indentation of htmlAttribute \'target\' beginning at L172:C102. Expected \'target\' to be at L175:C22.  attribute-indentation\n  172:117  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L172:C117. Expected \'<a>\' to be at L176:C20.  attribute-indentation\n  215:28  error  Incorrect indentation of htmlAttribute \'class\' beginning at L215:C28. Expected \'class\' to be at L216:C22.  attribute-indentation\n  215:86  error  Incorrect indentation of element modifier \'action\' beginning at L215:C86. Expected \'action\' to be at L217:C22.  attribute-indentation\n  215:105  error  Incorrect indentation of close bracket \'>\' for the element \'<button>\' beginning at L215:C105. Expected \'<button>\' to be at L218:C20.  attribute-indentation\n  215:113  error  Incorrect indentation of close tag \'</button>\' for element \'<button>\' beginning at L215:C113. Expected \'</button>\' to be at L215:C20.  attribute-indentation\n  313:21  error  Incorrect indentation of htmlAttribute \'class\' beginning at L313:C21. Expected \'class\' to be at L314:C18.  attribute-indentation\n  313:77  error  Incorrect indentation of htmlAttribute \'style\' beginning at L313:C77. Expected \'style\' to be at L315:C18.  attribute-indentation\n  313:108  error  Incorrect indentation of close bracket \'>\' for the element \'<div>\' beginning at L313:C108. Expected \'<div>\' to be at L316:C16.  attribute-indentation\n  329:31  error  Incorrect indentation of htmlAttribute \'action\' beginning at L329:C31. Expected \'action\' to be at L330:C27.  attribute-indentation\n  329:78  error  Incorrect indentation of htmlAttribute \'method\' beginning at L329:C78. Expected \'method\' to be at L331:C27.  attribute-indentation\n  329:92  error  Incorrect indentation of htmlAttribute \'target\' beginning at L329:C92. Expected \'target\' to be at L332:C27.  attribute-indentation\n  329:105  error  Incorrect indentation of close bracket \'>\' for the element \'<form>\' beginning at L329:C105. Expected \'<form>\' to be at L333:C25.  attribute-indentation\n  334:0  error  Incorrect indentation of close tag \'</form>\' for element \'<form>\' beginning at L334:C0. Expected \'</form>\' to be at L334:C25.  attribute-indentation\n  332:7  error  Incorrect indentation of htmlAttribute \'type\' beginning at L332:C7. Expected \'type\' to be at L333:C2.  attribute-indentation\n  332:20  error  Incorrect indentation of htmlAttribute \'src\' beginning at L332:C20. Expected \'src\' to be at L334:C2.  attribute-indentation\n  332:91  error  Incorrect indentation of htmlAttribute \'border\' beginning at L332:C91. Expected \'border\' to be at L335:C2.  attribute-indentation\n  332:102  error  Incorrect indentation of htmlAttribute \'name\' beginning at L332:C102. Expected \'name\' to be at L336:C2.  attribute-indentation\n  332:116  error  Incorrect indentation of htmlAttribute \'title\' beginning at L332:C116. Expected \'title\' to be at L337:C2.  attribute-indentation\n  332:170  error  Incorrect indentation of htmlAttribute \'alt\' beginning at L332:C170. Expected \'alt\' to be at L338:C2.  attribute-indentation\n  332:202  error  Incorrect indentation of close bracket \'>\' for the element \'<input>\' beginning at L332:C202. Expected \'<input>\' to be at L339:C0.  attribute-indentation\n  333:5  error  Incorrect indentation of htmlAttribute \'alt\' beginning at L333:C5. Expected \'alt\' to be at L334:C2.  attribute-indentation\n  333:12  error  Incorrect indentation of htmlAttribute \'border\' beginning at L333:C12. Expected \'border\' to be at L335:C2.  attribute-indentation\n  333:23  error  Incorrect indentation of htmlAttribute \'src\' beginning at L333:C23. Expected \'src\' to be at L336:C2.  attribute-indentation\n  333:74  error  Incorrect indentation of htmlAttribute \'width\' beginning at L333:C74. Expected \'width\' to be at L337:C2.  attribute-indentation\n  333:84  error  Incorrect indentation of htmlAttribute \'height\' beginning at L333:C84. Expected \'height\' to be at L338:C2.  attribute-indentation\n  333:95  error  Incorrect indentation of close bracket \'>\' for the element \'<img>\' beginning at L333:C95. Expected \'<img>\' to be at L339:C0.  attribute-indentation\n  439:19  error  Incorrect indentation of htmlAttribute \'href\' beginning at L439:C19. Expected \'href\' to be at L440:C18.  attribute-indentation\n  439:72  error  Incorrect indentation of htmlAttribute \'target\' beginning at L439:C72. Expected \'target\' to be at L441:C18.  attribute-indentation\n  439:88  error  Incorrect indentation of htmlAttribute \'class\' beginning at L439:C88. Expected \'class\' to be at L442:C18.  attribute-indentation\n  439:141  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L439:C141. Expected \'<a>\' to be at L443:C16.  attribute-indentation\n  443:19  error  Incorrect indentation of htmlAttribute \'href\' beginning at L443:C19. Expected \'href\' to be at L444:C18.  attribute-indentation\n  443:65  error  Incorrect indentation of htmlAttribute \'target\' beginning at L443:C65. Expected \'target\' to be at L445:C18.  attribute-indentation\n  443:81  error  Incorrect indentation of htmlAttribute \'class\' beginning at L443:C81. Expected \'class\' to be at L446:C18.  attribute-indentation\n  443:133  error  Incorrect indentation of close bracket \'>\' for the element \'<a>\' beginning at L443:C133. Expected \'<a>\' to be at L447:C16.  attribute-indentation\n  3:0  error  Incorrect indentation for `<head>` beginning at L3:C0. Expected `<head>` to be at an indentation of 2 but was found at 0.  block-indentation\n  15:0  error  Incorrect indentation for `<body>` beginning at L15:C0. Expected `<body>` to be at an indentation of 2 but was found at 0.  block-indentation\n  4:4  error  Incorrect indentation for `<!-- Required meta tags -->` beginning at L4:C4. Expected `<!-- Required meta tags -->` to be at an indentation of 2 but was found at 4.  block-indentation\n  5:4  error  Incorrect indentation for `<meta>` beginning at L5:C4. Expected `<meta>` to be at an indentation of 2 but was found at 4.  block-indentation\n  6:4  error  Incorrect indentation for `<meta>` beginning at L6:C4. Expected `<meta>` to be at an indentation of 2 but was found at 4.  block-indentation\n  7:4  error  Incorrect indentation for `<title>` beginning at L7:C4. Expected `<title>` to be at an indentation of 2 but was found at 4.  block-indentation\n  8:4  error  Incorrect indentation for `<!-- Plugins CSS -->` beginning at L8:C4. Expected `<!-- Plugins CSS -->` to be at an indentation of 2 but was found at 4.  block-indentation\n  9:4  error  Incorrect indentation for `<link>` beginning at L9:C4. Expected `<link>` to be at an indentation of 2 but was found at 4.  block-indentation\n  10:4  error  Incorrect indentation for `<link>` beginning at L10:C4. Expected `<link>` to be at an indentation of 2 but was found at 4.  block-indentation\n  11:4  error  Incorrect indentation for `<script>` beginning at L11:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  16:4  error  Incorrect indentation for `<div>` beginning at L16:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  19:4  error  Incorrect indentation for `<!--/preloader-->` beginning at L19:C4. Expected `<!--/preloader-->` to be at an indentation of 2 but was found at 4.  block-indentation\n  20:4  error  Incorrect indentation for `<!-- Pushy Menu -->` beginning at L20:C4. Expected `<!-- Pushy Menu -->` to be at an indentation of 2 but was found at 4.  block-indentation\n  21:4  error  Incorrect indentation for `<aside>` beginning at L21:C4. Expected `<aside>` to be at an indentation of 2 but was found at 4.  block-indentation\n  43:4  error  Incorrect indentation for `<!-- Site Overlay -->` beginning at L43:C4. Expected `<!-- Site Overlay -->` to be at an indentation of 2 but was found at 4.  block-indentation\n  44:4  error  Incorrect indentation for `<div>` beginning at L44:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  45:4  error  Incorrect indentation for `<nav>` beginning at L45:C4. Expected `<nav>` to be at an indentation of 2 but was found at 4.  block-indentation\n  48:4  error  Incorrect indentation for `<div>` beginning at L48:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  79:4  error  Incorrect indentation for `<!--home-->` beginning at L79:C4. Expected `<!--home-->` to be at an indentation of 2 but was found at 4.  block-indentation\n  80:4  error  Incorrect indentation for `<div>` beginning at L80:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  128:4  error  Incorrect indentation for `<div>` beginning at L128:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  187:4  error  Incorrect indentation for `<div>` beginning at L187:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  223:4  error  Incorrect indentation for `<div>` beginning at L223:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  307:4  error  Incorrect indentation for `<div>` beginning at L307:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  361:4  error  Incorrect indentation for `<div>` beginning at L361:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  436:4  error  Incorrect indentation for `<footer>` beginning at L436:C4. Expected `<footer>` to be at an indentation of 2 but was found at 4.  block-indentation\n  453:4  error  Incorrect indentation for `<!--back to top-->` beginning at L453:C4. Expected `<!--back to top-->` to be at an indentation of 2 but was found at 4.  block-indentation\n  454:4  error  Incorrect indentation for `<a>` beginning at L454:C4. Expected `<a>` to be at an indentation of 2 but was found at 4.  block-indentation\n  455:4  error  Incorrect indentation for `<!-- jQuery first, then Tether, then Bootstrap JS. -->` beginning at L455:C4. Expected `<!-- jQuery first, then Tether, then Bootstrap JS. -->` to be at an indentation of 2 but was found at 4.  block-indentation\n  456:4  error  Incorrect indentation for `<script>` beginning at L456:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  457:4  error  Incorrect indentation for `<script>` beginning at L457:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  458:4  error  Incorrect indentation for `<script>` beginning at L458:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  459:4  error  Incorrect indentation for `<script>` beginning at L459:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  462:4  error  Incorrect indentation for `<script>` beginning at L462:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  463:4  error  Incorrect indentation for `<script>` beginning at L463:C4. Expected `<script>` to be at an indentation of 2 but was found at 4.  block-indentation\n  17:8  error  Incorrect indentation for `<div>` beginning at L17:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  22:8  error  Incorrect indentation for `<div>` beginning at L22:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  25:8  error  Incorrect indentation for `<div>` beginning at L25:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  35:8  error  Incorrect indentation for `<div>` beginning at L35:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  23:12  error  Incorrect indentation for `<a>` beginning at L23:C12. Expected `<a>` to be at an indentation of 10 but was found at 12.  block-indentation\n  26:12  error  Incorrect indentation for `<ul>` beginning at L26:C12. Expected `<ul>` to be at an indentation of 10 but was found at 12.  block-indentation\n  27:16  error  Incorrect indentation for `<li>` beginning at L27:C16. Expected `<li>` to be at an indentation of 14 but was found at 16.  block-indentation\n  28:16  error  Incorrect indentation for `<li>` beginning at L28:C16. Expected `<li>` to be at an indentation of 14 but was found at 16.  block-indentation\n  29:16  error  Incorrect indentation for `<li>` beginning at L29:C16. Expected `<li>` to be at an indentation of 14 but was found at 16.  block-indentation\n  30:16  error  Incorrect indentation for `<li>` beginning at L30:C16. Expected `<li>` to be at an indentation of 14 but was found at 16.  block-indentation\n  31:16  error  Incorrect indentation for `<li>` beginning at L31:C16. Expected `<li>` to be at an indentation of 14 but was found at 16.  block-indentation\n  32:16  error  Incorrect indentation for `<li>` beginning at L32:C16. Expected `<li>` to be at an indentation of 14 but was found at 16.  block-indentation\n  36:12  error  Incorrect indentation for `<h5>` beginning at L36:C12. Expected `<h5>` to be at an indentation of 10 but was found at 12.  block-indentation\n  37:12  error  Incorrect indentation for `<p>` beginning at L37:C12. Expected `<p>` to be at an indentation of 10 but was found at 12.  block-indentation\n  38:12  error  Incorrect indentation for `<p>` beginning at L38:C12. Expected `<p>` to be at an indentation of 10 but was found at 12.  block-indentation\n  39:12  error  Incorrect indentation for `<h5>` beginning at L39:C12. Expected `<h5>` to be at an indentation of 10 but was found at 12.  block-indentation\n  40:12  error  Incorrect indentation for `<p>` beginning at L40:C12. Expected `<p>` to be at an indentation of 10 but was found at 12.  block-indentation\n  46:8  error  Incorrect indentation for `<a>` beginning at L46:C8. Expected `<a>` to be at an indentation of 6 but was found at 8.  block-indentation\n  49:8  error  Incorrect indentation for `<div>` beginning at L49:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  50:12  error  Incorrect indentation for `<div>` beginning at L50:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  51:16  error  Incorrect indentation for `<div>` beginning at L51:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  52:20  error  Incorrect indentation for `<div>` beginning at L52:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  53:24  error  Incorrect indentation for `<div>` beginning at L53:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  54:28  error  Incorrect indentation for `<div>` beginning at L54:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  59:28  error  Incorrect indentation for `<p>` beginning at L59:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  63:28  error  Incorrect indentation for `<div>` beginning at L63:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  55:32  error  Incorrect indentation for `<h5>` beginning at L55:C32. Expected `<h5>` to be at an indentation of 30 but was found at 32.  block-indentation\n  57:32  error  Incorrect indentation for `<span>` beginning at L57:C32. Expected `<span>` to be at an indentation of 30 but was found at 32.  block-indentation\n  56:55  error  Incorrect indentation for `h5` beginning at L55:C32. Expected `</h5>` ending at L56:C55 to be at an indentation of 32 but was found at 50.  block-indentation\n  55:92  error  Incorrect indentation for `Ciegos Fundaci\xF3n\n                                    Hidalguense AC` beginning at L55:C92. Expected `Ciegos Fundaci\xF3n\n                                    Hidalguense AC` to be at an indentation of 34 but was found at 92.  block-indentation\n  59:84  error  Incorrect indentation for `\n                                Nos definimos por ser una instituci\xF3n sin fines de lucro con el objetivo de brindar\n                                herramientas a ni\xF1os invidentes o con discapacidad visual.\n                            ` beginning at L59:C84. Expected `\n                                Nos definimos por ser una instituci\xF3n sin fines de lucro con el objetivo de brindar\n                                herramientas a ni\xF1os invidentes o con discapacidad visual.\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  64:32  error  Incorrect indentation for `<a>` beginning at L64:C32. Expected `<a>` to be at an indentation of 30 but was found at 32.  block-indentation\n  68:32  error  Incorrect indentation for `<a>` beginning at L68:C32. Expected `<a>` to be at an indentation of 30 but was found at 32.  block-indentation\n  65:36  error  Incorrect indentation for `<i>` beginning at L65:C36. Expected `<i>` to be at an indentation of 34 but was found at 36.  block-indentation\n  66:36  error  Incorrect indentation for `<i>` beginning at L66:C36. Expected `<i>` to be at an indentation of 34 but was found at 36.  block-indentation\n  69:36  error  Incorrect indentation for `<i>` beginning at L69:C36. Expected `<i>` to be at an indentation of 34 but was found at 36.  block-indentation\n  70:36  error  Incorrect indentation for `<i>` beginning at L70:C36. Expected `<i>` to be at an indentation of 34 but was found at 36.  block-indentation\n  81:8  error  Incorrect indentation for `<div>` beginning at L81:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  106:8  error  Incorrect indentation for `<div>` beginning at L106:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  82:12  error  Incorrect indentation for `<div>` beginning at L82:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  85:12  error  Incorrect indentation for `<div>` beginning at L85:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  83:16  error  Incorrect indentation for `<img>` beginning at L83:C16. Expected `<img>` to be at an indentation of 14 but was found at 16.  block-indentation\n  86:16  error  Incorrect indentation for `<h5>` beginning at L86:C16. Expected `<h5>` to be at an indentation of 14 but was found at 16.  block-indentation\n  87:16  error  Incorrect indentation for `<h3>` beginning at L87:C16. Expected `<h3>` to be at an indentation of 14 but was found at 16.  block-indentation\n  88:16  error  Incorrect indentation for `<p>` beginning at L88:C16. Expected `<p>` to be at an indentation of 14 but was found at 16.  block-indentation\n  88:19  error  Incorrect indentation for `\n                    Ver\xF3nica Ortega, ingeniero industrial, encontr\xF3 su amor en la vida de forma inesperada.\n                    Quer\xEDa ser educadora, pero sus padres no pod\xEDan costearle la carrera, eligi\xF3 la ingenier\xEDa, a la\n                    que tuvo que renunciar tambi\xE9n para cuidar de Amanda Berenice, quien nace prematura, con 5 meses de\n                    gestaci\xF3n. Se mantiene con vida, supera infecciones y hemorragias, pero le quedan secuelas, una\n                    hidrocefalia cong\xE9nita y ceguera total.\n                    Es necesario prodigiarle terapia para su rehabilitaci\xF3n. Inicia el desgaste f\xEDsico, emocional y\n                    econ\xF3mico tras viajar tres veces por semana al Instituto Nacional del Ni\xF1o Ciego en la Ciudad de\n                    M\xE9xico.\n                    Ver\xF3nica quiere hacer algo m\xE1s por otros ni\xF1os del estado. Le surge la idea y, a trav\xE9s del\n                    peri\xF3dico, convoca a padres de familia con la misma problem\xE1tica; tiene eco, cuenta con el apoyo\n                    del DIF municipal, el oftalm\xF3logo Gilberto Islas de la Vega y la doctora Norma Arreola. As\xED nace\n                    Ciegos Fundaci\xF3n Hidalguense A.C.\n                ` beginning at L88:C19. Expected `\n                    Ver\xF3nica Ortega, ingeniero industrial, encontr\xF3 su amor en la vida de forma inesperada.\n                    Quer\xEDa ser educadora, pero sus padres no pod\xEDan costearle la carrera, eligi\xF3 la ingenier\xEDa, a la\n                    que tuvo que renunciar tambi\xE9n para cuidar de Amanda Berenice, quien nace prematura, con 5 meses de\n                    gestaci\xF3n. Se mantiene con vida, supera infecciones y hemorragias, pero le quedan secuelas, una\n                    hidrocefalia cong\xE9nita y ceguera total.\n                    Es necesario prodigiarle terapia para su rehabilitaci\xF3n. Inicia el desgaste f\xEDsico, emocional y\n                    econ\xF3mico tras viajar tres veces por semana al Instituto Nacional del Ni\xF1o Ciego en la Ciudad de\n                    M\xE9xico.\n                    Ver\xF3nica quiere hacer algo m\xE1s por otros ni\xF1os del estado. Le surge la idea y, a trav\xE9s del\n                    peri\xF3dico, convoca a padres de familia con la misma problem\xE1tica; tiene eco, cuenta con el apoyo\n                    del DIF municipal, el oftalm\xF3logo Gilberto Islas de la Vega y la doctora Norma Arreola. As\xED nace\n                    Ciegos Fundaci\xF3n Hidalguense A.C.\n                ` to be at an indentation of 18 but was found at 20.  block-indentation\n  108:12  error  Incorrect indentation for `<div>` beginning at L108:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  122:12  error  Incorrect indentation for `<div>` beginning at L122:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  109:16  error  Incorrect indentation for `<h5>` beginning at L109:C16. Expected `<h5>` to be at an indentation of 14 but was found at 16.  block-indentation\n  110:16  error  Incorrect indentation for `<h3>` beginning at L110:C16. Expected `<h3>` to be at an indentation of 14 but was found at 16.  block-indentation\n  111:16  error  Incorrect indentation for `<p>` beginning at L111:C16. Expected `<p>` to be at an indentation of 14 but was found at 16.  block-indentation\n  116:16  error  Incorrect indentation for `<h3>` beginning at L116:C16. Expected `<h3>` to be at an indentation of 14 but was found at 16.  block-indentation\n  117:16  error  Incorrect indentation for `<p>` beginning at L117:C16. Expected `<p>` to be at an indentation of 14 but was found at 16.  block-indentation\n  111:19  error  Incorrect indentation for `\n                    Brindar atenci\xF3n, habilitaci\xF3n y educaci\xF3n a ni\xF1os y j\xF3venes con debilidad visual, impulsando su\n                    integraci\xF3n social y educativa, as\xED como facilitar las herramientas que les permitan ser\n                    independientes en su desarrollo escolar, laboral y entorno social para mejorar su calidad de vida.\n                ` beginning at L111:C19. Expected `\n                    Brindar atenci\xF3n, habilitaci\xF3n y educaci\xF3n a ni\xF1os y j\xF3venes con debilidad visual, impulsando su\n                    integraci\xF3n social y educativa, as\xED como facilitar las herramientas que les permitan ser\n                    independientes en su desarrollo escolar, laboral y entorno social para mejorar su calidad de vida.\n                ` to be at an indentation of 18 but was found at 20.  block-indentation\n  117:19  error  Incorrect indentation for `\n                    Ser una instituci\xF3n pionera en la atenci\xF3n a ni\xF1os y j\xF3venes con debilidad visual, compartiendo las\n                    experiencias y metodolog\xEDas que mejor funcionen, para garantizar servicios integrales de calidad.\n                ` beginning at L117:C19. Expected `\n                    Ser una instituci\xF3n pionera en la atenci\xF3n a ni\xF1os y j\xF3venes con debilidad visual, compartiendo las\n                    experiencias y metodolog\xEDas que mejor funcionen, para garantizar servicios integrales de calidad.\n                ` to be at an indentation of 18 but was found at 20.  block-indentation\n  123:16  error  Incorrect indentation for `<img>` beginning at L123:C16. Expected `<img>` to be at an indentation of 14 but was found at 16.  block-indentation\n  129:8  error  Incorrect indentation for `<div>` beginning at L129:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  131:12  error  Incorrect indentation for `<div>` beginning at L131:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  133:16  error  Incorrect indentation for `<div>` beginning at L133:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  134:20  error  Incorrect indentation for `<h3>` beginning at L134:C20. Expected `<h3>` to be at an indentation of 18 but was found at 20.  block-indentation\n  135:20  error  Incorrect indentation for `<p>` beginning at L135:C20. Expected `<p>` to be at an indentation of 18 but was found at 20.  block-indentation\n  139:20  error  Incorrect indentation for `<p>` beginning at L139:C20. Expected `<p>` to be at an indentation of 18 but was found at 20.  block-indentation\n  144:20  error  Incorrect indentation for `<a>` beginning at L144:C20. Expected `<a>` to be at an indentation of 18 but was found at 20.  block-indentation\n  153:20  error  Incorrect indentation for `<a>` beginning at L153:C20. Expected `<a>` to be at an indentation of 18 but was found at 20.  block-indentation\n  163:20  error  Incorrect indentation for `<a>` beginning at L163:C20. Expected `<a>` to be at an indentation of 18 but was found at 20.  block-indentation\n  172:20  error  Incorrect indentation for `<a>` beginning at L172:C20. Expected `<a>` to be at an indentation of 18 but was found at 20.  block-indentation\n  135:57  error  Incorrect indentation for `\n                        Ay\xFAdanos desarrollando acciones a beneficio de los ni\xF1os, ni\xF1as y j\xF3venes con discapacidad\n                        visual, mediante la donaci\xF3n de tu tiempo para apoyarlos en su aprendizaje pedag\xF3gico.\n                    ` beginning at L135:C57. Expected `\n                        Ay\xFAdanos desarrollando acciones a beneficio de los ni\xF1os, ni\xF1as y j\xF3venes con discapacidad\n                        visual, mediante la donaci\xF3n de tu tiempo para apoyarlos en su aprendizaje pedag\xF3gico.\n                    ` to be at an indentation of 22 but was found at 24.  block-indentation\n  139:57  error  Incorrect indentation for `\n                        \xBFDe qu\xE9 otra forma puedo ayudar a CIFUNHI?\n                        ` beginning at L139:C57. Expected `\n                        \xBFDe qu\xE9 otra forma puedo ayudar a CIFUNHI?\n                        ` to be at an indentation of 22 but was found at 24.  block-indentation\n  141:24  error  Incorrect indentation for `<br>` beginning at L141:C24. Expected `<br>` to be at an indentation of 22 but was found at 24.  block-indentation\n  145:24  error  Incorrect indentation for `<h5>` beginning at L145:C24. Expected `<h5>` to be at an indentation of 22 but was found at 24.  block-indentation\n  146:24  error  Incorrect indentation for `<p>` beginning at L146:C24. Expected `<p>` to be at an indentation of 22 but was found at 24.  block-indentation\n  147:28  error  Incorrect indentation for `<img>` beginning at L147:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  148:28  error  Incorrect indentation for `<img>` beginning at L148:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  149:28  error  Incorrect indentation for `<img>` beginning at L149:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  150:28  error  Incorrect indentation for `<img>` beginning at L150:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  154:24  error  Incorrect indentation for `<h5>` beginning at L154:C24. Expected `<h5>` to be at an indentation of 22 but was found at 24.  block-indentation\n  155:24  error  Incorrect indentation for `<p>` beginning at L155:C24. Expected `<p>` to be at an indentation of 22 but was found at 24.  block-indentation\n  156:28  error  Incorrect indentation for `<img>` beginning at L156:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  157:28  error  Incorrect indentation for `<img>` beginning at L157:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  158:28  error  Incorrect indentation for `<img>` beginning at L158:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  159:28  error  Incorrect indentation for `<img>` beginning at L159:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  164:24  error  Incorrect indentation for `<h5>` beginning at L164:C24. Expected `<h5>` to be at an indentation of 22 but was found at 24.  block-indentation\n  165:24  error  Incorrect indentation for `<p>` beginning at L165:C24. Expected `<p>` to be at an indentation of 22 but was found at 24.  block-indentation\n  166:28  error  Incorrect indentation for `<img>` beginning at L166:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  167:28  error  Incorrect indentation for `<img>` beginning at L167:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  168:28  error  Incorrect indentation for `<img>` beginning at L168:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  169:28  error  Incorrect indentation for `<img>` beginning at L169:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  173:24  error  Incorrect indentation for `<h5>` beginning at L173:C24. Expected `<h5>` to be at an indentation of 22 but was found at 24.  block-indentation\n  174:24  error  Incorrect indentation for `<p>` beginning at L174:C24. Expected `<p>` to be at an indentation of 22 but was found at 24.  block-indentation\n  175:28  error  Incorrect indentation for `<img>` beginning at L175:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  176:28  error  Incorrect indentation for `<img>` beginning at L176:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  177:28  error  Incorrect indentation for `<img>` beginning at L177:C28. Expected `<img>` to be at an indentation of 26 but was found at 28.  block-indentation\n  188:8  error  Incorrect indentation for `<div>` beginning at L188:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  189:12  error  Incorrect indentation for `<h3>` beginning at L189:C12. Expected `<h3>` to be at an indentation of 10 but was found at 12.  block-indentation\n  191:12  error  Incorrect indentation for `<div>` beginning at L191:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  192:16  error  Incorrect indentation for `{{#each}}` beginning at L192:C16. Expected `{{#each}}` to be at an indentation of 14 but was found at 16.  block-indentation\n  213:16  error  Incorrect indentation for `{{#if}}` beginning at L213:C16. Expected `{{#if}}` to be at an indentation of 14 but was found at 16.  block-indentation\n  193:16  error  Incorrect indentation for `{{#if}}` beginning at L193:C16. Expected `{{#if}}` to be at an indentation of 18 but was found at 16.  block-indentation\n  208:16  error  Incorrect indentation for `<div>` beginning at L208:C16. Expected `<div>` to be at an indentation of 18 but was found at 16.  block-indentation\n  194:16  error  Incorrect indentation for `<div>` beginning at L194:C16. Expected `<div>` to be at an indentation of 18 but was found at 16.  block-indentation\n  205:16  error  Incorrect indentation for `<!--/col-->` beginning at L205:C16. Expected `<!--/col-->` to be at an indentation of 18 but was found at 16.  block-indentation\n  195:20  error  Incorrect indentation for `<a>` beginning at L195:C20. Expected `<a>` to be at an indentation of 18 but was found at 20.  block-indentation\n  196:24  error  Incorrect indentation for `<img>` beginning at L196:C24. Expected `<img>` to be at an indentation of 22 but was found at 24.  block-indentation\n  197:24  error  Incorrect indentation for `<div>` beginning at L197:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  198:28  error  Incorrect indentation for `<div>` beginning at L198:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  199:32  error  Incorrect indentation for `<h4>` beginning at L199:C32. Expected `<h4>` to be at an indentation of 30 but was found at 32.  block-indentation\n  200:32  error  Incorrect indentation for `<p>` beginning at L200:C32. Expected `<p>` to be at an indentation of 30 but was found at 32.  block-indentation\n  209:20  error  Incorrect indentation for `<h3>` beginning at L209:C20. Expected `<h3>` to be at an indentation of 18 but was found at 20.  block-indentation\n  214:16  error  Incorrect indentation for `<div>` beginning at L214:C16. Expected `<div>` to be at an indentation of 18 but was found at 16.  block-indentation\n  215:20  error  Incorrect indentation for `<button>` beginning at L215:C20. Expected `<button>` to be at an indentation of 18 but was found at 20.  block-indentation\n  224:8  error  Incorrect indentation for `<div>` beginning at L224:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  225:12  error  Incorrect indentation for `<h3>` beginning at L225:C12. Expected `<h3>` to be at an indentation of 10 but was found at 12.  block-indentation\n  227:12  error  Incorrect indentation for `<div>` beginning at L227:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  228:16  error  Incorrect indentation for `<div>` beginning at L228:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  240:16  error  Incorrect indentation for `<!--/col-->` beginning at L240:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  241:16  error  Incorrect indentation for `<div>` beginning at L241:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  253:16  error  Incorrect indentation for `<!--/col-->` beginning at L253:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  254:16  error  Incorrect indentation for `<div>` beginning at L254:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  265:16  error  Incorrect indentation for `<!--/col-->` beginning at L265:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  266:16  error  Incorrect indentation for `<div>` beginning at L266:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  278:16  error  Incorrect indentation for `<!--/col-->` beginning at L278:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  279:16  error  Incorrect indentation for `<div>` beginning at L279:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  291:16  error  Incorrect indentation for `<!--/col-->` beginning at L291:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  292:16  error  Incorrect indentation for `<div>` beginning at L292:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  303:16  error  Incorrect indentation for `<!--/col-->` beginning at L303:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  229:20  error  Incorrect indentation for `<div>` beginning at L229:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  230:24  error  Incorrect indentation for `<i>` beginning at L230:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  231:24  error  Incorrect indentation for `<div>` beginning at L231:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  232:28  error  Incorrect indentation for `<h4>` beginning at L232:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  233:28  error  Incorrect indentation for `<p>` beginning at L233:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  233:31  error  Incorrect indentation for `\n                                Consiste en la evaluaci\xF3n oportuna y completa de los prematuros de riesgo para tratar\n                                en el momento adecuado.\n                            ` beginning at L233:C31. Expected `\n                                Consiste en la evaluaci\xF3n oportuna y completa de los prematuros de riesgo para tratar\n                                en el momento adecuado.\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  242:20  error  Incorrect indentation for `<div>` beginning at L242:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  243:24  error  Incorrect indentation for `<i>` beginning at L243:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  244:24  error  Incorrect indentation for `<div>` beginning at L244:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  245:28  error  Incorrect indentation for `<h4>` beginning at L245:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  246:28  error  Incorrect indentation for `<p>` beginning at L246:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  246:31  error  Incorrect indentation for `\n                                Atenci\xF3n del alumno con ceguera de acuerdo al curr\xEDculo oficial aplicado para el resto\n                                de los estudiantes.\n                            ` beginning at L246:C31. Expected `\n                                Atenci\xF3n del alumno con ceguera de acuerdo al curr\xEDculo oficial aplicado para el resto\n                                de los estudiantes.\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  255:20  error  Incorrect indentation for `<div>` beginning at L255:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  256:24  error  Incorrect indentation for `<i>` beginning at L256:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  257:24  error  Incorrect indentation for `<div>` beginning at L257:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  258:28  error  Incorrect indentation for `<h4>` beginning at L258:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  259:28  error  Incorrect indentation for `<p>` beginning at L259:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  259:31  error  Incorrect indentation for `\n                                Uso correcto del bast\xF3n blanco durante el crecimiento del menor.\n                            ` beginning at L259:C31. Expected `\n                                Uso correcto del bast\xF3n blanco durante el crecimiento del menor.\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  267:20  error  Incorrect indentation for `<div>` beginning at L267:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  268:24  error  Incorrect indentation for `<i>` beginning at L268:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  269:24  error  Incorrect indentation for `<div>` beginning at L269:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  270:28  error  Incorrect indentation for `<h4>` beginning at L270:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  271:28  error  Incorrect indentation for `<p>` beginning at L271:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  271:31  error  Incorrect indentation for `\n                                Capacitaci\xF3n en habilidades como el sistema braile, el uso del bast\xF3n y el empleo del\n                                \xE1baco (Kramer).\n                            ` beginning at L271:C31. Expected `\n                                Capacitaci\xF3n en habilidades como el sistema braile, el uso del bast\xF3n y el empleo del\n                                \xE1baco (Kramer).\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  280:20  error  Incorrect indentation for `<div>` beginning at L280:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  281:24  error  Incorrect indentation for `<i>` beginning at L281:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  282:24  error  Incorrect indentation for `<div>` beginning at L282:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  283:28  error  Incorrect indentation for `<h4>` beginning at L283:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  284:28  error  Incorrect indentation for `<p>` beginning at L284:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  284:31  error  Incorrect indentation for `\n                                Taller de los zapatos del ni\xF1o ciego, taller de ense\xF1anza del c\xF3digo braile y taller de\n                                orientaci\xF3n y motriicidad.\n                            ` beginning at L284:C31. Expected `\n                                Taller de los zapatos del ni\xF1o ciego, taller de ense\xF1anza del c\xF3digo braile y taller de\n                                orientaci\xF3n y motriicidad.\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  293:20  error  Incorrect indentation for `<div>` beginning at L293:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  294:24  error  Incorrect indentation for `<i>` beginning at L294:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  295:24  error  Incorrect indentation for `<div>` beginning at L295:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  296:28  error  Incorrect indentation for `<h4>` beginning at L296:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  297:28  error  Incorrect indentation for `<p>` beginning at L297:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  297:31  error  Incorrect indentation for `\n                                Elaboraci\xF3n de proyectos para convocatorias en beneficio de los ni\xF1os.\n                            ` beginning at L297:C31. Expected `\n                                Elaboraci\xF3n de proyectos para convocatorias en beneficio de los ni\xF1os.\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  308:8  error  Incorrect indentation for `<div>` beginning at L308:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  309:12  error  Incorrect indentation for `<div>` beginning at L309:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  344:12  error  Incorrect indentation for `<div>` beginning at L344:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  310:16  error  Incorrect indentation for `<div>` beginning at L310:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  313:16  error  Incorrect indentation for `<div>` beginning at L313:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  311:20  error  Incorrect indentation for `<h3>` beginning at L311:C20. Expected `<h3>` to be at an indentation of 18 but was found at 20.  block-indentation\n  315:15  error  Incorrect indentation for `{{!      {{#if pagar}} }}` beginning at L315:C15. Expected `{{!      {{#if pagar}} }}` to be at an indentation of 18 but was found at 15.  block-indentation\n  317:24  error  Incorrect indentation for `<div>` beginning at L317:C24. Expected `<div>` to be at an indentation of 18 but was found at 24.  block-indentation\n  339:0  error  Incorrect indentation for `{{!                     {{else}}\n                     <button {{action \'donar\'}} class=\'btn btn-primary btn-lg\' style="height:50px; margin-top:20px">Donar</button>\n                    {{/if}} }}` beginning at L339:C0. Expected `{{!                     {{else}}\n                     <button {{action \'donar\'}} class=\'btn btn-primary btn-lg\' style="height:50px; margin-top:20px">Donar</button>\n                    {{/if}} }}` to be at an indentation of 18 but was found at 0.  block-indentation\n  318:0  error  Incorrect indentation for `{{!                             <form>\n                                <div class="col-md-4"></div>\n                                <div class="col-md-4"><strong>Ingrese la cantidad que desea donar</strong></div>\n                                <div class="col-md-4"></div>\n                                 <div class="col-md-4"></div>\n                                <div class="col-md-4">${{input type="number" value=cantidad required=true}} MXN </div>\n                                <div class="col-md-4"></div>\n\n                               <button>Cancelar</button>\n                            </form> }}` beginning at L318:C0. Expected `{{!                             <form>\n                                <div class="col-md-4"></div>\n                                <div class="col-md-4"><strong>Ingrese la cantidad que desea donar</strong></div>\n                                <div class="col-md-4"></div>\n                                 <div class="col-md-4"></div>\n                                <div class="col-md-4">${{input type="number" value=cantidad required=true}} MXN </div>\n                                <div class="col-md-4"></div>\n\n                               <button>Cancelar</button>\n                            </form> }}` to be at an indentation of 26 but was found at 0.  block-indentation\n  329:25  error  Incorrect indentation for `<form>` beginning at L329:C25. Expected `<form>` to be at an indentation of 26 but was found at 25.  block-indentation\n  334:7  error  Incorrect indentation for `form` beginning at L329:C25. Expected `</form>` ending at L334:C7 to be at an indentation of 25 but was found at 0.  block-indentation\n  330:0  error  Incorrect indentation for `<input>` beginning at L330:C0. Expected `<input>` to be at an indentation of 27 but was found at 0.  block-indentation\n  331:0  error  Incorrect indentation for `<input>` beginning at L331:C0. Expected `<input>` to be at an indentation of 27 but was found at 0.  block-indentation\n  332:0  error  Incorrect indentation for `<input>` beginning at L332:C0. Expected `<input>` to be at an indentation of 27 but was found at 0.  block-indentation\n  333:0  error  Incorrect indentation for `<img>` beginning at L333:C0. Expected `<img>` to be at an indentation of 27 but was found at 0.  block-indentation\n  345:16  error  Incorrect indentation for `<div>` beginning at L345:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  346:16  error  Incorrect indentation for `<div>` beginning at L346:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  347:16  error  Incorrect indentation for `<div>` beginning at L347:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  348:17  error  Incorrect indentation for `<div>` beginning at L348:C17. Expected `<div>` to be at an indentation of 14 but was found at 17.  block-indentation\n  349:16  error  Incorrect indentation for `<div>` beginning at L349:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  350:16  error  Incorrect indentation for `<div>` beginning at L350:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  351:17  error  Incorrect indentation for `<div>` beginning at L351:C17. Expected `<div>` to be at an indentation of 14 but was found at 17.  block-indentation\n  352:16  error  Incorrect indentation for `<div>` beginning at L352:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  353:17  error  Incorrect indentation for `<div>` beginning at L353:C17. Expected `<div>` to be at an indentation of 14 but was found at 17.  block-indentation\n  354:18  error  Incorrect indentation for `<div>` beginning at L354:C18. Expected `<div>` to be at an indentation of 14 but was found at 18.  block-indentation\n  355:16  error  Incorrect indentation for `<div>` beginning at L355:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  356:16  error  Incorrect indentation for `<div>` beginning at L356:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  362:8  error  Incorrect indentation for `<div>` beginning at L362:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  363:12  error  Incorrect indentation for `<div>` beginning at L363:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  369:12  error  Incorrect indentation for `<div>` beginning at L369:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  413:12  error  Incorrect indentation for `<div>` beginning at L413:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  364:16  error  Incorrect indentation for `<div>` beginning at L364:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  365:20  error  Incorrect indentation for `<h3>` beginning at L365:C20. Expected `<h3>` to be at an indentation of 18 but was found at 20.  block-indentation\n  366:20  error  Incorrect indentation for `<p>` beginning at L366:C20. Expected `<p>` to be at an indentation of 18 but was found at 20.  block-indentation\n  370:16  error  Incorrect indentation for `<div>` beginning at L370:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  381:16  error  Incorrect indentation for `<!--/col-->` beginning at L381:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  382:16  error  Incorrect indentation for `<div>` beginning at L382:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  396:16  error  Incorrect indentation for `<!--/col-->` beginning at L396:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  397:16  error  Incorrect indentation for `<div>` beginning at L397:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  411:16  error  Incorrect indentation for `<!--/col-->` beginning at L411:C16. Expected `<!--/col-->` to be at an indentation of 14 but was found at 16.  block-indentation\n  371:20  error  Incorrect indentation for `<div>` beginning at L371:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  372:24  error  Incorrect indentation for `<i>` beginning at L372:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  373:24  error  Incorrect indentation for `<div>` beginning at L373:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  374:28  error  Incorrect indentation for `<h4>` beginning at L374:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  375:28  error  Incorrect indentation for `<p>` beginning at L375:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  375:31  error  Incorrect indentation for `\n                                Calle Juli\xE1n Villagr\xE1n #412 Centro` beginning at L375:C31. Expected `\n                                Calle Juli\xE1n Villagr\xE1n #412 Centro` to be at an indentation of 30 but was found at 32.  block-indentation\n  383:20  error  Incorrect indentation for `<div>` beginning at L383:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  384:24  error  Incorrect indentation for `<i>` beginning at L384:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  385:24  error  Incorrect indentation for `<div>` beginning at L385:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  386:28  error  Incorrect indentation for `<h4>` beginning at L386:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  387:28  error  Incorrect indentation for `<p>` beginning at L387:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  390:28  error  Incorrect indentation for `<p>` beginning at L390:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  387:31  error  Incorrect indentation for `\n                                cifunhi2@gmail.com\n                            ` beginning at L387:C31. Expected `\n                                cifunhi2@gmail.com\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  390:31  error  Incorrect indentation for `\n                                cifunhi@yahoo.com.mx\n                            ` beginning at L390:C31. Expected `\n                                cifunhi@yahoo.com.mx\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  398:20  error  Incorrect indentation for `<div>` beginning at L398:C20. Expected `<div>` to be at an indentation of 18 but was found at 20.  block-indentation\n  399:24  error  Incorrect indentation for `<i>` beginning at L399:C24. Expected `<i>` to be at an indentation of 22 but was found at 24.  block-indentation\n  400:24  error  Incorrect indentation for `<div>` beginning at L400:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  401:28  error  Incorrect indentation for `<h4>` beginning at L401:C28. Expected `<h4>` to be at an indentation of 26 but was found at 28.  block-indentation\n  402:28  error  Incorrect indentation for `<p>` beginning at L402:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  405:28  error  Incorrect indentation for `<p>` beginning at L405:C28. Expected `<p>` to be at an indentation of 26 but was found at 28.  block-indentation\n  402:31  error  Incorrect indentation for `\n                                771 715 1789\n                            ` beginning at L402:C31. Expected `\n                                771 715 1789\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  405:31  error  Incorrect indentation for `\n                                771 209 5221\n                            ` beginning at L405:C31. Expected `\n                                771 209 5221\n                            ` to be at an indentation of 30 but was found at 32.  block-indentation\n  414:16  error  Incorrect indentation for `<div>` beginning at L414:C16. Expected `<div>` to be at an indentation of 14 but was found at 16.  block-indentation\n  415:20  error  Incorrect indentation for `<form>` beginning at L415:C20. Expected `<form>` to be at an indentation of 18 but was found at 20.  block-indentation\n  416:24  error  Incorrect indentation for `<div>` beginning at L416:C24. Expected `<div>` to be at an indentation of 22 but was found at 24.  block-indentation\n  417:28  error  Incorrect indentation for `<div>` beginning at L417:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  420:28  error  Incorrect indentation for `<div>` beginning at L420:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  423:28  error  Incorrect indentation for `<div>` beginning at L423:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  426:28  error  Incorrect indentation for `<div>` beginning at L426:C28. Expected `<div>` to be at an indentation of 26 but was found at 28.  block-indentation\n  418:32  error  Incorrect indentation for `<input>` beginning at L418:C32. Expected `<input>` to be at an indentation of 30 but was found at 32.  block-indentation\n  421:32  error  Incorrect indentation for `<input>` beginning at L421:C32. Expected `<input>` to be at an indentation of 30 but was found at 32.  block-indentation\n  424:32  error  Incorrect indentation for `<textarea>` beginning at L424:C32. Expected `<textarea>` to be at an indentation of 30 but was found at 32.  block-indentation\n  427:32  error  Incorrect indentation for `<button>` beginning at L427:C32. Expected `<button>` to be at an indentation of 30 but was found at 32.  block-indentation\n  437:8  error  Incorrect indentation for `<div>` beginning at L437:C8. Expected `<div>` to be at an indentation of 6 but was found at 8.  block-indentation\n  438:12  error  Incorrect indentation for `<div>` beginning at L438:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  448:12  error  Incorrect indentation for `<div>` beginning at L448:C12. Expected `<div>` to be at an indentation of 10 but was found at 12.  block-indentation\n  439:16  error  Incorrect indentation for `<a>` beginning at L439:C16. Expected `<a>` to be at an indentation of 14 but was found at 16.  block-indentation\n  443:16  error  Incorrect indentation for `<a>` beginning at L443:C16. Expected `<a>` to be at an indentation of 14 but was found at 16.  block-indentation\n  440:20  error  Incorrect indentation for `<i>` beginning at L440:C20. Expected `<i>` to be at an indentation of 18 but was found at 20.  block-indentation\n  441:20  error  Incorrect indentation for `<i>` beginning at L441:C20. Expected `<i>` to be at an indentation of 18 but was found at 20.  block-indentation\n  444:20  error  Incorrect indentation for `<i>` beginning at L444:C20. Expected `<i>` to be at an indentation of 18 but was found at 20.  block-indentation\n  445:20  error  Incorrect indentation for `<i>` beginning at L445:C20. Expected `<i>` to be at an indentation of 18 but was found at 20.  block-indentation\n  448:26  error  Incorrect indentation for `\n                \xA9 Copyright 2018. All Right Reserved. Talentics.\n            ` beginning at L448:C26. Expected `\n                \xA9 Copyright 2018. All Right Reserved. Talentics.\n            ` to be at an indentation of 14 but was found at 16.  block-indentation\n  147:28  error  img tags must have an alt attribute  img-alt-attributes\n  148:28  error  img tags must have an alt attribute  img-alt-attributes\n  149:28  error  img tags must have an alt attribute  img-alt-attributes\n  150:28  error  img tags must have an alt attribute  img-alt-attributes\n  156:28  error  img tags must have an alt attribute  img-alt-attributes\n  157:28  error  img tags must have an alt attribute  img-alt-attributes\n  158:28  error  img tags must have an alt attribute  img-alt-attributes\n  159:28  error  img tags must have an alt attribute  img-alt-attributes\n  166:28  error  img tags must have an alt attribute  img-alt-attributes\n  167:28  error  img tags must have an alt attribute  img-alt-attributes\n  168:28  error  img tags must have an alt attribute  img-alt-attributes\n  169:28  error  img tags must have an alt attribute  img-alt-attributes\n  175:28  error  img tags must have an alt attribute  img-alt-attributes\n  176:28  error  img tags must have an alt attribute  img-alt-attributes\n  177:28  error  img tags must have an alt attribute  img-alt-attributes\n  64:32  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  68:32  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  144:20  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  153:20  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  163:20  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  172:20  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  439:16  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  443:16  error  links with target="_blank" must have rel="noopener"  link-rel-noopener\n  4:4  error  HTML comment detected  no-html-comments\n  8:4  error  HTML comment detected  no-html-comments\n  19:4  error  HTML comment detected  no-html-comments\n  20:4  error  HTML comment detected  no-html-comments\n  43:4  error  HTML comment detected  no-html-comments\n  79:4  error  HTML comment detected  no-html-comments\n  205:16  error  HTML comment detected  no-html-comments\n  240:16  error  HTML comment detected  no-html-comments\n  253:16  error  HTML comment detected  no-html-comments\n  265:16  error  HTML comment detected  no-html-comments\n  278:16  error  HTML comment detected  no-html-comments\n  291:16  error  HTML comment detected  no-html-comments\n  303:16  error  HTML comment detected  no-html-comments\n  381:16  error  HTML comment detected  no-html-comments\n  396:16  error  HTML comment detected  no-html-comments\n  411:16  error  HTML comment detected  no-html-comments\n  453:4  error  HTML comment detected  no-html-comments\n  455:4  error  HTML comment detected  no-html-comments\n  16:24  error  elements cannot have inline styles  no-inline-styles\n  48:97  error  elements cannot have inline styles  no-inline-styles\n  55:68  error  elements cannot have inline styles  no-inline-styles\n  59:60  error  elements cannot have inline styles  no-inline-styles\n  195:68  error  elements cannot have inline styles  no-inline-styles\n  313:77  error  elements cannot have inline styles  no-inline-styles\n  317:52  error  elements cannot have inline styles  no-inline-styles\n  344:41  error  elements cannot have inline styles  no-inline-styles\n  427:85  error  elements cannot have inline styles  no-inline-styles\n  48:80  error  you must use double quotes in templates  quotes\n  48:103  error  you must use double quotes in templates  quotes\n  82:23  error  you must use double quotes in templates  quotes\n  83:25  error  you must use double quotes in templates  quotes\n  83:42  error  you must use double quotes in templates  quotes\n  83:51  error  you must use double quotes in templates  quotes\n  122:23  error  you must use double quotes in templates  quotes\n  123:25  error  you must use double quotes in templates  quotes\n  123:49  error  you must use double quotes in templates  quotes\n  123:58  error  you must use double quotes in templates  quotes\n  215:95  error  you must use double quotes in templates  quotes\n  223:15  error  you must use double quotes in templates  quotes\n  223:39  error  you must use double quotes in templates  quotes\n  224:19  error  you must use double quotes in templates  quotes\n  227:23  error  you must use double quotes in templates  quotes\n  228:27  error  you must use double quotes in templates  quotes\n  228:71  error  you must use double quotes in templates  quotes\n  229:31  error  you must use double quotes in templates  quotes\n  230:33  error  you must use double quotes in templates  quotes\n  231:35  error  you must use double quotes in templates  quotes\n  241:27  error  you must use double quotes in templates  quotes\n  241:71  error  you must use double quotes in templates  quotes\n  242:31  error  you must use double quotes in templates  quotes\n  243:33  error  you must use double quotes in templates  quotes\n  244:35  error  you must use double quotes in templates  quotes\n  254:27  error  you must use double quotes in templates  quotes\n  254:71  error  you must use double quotes in templates  quotes\n  255:31  error  you must use double quotes in templates  quotes\n  256:33  error  you must use double quotes in templates  quotes\n  257:35  error  you must use double quotes in templates  quotes\n  266:27  error  you must use double quotes in templates  quotes\n  266:71  error  you must use double quotes in templates  quotes\n  267:31  error  you must use double quotes in templates  quotes\n  268:33  error  you must use double quotes in templates  quotes\n  269:35  error  you must use double quotes in templates  quotes\n  279:27  error  you must use double quotes in templates  quotes\n  279:71  error  you must use double quotes in templates  quotes\n  280:31  error  you must use double quotes in templates  quotes\n  281:33  error  you must use double quotes in templates  quotes\n  282:35  error  you must use double quotes in templates  quotes\n  292:27  error  you must use double quotes in templates  quotes\n  292:71  error  you must use double quotes in templates  quotes\n  293:31  error  you must use double quotes in templates  quotes\n  294:33  error  you must use double quotes in templates  quotes\n  295:35  error  you must use double quotes in templates  quotes\n  307:15  error  you must use double quotes in templates  quotes\n  308:19  error  you must use double quotes in templates  quotes\n  309:23  error  you must use double quotes in templates  quotes\n  310:27  error  you must use double quotes in templates  quotes\n  311:30  error  you must use double quotes in templates  quotes\n  313:27  error  you must use double quotes in templates  quotes\n  330:0  error  Self-closing a void element is redundant  self-closing-void-elements\n  331:0  error  Self-closing a void element is redundant  self-closing-void-elements\n  332:0  error  Self-closing a void element is redundant  self-closing-void-elements\n  333:0  error  Self-closing a void element is redundant  self-closing-void-elements\n');
  });

  QUnit.test('cifunhi/templates/login.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/login.hbs should pass TemplateLint.\n\ncifunhi/templates/login.hbs\n  28:18  error  Incorrect indentation of attribute \'type\' beginning at L28:C18. Expected \'type\' to be at L29:C12.  attribute-indentation\n  28:30  error  Incorrect indentation of attribute \'id\' beginning at L28:C30. Expected \'id\' to be at L30:C12.  attribute-indentation\n  28:45  error  Incorrect indentation of attribute \'value\' beginning at L28:C45. Expected \'value\' to be at L31:C12.  attribute-indentation\n  28:56  error  Incorrect indentation of attribute \'class\' beginning at L28:C56. Expected \'class\' to be at L32:C12.  attribute-indentation\n  28:73  error  Incorrect indentation of attribute \'focus-out\' beginning at L28:C73. Expected \'focus-out\' to be at L33:C12.  attribute-indentation\n  28:113  error  Incorrect indentation of attribute \'tabindex\' beginning at L28:C113. Expected \'tabindex\' to be at L34:C12.  attribute-indentation\n  28:123  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L28:C123. Expected \'{{input}}\' to be at L35:C10.  attribute-indentation\n  30:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L30:C16. Expected \'class\' to be at L31:C12.  attribute-indentation\n  30:36  error  Incorrect indentation of htmlAttribute \'data-error\' beginning at L30:C36. Expected \'data-error\' to be at L32:C12.  attribute-indentation\n  30:108  error  Incorrect indentation of close bracket \'>\' for the element \'<span>\' beginning at L30:C108. Expected \'<span>\' to be at L33:C10.  attribute-indentation\n  34:18  error  Incorrect indentation of attribute \'type\' beginning at L34:C18. Expected \'type\' to be at L35:C12.  attribute-indentation\n  34:34  error  Incorrect indentation of attribute \'id\' beginning at L34:C34. Expected \'id\' to be at L36:C12.  attribute-indentation\n  34:49  error  Incorrect indentation of attribute \'value\' beginning at L34:C49. Expected \'value\' to be at L37:C12.  attribute-indentation\n  34:60  error  Incorrect indentation of attribute \'class\' beginning at L34:C60. Expected \'class\' to be at L38:C12.  attribute-indentation\n  35:10  error  Incorrect indentation of attribute \'focus-out\' beginning at L35:C10. Expected \'focus-out\' to be at L39:C12.  attribute-indentation\n  35:50  error  Incorrect indentation of attribute \'tabindex\' beginning at L35:C50. Expected \'tabindex\' to be at L40:C12.  attribute-indentation\n  35:60  error  Incorrect indentation of close curly braces \'}}\' for the component \'{{input}}\' beginning at L35:C60. Expected \'{{input}}\' to be at L41:C10.  attribute-indentation\n  37:16  error  Incorrect indentation of htmlAttribute \'class\' beginning at L37:C16. Expected \'class\' to be at L38:C12.  attribute-indentation\n  37:36  error  Incorrect indentation of htmlAttribute \'data-error\' beginning at L37:C36. Expected \'data-error\' to be at L39:C12.  attribute-indentation\n  37:107  error  Incorrect indentation of close bracket \'>\' for the element \'<span>\' beginning at L37:C107. Expected \'<span>\' to be at L40:C10.  attribute-indentation\n  1:2  error  Incorrect indentation for `<style>` beginning at L1:C2. Expected `<style>` to be at an indentation of 0, but was found at 2.  block-indentation\n  22:8  error  Incorrect indentation for `style` beginning at L1:C2. Expected `</style>` ending at L22:C8 to be at an indentation of 2 but was found at 0.  block-indentation\n  1:9  error  Incorrect indentation for `\n\n.tra{\n  opacity: .95;\n}\n.ex{\n    border: 2px;\n    border-radius: 25px;\n}\nbutton{\n margin-bottom: 5%;\n}\n.ini{\n  height: 120px;\n}\n.topmar{\n  margin-top: 5%;\n}\nbody{\n  background: #344259;\n}\n` beginning at L1:C9. Expected `\n\n.tra{\n  opacity: .95;\n}\n.ex{\n    border: 2px;\n    border-radius: 25px;\n}\nbutton{\n margin-bottom: 5%;\n}\n.ini{\n  height: 120px;\n}\n.topmar{\n  margin-top: 5%;\n}\nbody{\n  background: #344259;\n}\n` to be at an indentation of 4 but was found at 0.  block-indentation\n  24:4  error  Incorrect indentation for `<div>` beginning at L24:C4. Expected `<div>` to be at an indentation of 2 but was found at 4.  block-indentation\n  41:58  error  elements cannot have inline styles  no-inline-styles\n  30:47  error  Unnecessary string concatenation. Use {{if loginUserError loginUserError \'Revisando...\'}} instead of "{{if loginUserError loginUserError \'Revisando...\'}}".  no-unnecessary-concat\n  37:47  error  Unnecessary string concatenation. Use {{if loginPassError loginPassError \'Revisando...\'}} instead of "{{if loginPassError loginPassError \'Revisando...\'}}".  no-unnecessary-concat\n  23:51  error  you must use double quotes in templates  quotes\n  28:91  error  you must use double quotes in templates  quotes\n  28:105  error  you must use double quotes in templates  quotes\n  30:83  error  you must use double quotes in templates  quotes\n  35:28  error  you must use double quotes in templates  quotes\n  35:42  error  you must use double quotes in templates  quotes\n  37:83  error  you must use double quotes in templates  quotes\n');
  });

  QUnit.test('cifunhi/templates/success.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'cifunhi/templates/success.hbs should pass TemplateLint.\n\ncifunhi/templates/success.hbs\n  3:17  error  you must use double quotes in templates  quotes\n');
  });
});
define('cifunhi/tests/lint/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('integration/components/kid-anexos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/kid-anexos-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/kid-documents-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/kid-documents-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/kid-general-data-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/kid-general-data-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/pregunta-abierta-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/pregunta-abierta-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/pregunta-una-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/pregunta-una-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/pregunta-varias-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/pregunta-varias-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/cuestionarios/aplicar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/cuestionarios/aplicar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/cuestionarios/editar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/cuestionarios/editar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/cuestionarios/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/cuestionarios/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/cuestionarios/nuevo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/cuestionarios/nuevo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/expedientes/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/expedientes/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/expedientes/nuevo/anexos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/expedientes/nuevo/anexos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/expedientes/nuevo/datos-generales-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/expedientes/nuevo/datos-generales-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/expedientes/nuevo/documentos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/expedientes/nuevo/documentos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/expedientes/ver/cuestionarios-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/expedientes/ver/cuestionarios-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/expedientes/ver/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/expedientes/ver/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/administrador/fotos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/administrador/fotos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/cancel-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/cancel-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/cuestionario-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/cuestionario-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/login-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/login-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/success-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/success-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/admin-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/admin-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/anexo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/anexo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/answer-kid-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/answer-kid-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/answer-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/answer-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/caracteristicas-fisicas-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/caracteristicas-fisicas-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/cuestionario-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/cuestionario-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/datos-generales-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/datos-generales-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/documento-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/documento-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/kid-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/kid-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/question-kid-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/question-kid-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/question-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/question-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/questionary-kid-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/questionary-kid-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/servicio-medico-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/servicio-medico-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/cuestionarios/aplicar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/cuestionarios/aplicar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/cuestionarios/detalles-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/cuestionarios/detalles-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/cuestionarios/editar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/cuestionarios/editar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/cuestionarios/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/cuestionarios/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/cuestionarios/nuevo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/cuestionarios/nuevo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/editar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/editar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/editar/anexos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/editar/anexos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/editar/datos-generales-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/editar/datos-generales-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/editar/documentos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/editar/documentos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/nuevo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/nuevo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/nuevo/anexos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/nuevo/anexos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/nuevo/datos-generales-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/nuevo/datos-generales-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/nuevo/documentos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/nuevo/documentos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/ver-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/ver-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/ver/cuestionarios-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/ver/cuestionarios-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/expedientes/ver/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/expedientes/ver/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/fotos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/fotos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/administrador/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/administrador/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cancel-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cancel-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cuestionario-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cuestionario-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cuestionarios/aplicar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cuestionarios/aplicar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cuestionarios/editar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cuestionarios/editar-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cuestionarios/nuevo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cuestionarios/nuevo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/login-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/login-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/success-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/success-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/services/ajax-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/services/ajax-test.js should pass ESLint\n\n');
  });
});
define("qunit/index", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  /* globals QUnit */

  var _module = QUnit.module;
  exports.module = _module;
  var test = exports.test = QUnit.test;
  var skip = exports.skip = QUnit.skip;
  var only = exports.only = QUnit.only;
  var todo = exports.todo = QUnit.todo;

  exports.default = QUnit;
});
define('cifunhi/tests/test-helper', ['cifunhi/app', 'cifunhi/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('cifunhi/tests/unit/controllers/administrador-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/cuestionarios/aplicar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/cuestionarios/aplicar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/cuestionarios/aplicar');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/cuestionarios/editar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/cuestionarios/editar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/cuestionarios/editar');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/cuestionarios/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/cuestionarios/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/cuestionarios/index');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/cuestionarios/nuevo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/cuestionarios/nuevo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/cuestionarios/nuevo');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/expedientes/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/expedientes/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/expedientes/index');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/expedientes/nuevo/anexos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/expedientes/nuevo/anexos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/expedientes/nuevo/anexos');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/expedientes/nuevo/datos-generales-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/expedientes/nuevo/datos-generales', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/expedientes/nuevo/datos-generales');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/expedientes/nuevo/documentos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/expedientes/nuevo/documentos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/expedientes/nuevo/documentos');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/expedientes/ver/cuestionarios-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/expedientes/ver/cuestionarios', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/expedientes/ver/cuestionarios');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/expedientes/ver/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/expedientes/ver/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/expedientes/ver/index');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/administrador/fotos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | administrador/fotos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:administrador/fotos');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/cancel-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | cancel', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:cancel');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/cuestionario-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | cuestionario', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:cuestionario');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:index');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/login-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | login', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:login');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/controllers/success-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | success', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:success');
      assert.ok(controller);
    });
  });
});
define('cifunhi/tests/unit/models/admin-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | admin', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('admin', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/anexo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | anexo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('anexo', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/answer-kid-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | answer kid', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('answer-kid', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/answer-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | answer', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('answer', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/caracteristicas-fisicas-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | caracteristicas fisicas', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('caracteristicas-fisicas', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/cuestionario-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | cuestionario', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('cuestionario', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/datos-generales-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | datos generales', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('datos-generales', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/documento-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | documento', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('documento', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/kid-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | kid', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('kid', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/photo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | photo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('photo', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/question-kid-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | question kid', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('question-kid', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/question-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | question', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('question', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/questionary-kid-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | questionary kid', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('questionary-kid', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/models/servicio-medico-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | servicio medico', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('servicio-medico', {});
      assert.ok(model);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/cuestionarios/aplicar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/cuestionarios/aplicar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/cuestionarios/aplicar');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/cuestionarios/detalles-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/cuestionarios/detalles', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/cuestionarios/detalles');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/cuestionarios/editar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/cuestionarios/editar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/cuestionarios/editar');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/cuestionarios/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/cuestionarios/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/cuestionarios/index');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/cuestionarios/nuevo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/cuestionarios/nuevo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/cuestionarios/nuevo');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/editar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/editar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/editar');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/editar/anexos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/editar/anexos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/editar/anexos');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/editar/datos-generales-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/editar/datosGenerales', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/editar/datos-generales');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/editar/documentos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/editar/documentos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/editar/documentos');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/index');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/nuevo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/nuevo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/nuevo');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/nuevo/anexos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/nuevo/anexos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/nuevo/anexos');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/nuevo/datos-generales-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/nuevo/datosGenerales', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/nuevo/datos-generales');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/nuevo/documentos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/nuevo/documentos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/nuevo/documentos');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/ver-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/ver', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/ver');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/ver/cuestionarios-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/ver/cuestionarios', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/ver/cuestionarios');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/expedientes/ver/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/expedientes/ver/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/expedientes/ver/index');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/fotos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/fotos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/fotos');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/administrador/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | administrador/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:administrador/index');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/cancel-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | cancel', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:cancel');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/cuestionario-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | cuestionario', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:cuestionario');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/cuestionarios/aplicar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | cuestionarios/aplicar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:cuestionarios/aplicar');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/cuestionarios/editar-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | cuestionarios/editar', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:cuestionarios/editar');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/cuestionarios/nuevo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | cuestionarios/nuevo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:cuestionarios/nuevo');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:index');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/login-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | login', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:login');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/routes/success-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | success', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:success');
      assert.ok(route);
    });
  });
});
define('cifunhi/tests/unit/services/ajax-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Service | ajax', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let service = this.owner.lookup('service:ajax');
      assert.ok(service);
    });
  });
});
define('cifunhi/config/environment', [], function() {
  var prefix = 'cifunhi';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('cifunhi/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
